@echo off
echo Build APK...
call cd /d D:\Dev\BitBucket\indigo-venues\client
call gradlew assembleDebug
echo Reinstalling the app...
timeout /t 3
call cd /d C:\adb\platform-tools
call adb install -r D:\Dev\BitBucket\indigo-venues\client\app\build\outputs\apk\debug\app-debug.apk
echo App installed
call adb shell monkey -p 'ro.uaic.fii.indigovenues' -v 1
echo Launching the app...
timeout /t 3
