package ro.uaic.fii.indigovenues.ui.activity;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.service.foursquare.AddToScheduleService;
import ro.uaic.fii.indigovenues.service.foursquare.CheckVenuePresenceInScheduleService;
import ro.uaic.fii.indigovenues.service.foursquare.RemoveFromScheduleService;
import ro.uaic.fii.indigovenues.service.foursquare.VenueDetailsService;

public class VenueDetailsActivity extends AppCompatActivity {
    private static final String TAG_LOGGING = VenueDetailsActivity.class.getName();

    public static String VENUE_ID_KEY = "VENUE_ID";
    public String venueId;
    private TextView textViewVenueTitle, textViewCategory, textViewLocation,
            textViewAddress, textViewHours, textViewPhone, textViewUrlWebsite;
    private ProgressBar progressBar;
    private RelativeLayout relativeLayoutVenueDetailsWrapper;
    private ImageView imageViewBestPhoto;
    private Button btnAddToSchedule; // this button can be 'remove from schedule' if venue is in schedule

    private String addToScheduleText;
    private String removeFromScheduleText;

    private View.OnClickListener addToScheduleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (btnAddToSchedule.getText().toString().equals(addToScheduleText)) {
                attemptAddToSchedule();
            } else {
                attemptRemoveFromSchedule();
            }
        }
    };

    public TextView getTextViewVenueTitle() {
        return textViewVenueTitle;
    }

    public TextView getTextViewCategory() {
        return textViewCategory;
    }

    public TextView getTextViewLocation() {
        return textViewLocation;
    }

    public TextView getTextViewAddress() {
        return textViewAddress;
    }

    public TextView getTextViewHours() {
        return textViewHours;
    }

    public TextView getTextViewPhone() {
        return textViewPhone;
    }

    public TextView getTextViewUrlWebsite() {
        return textViewUrlWebsite;
    }

    public ImageView getImageViewBestPhoto() {
        return imageViewBestPhoto;
    }

    public RelativeLayout getRelativeLayoutVenueDetailsWrapper() {
        return relativeLayoutVenueDetailsWrapper;
    }

    public Button getBtnAddToSchedule() {
        return btnAddToSchedule;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_details);
        relativeLayoutVenueDetailsWrapper = findViewById(R.id.relative_layout_wrapper_venue_details);
        addToScheduleText = getBaseContext().getString(R.string.add_to_schedule);
        removeFromScheduleText = getBaseContext().getString(R.string.remove_from_schedule);
        venueId = getStringFromExtra(savedInstanceState, VENUE_ID_KEY);
        initializeWidgets();
        getVenueDetailsByIdFromServer(venueId);
        btnAddToSchedule.setOnClickListener(addToScheduleClickListener);

    }

    private String getStringFromExtra(Bundle savedInstanceState, final String KEY) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                Log.e(TAG_LOGGING, "Could not find the following key in savedInstanceState: " + KEY);
                return "";
            } else {
                Log.i(TAG_LOGGING, "Found the following key in savedInstanceState: " + KEY);
                return extras.getString(KEY);
            }
        } else {
            Log.i(TAG_LOGGING, "Found the following key in savedInstanceState: " + KEY);
            return (String) savedInstanceState.getSerializable(KEY);
        }
    }

    private void initializeWidgets() {
        Log.i(TAG_LOGGING, "Initializing widgets...");
        textViewVenueTitle = findViewById(R.id.text_view_venue_name);
        textViewCategory = findViewById(R.id.text_view_venue_category);
        textViewLocation = findViewById(R.id.text_view_venue_location);
        textViewAddress = findViewById(R.id.text_view_address);
        textViewHours = findViewById(R.id.text_view_hours);
        textViewPhone = findViewById(R.id.text_view_phone);
        textViewUrlWebsite = findViewById(R.id.text_view_url_website);
        btnAddToSchedule = findViewById(R.id.btn_add_to_schedule);
        imageViewBestPhoto = findViewById(R.id.image_view_best_photo);
        btnAddToSchedule = findViewById(R.id.btn_add_to_schedule);
        setIntialStateBtnAddToSchedule();
    }

    private void setIntialStateBtnAddToSchedule() {
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this.getBaseContext(), R.color.primary), PorterDuff.Mode.MULTIPLY);
        CheckVenuePresenceInScheduleService service = new CheckVenuePresenceInScheduleService(this, progressBar);
        service.makeRequest(venueId);

    }

    private void getVenueDetailsByIdFromServer(String venueId) {
        Log.i(TAG_LOGGING, "Getting venue details by id from server...");
        progressBar.setVisibility(View.VISIBLE);
        VenueDetailsService venueDetailsService = new VenueDetailsService(this, progressBar);
        venueDetailsService.makeRequest(venueId);
    }

    private void attemptAddToSchedule() {
        progressBar.setVisibility(View.VISIBLE);
        AddToScheduleService addToScheduleService = new AddToScheduleService(this, progressBar);
        addToScheduleService.makeRequest(venueId);
    }

    private void attemptRemoveFromSchedule() {
        progressBar.setVisibility(View.VISIBLE);
        RemoveFromScheduleService removeFromScheduleService = new RemoveFromScheduleService(this, progressBar);
        removeFromScheduleService.makeRequest(venueId);
    }

    public void changeBtnAddToScheduleStateBasedOnText() {
        Log.i(TAG_LOGGING, "Changing button add to schedule state");
        if (btnAddToSchedule.getText().toString().equals(addToScheduleText)) {
            changeBtnAddToScheduleState(true);
        } else {
            changeBtnAddToScheduleState(false);
        }
    }

    public void changeBtnAddToScheduleState(boolean present) {
        if (present) {
            btnAddToSchedule.setText(removeFromScheduleText);
            btnAddToSchedule.setTextColor(this.getBaseContext().getColor(R.color.red));
        } else {
            btnAddToSchedule.setText(addToScheduleText);
            btnAddToSchedule.setTextColor(this.getBaseContext().getColor(R.color.primary));
        }

    }
}
