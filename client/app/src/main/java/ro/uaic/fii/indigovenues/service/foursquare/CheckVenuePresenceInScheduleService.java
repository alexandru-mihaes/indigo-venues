package ro.uaic.fii.indigovenues.service.foursquare;

import android.app.Activity;
import android.util.Log;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.service.BaseService;
import ro.uaic.fii.indigovenues.ui.activity.VenueDetailsActivity;

public class CheckVenuePresenceInScheduleService extends BaseService {
    private static final String TAG_LOGGING = CheckVenuePresenceInScheduleService.class.getName();

    private final VenueDetailsActivity venueDetailsActivity;

    public CheckVenuePresenceInScheduleService(Activity activity, ProgressBar progressBar) {
        super(activity, progressBar, ServerUtils.getUrl(activity.getBaseContext(), "venues/schedule/is-present"));
        this.venueDetailsActivity = (VenueDetailsActivity) activity;
    }

    public void makeRequest(final String venueId) {
        super.setUrlString(ServerUtils.getUrl(activity.getBaseContext(), "venues/schedule/is-present") + "/" + venueId);
        Log.i(TAG_LOGGING, "Making GET request - Check if venue is present in schedule");
        super.makeRequest(
                Request.Method.GET,
                true,
                this.getClass()
        );
    }

    @Override
    public void handleSuccess(final JSONObject response) {
        super.handleSuccess(response);
        try {
            Boolean present = response.getBoolean("present");
            if (present) {
                venueDetailsActivity.changeBtnAddToScheduleState(true);
            }
        } catch (JSONException e) {
            Log.e(TAG_LOGGING, "Throwing InvalidVenuePresenceResponseException...");
        }
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}
