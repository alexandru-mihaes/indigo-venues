package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Timeframe implements Serializable {
    @SerializedName("days")
    private String days;

    @SerializedName("includesToday")
    private boolean includesToday;

    @SerializedName("open")
    private List<Open> open;

    public Timeframe() {
    }

    public Timeframe(String days, boolean includesToday, List<Open> open) {
        this.days = days;
        this.includesToday = includesToday;
        this.open = open;
    }

    public String getDays() {
        return days;
    }

    public boolean isIncludesToday() {
        return includesToday;
    }

    public List<Open> getOpen() {
        return open;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public void setIncludesToday(boolean includesToday) {
        this.includesToday = includesToday;
    }

    public void setOpen(List<Open> open) {
        this.open = open;
    }
}
