package ro.uaic.fii.indigovenues.exception;

public class InvalidAuthResponseException extends RuntimeException {
    public InvalidAuthResponseException(String message) {
        super(message);
    }

    public InvalidAuthResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
