package ro.uaic.fii.indigovenues.exception.foursquare;

public class InvalidRecommendedVenuesResponse extends RuntimeException {
    public InvalidRecommendedVenuesResponse(String message) {
        super(message);
    }

    public InvalidRecommendedVenuesResponse(String message, Throwable cause) {
        super(message, cause);
    }
}
