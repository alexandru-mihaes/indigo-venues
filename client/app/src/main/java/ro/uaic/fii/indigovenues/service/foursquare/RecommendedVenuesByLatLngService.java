package ro.uaic.fii.indigovenues.service.foursquare;

import android.app.Activity;
import android.util.Log;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ro.uaic.fii.indigovenues.exception.foursquare.InvalidRecommendedVenuesResponse;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Venue;
import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.service.BaseService;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment.ExploreFragment;
import ro.uaic.fii.indigovenues.ui.widget.VenueMapPoint;

public class RecommendedVenuesByLatLngService extends BaseService {
    private static final String TAG_LOGGING = RecommendedVenuesByLatLngService.class.getName();

    private final ExploreFragment exploreFragment;

    public RecommendedVenuesByLatLngService(Activity activity, ProgressBar progressBar, ExploreFragment exploreFragment) {
        super(activity, progressBar, ServerUtils.getUrl(activity.getBaseContext(), "venues/explore"));
        this.exploreFragment = exploreFragment;
    }

    public void makeRequest(final double lat, final double lng) {
        super.setUrlString(ServerUtils.getUrl(activity.getBaseContext(), "venues/explore") + "?ll=" + lat + "," + lng);
        Log.i(TAG_LOGGING, "Making GET request - recommended venues by lat and lng");
        super.makeRequest(
                Request.Method.GET,
                true,
                this.getClass()
        );
    }

    @Override
    public void handleSuccess(final JSONObject response) {
        super.handleSuccess(response);
        try {
            Log.i(TAG_LOGGING, "Converting response into object");
            JSONArray jsonArray = response.getJSONArray("venues");
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("M/d/yy hh:mm a");
            Gson gson = gsonBuilder.create();
            if (jsonArray.length() > 0) {
                List<Venue> venues = Arrays.asList(gson.fromJson(jsonArray.toString(), Venue[].class));
                placeVenuesOnTheMap(venues);
            }
        } catch (JSONException e) {
            Log.e(TAG_LOGGING, "Throwing InvalidAuthResponseException...");
            throw new InvalidRecommendedVenuesResponse("Invalid recommended venues response - " + e.getMessage());
        }
    }

    private void placeVenuesOnTheMap(List<Venue> venues) {
        Log.i(TAG_LOGGING, "Placing venues on the map");
        List<VenueMapPoint> venueMapPointListCurrent = exploreFragment.getVenueMapPointList();
        List<VenueMapPoint> venueMapPointListNew = new ArrayList<>();
        for (Venue venue : venues) {
            boolean alreadyPresent = false;
            for (VenueMapPoint venueMapPointCurrent : venueMapPointListCurrent) {
                if (venueMapPointCurrent.getName().equals(venue.getName())) {
                    Log.i(TAG_LOGGING, "Venue " + venue.getName() + " is already present on the map");
                    alreadyPresent = true;
                }
            }
            if (!alreadyPresent) {
                Log.i(TAG_LOGGING, "Placing venue: " + venue.getName());
                VenueMapPoint venueMapPoint = new VenueMapPoint(
                        exploreFragment.getMapView(),
                        exploreFragment.getMapboxMap(),
                        exploreFragment.requireActivity(),
                        exploreFragment.requireContext(),
                        venue,
                        venue.getName()
                );
                venueMapPointListCurrent.add(venueMapPoint);
            }
        }
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}
