package ro.uaic.fii.indigovenues.service.foursquare;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.List;

import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Open;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Timeframe;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.VenueDetails;
import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.service.BaseService;
import ro.uaic.fii.indigovenues.ui.activity.VenueDetailsActivity;

public class VenueDetailsService extends BaseService {
    private static final String TAG_LOGGING = VenueDetailsService.class.getName();

    private final VenueDetailsActivity venueDetailsActivity;

    public VenueDetailsService(Activity activity, ProgressBar progressBar) {
        super(activity, progressBar, ServerUtils.getUrl(activity.getBaseContext(), "venues"));
        this.venueDetailsActivity = (VenueDetailsActivity) activity;
    }

    public void makeRequest(final String venueId) {
        super.setUrlString(ServerUtils.getUrl(activity.getBaseContext(), "venues/") + venueId);
        Log.i(TAG_LOGGING, "Making GET request - venue details");
        super.makeRequest(
                Request.Method.GET,
                true,
                this.getClass()
        );
    }

    @Override
    public void handleSuccess(final JSONObject response) {
        super.handleSuccess(response);
        Log.i(TAG_LOGGING, "Converting response into object");
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        Gson gson = gsonBuilder.create();
        VenueDetails venueDetails = gson.fromJson(response.toString(), VenueDetails.class);
        updateVenueDetailsUi(venueDetails);
    }

    private void updateVenueDetailsUi(VenueDetails venueDetails) {
        Log.i(TAG_LOGGING, "Updating venue details ui widgets...");
        updateTitleUi(venueDetails);
        updateCategoryUi(venueDetails);
        updateLocationUi(venueDetails);
        updateAddressUi(venueDetails);
        updateHoursUi(venueDetails);
        updatePhoneUi(venueDetails);
        updateUrlWebsiteUi(venueDetails);
        updateBestPhotoUi(venueDetails.getId());
        Log.i(TAG_LOGGING, "Making relative_layout_wrapper_venue_details visible");
        venueDetailsActivity.getRelativeLayoutVenueDetailsWrapper().setVisibility(View.VISIBLE);

    }

    private void updateTitleUi(VenueDetails venueDetails) {
        TextView textViewTitle = venueDetailsActivity.getTextViewVenueTitle();
        try {
            String venueTitle = venueDetails.getName();
            textViewTitle.setText(venueTitle);
        } catch (NullPointerException e) {
            makeTextViewEmpty(textViewTitle);
        }
    }

    private void updateCategoryUi(VenueDetails venueDetails) {
        TextView textViewCategory = venueDetailsActivity.getTextViewCategory();
        try {
            String venueCategory = venueDetails.getCategories().get(0).getName();
            textViewCategory.setText(venueCategory);
        } catch (NullPointerException e) {
            makeTextViewEmpty(textViewCategory);
        }
    }

    private void updateLocationUi(VenueDetails venueDetails) {
        TextView textViewLocation = venueDetailsActivity.getTextViewLocation();
        try {
            String venueLocation = venueDetails.getLocation().getCity();
            textViewLocation.setText(venueLocation);
        } catch (NullPointerException e) {
            makeTextViewEmpty(textViewLocation);
        }
    }

    private void updateAddressUi(VenueDetails venueDetails) {
        TextView textViewAddress = venueDetailsActivity.getTextViewAddress();
        try {
            String venueAddress = venueDetails.getLocation().getAddress();
            textViewAddress.setText(venueAddress);
        } catch (NullPointerException e) {
            makeTextViewEmpty(textViewAddress);
        }
    }

    private void updateHoursUi(VenueDetails venueDetails) {
        TextView textViewHours = venueDetailsActivity.getTextViewHours();
        try {
            StringBuilder venueHours = new StringBuilder();
            List<Timeframe> timeframes = venueDetails.getHours().getTimeframes();
            for (Timeframe timeframe : timeframes) {
                venueHours.append(timeframe.getDays()).append(" - ");

                List<Open> opens = timeframe.getOpen();
                for (Open open : opens) {
                    venueHours.append(open.getRenderedTime());
                }
                venueHours.append("\n");
            }
            textViewHours.setText(venueHours.toString());
        } catch (NullPointerException e) {
            makeTextViewEmpty(textViewHours);
        }
    }

    private void updatePhoneUi(VenueDetails venueDetails) {
        TextView textViewPhone = venueDetailsActivity.getTextViewPhone();
        try {
            String venuePhone = venueDetails.getContact().getFormattedPhone();
            textViewPhone.setText(venuePhone);
        } catch (NullPointerException e) {
            makeTextViewEmpty(textViewPhone);
        }
    }

    private void updateUrlWebsiteUi(VenueDetails venueDetails) {
        TextView textViewUrlWebsite = venueDetailsActivity.getTextViewUrlWebsite();
        try {
            String venueUrlWebsite = venueDetails.getUrl();
            textViewUrlWebsite.setText(venueUrlWebsite);
        } catch (NullPointerException e) {
            makeTextViewEmpty(textViewUrlWebsite);
        }
    }

    private void makeTextViewEmpty(TextView textView) {
        textView.setText("");
    }

    private void updateBestPhotoUi(String venueId) {
        Log.i(TAG_LOGGING, "Getting venue's best photo from server...");
        VenueBestPhotoService venueBestPhotoService = new VenueBestPhotoService(venueDetailsActivity);
        venueBestPhotoService.makeRequest(venueId);
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}
