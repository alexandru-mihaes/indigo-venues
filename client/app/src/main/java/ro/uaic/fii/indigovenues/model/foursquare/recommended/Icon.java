package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Icon implements Serializable {
    @SerializedName("prefix")
    private String prefix;

    private static final String SIZE_32 = "bg_32";
    private static final String SIZE_44 = "bg_44";
    private static final String SIZE_64 = "bg_64";
    private static final String SIZE_88 = "bg_88";

    @SerializedName("suffix")
    private String suffix;

    public Icon() {
    }

    public Icon(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public String getPrefix() {
        return prefix;
    }

    public static String getSize32() {
        return SIZE_32;
    }

    public static String getSize44() {
        return SIZE_44;
    }

    public static String getSize64() {
        return SIZE_64;
    }

    public static String getSize88() {
        return SIZE_88;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}

