package ro.uaic.fii.indigovenues.network;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import ro.uaic.fii.indigovenues.exception.JsonKeyException;

public class VolleyErrorUtils {
    public static String getValueFromKeyOfJsonResponse(VolleyError error, String key) {
        NetworkResponse networkResponse = error.networkResponse;
        if (networkResponse != null && networkResponse.data != null) {
            String body = new String(networkResponse.data);
            try {
                JSONObject jsonObject = new JSONObject(body);
                return jsonObject.getString(key);
            } catch (JSONException e) {
                throw new JsonKeyException(e.getMessage());
            }
        }
        return "";
    }

    public static String getValueFromKeyOfJsonResponse(Response response, String key) {
        NetworkResponse networkResponse = response.error.networkResponse;
        if (networkResponse != null && networkResponse.data != null) {
            String body = new String(networkResponse.data);
            try {
                JSONObject jsonObject = new JSONObject(body);
                return jsonObject.getString(key);
            } catch (JSONException e) {
                throw new JsonKeyException(e.getMessage());
            }
        }
        return "";
    }
}
