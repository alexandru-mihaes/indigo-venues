package ro.uaic.fii.indigovenues.service.mainscreen;

import android.app.Activity;
import android.util.Log;
import android.util.Pair;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.service.BaseService;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment.ProfileFragment;
import ro.uaic.fii.indigovenues.ui.widget.CustomToast;
import ro.uaic.fii.indigovenues.ui.widget.CustomToastUtils;
import ro.uaic.fii.indigovenues.utils.PreferencesUtils;

public class ProfileService extends BaseService {
    private static final String TAG_LOGGING = ProfileService.class.getName();

    private final ProfileFragment profileFragment;
    private String preferencesUsername, preferencesEmail;

    public ProfileService(Activity activity, ProgressBar progressBar, ProfileFragment profileFragment) {
        super(activity, progressBar, ServerUtils.getUrl(activity.getBaseContext(), "users/update"));
        this.profileFragment = profileFragment;
    }

    public void makeRequest(final String username,
                            final String email,
                            final String oldPassword,
                            final String newPassword,
                            final String newPasswordConfirm) {
        if (username.isEmpty() || email.isEmpty() || oldPassword.isEmpty() || newPassword.isEmpty() || newPasswordConfirm.isEmpty()) {
            CustomToastUtils.pleaseFillTheFields(activity);
        } else if (!newPassword.equals(newPasswordConfirm)) {
            CustomToastUtils.passwordsDontMatch(activity);
        } else {
            preferencesUsername = username;
            preferencesEmail = email;
            super.makeRequest(
                    Request.Method.PUT,
                    true,
                    this.getClass(),
                    new Pair<>("username", username),
                    new Pair<>("email", email),
                    new Pair<>("oldPassword", oldPassword),
                    new Pair<>("newPassword", newPassword)
            );
        }
    }

    @Override
    public void handleSuccess(final JSONObject response) {
        super.handleSuccess(response);
        PreferencesUtils.saveUser(activity, preferencesUsername, preferencesEmail);
        Log.e(TAG_LOGGING, "Showing 'Successfully updated' CustomToast...");
        CustomToast.showToast(
                activity.getApplicationContext().getString(R.string.msg_successfully_updated),
                activity,
                true
        );
        profileFragment.moveWidgetsToDefaultState();
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}
