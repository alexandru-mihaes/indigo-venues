package ro.uaic.fii.indigovenues.service.foursquare;

import android.app.Activity;
import android.util.Log;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.service.BaseService;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment.ExploreFragment;
import ro.uaic.fii.indigovenues.ui.widget.CustomToastUtils;

public class RecommendedVenuesService extends BaseService {
    private static final String TAG_LOGGING = RecommendedVenuesService.class.getName();

    private final ExploreFragment exploreFragment;

    public RecommendedVenuesService(Activity activity, ProgressBar progressBar, ExploreFragment exploreFragment) {
        super(activity, progressBar, ServerUtils.getUrl(activity.getBaseContext(), "venues/explore"));
        this.exploreFragment = exploreFragment;
    }

    public void makeRequest(final String location) {
        if (location.isEmpty()) {
            Log.i(TAG_LOGGING, "Location parameter is empty");
            CustomToastUtils.pleaseFillTheFields(activity);
        } else {
            super.setUrlString(ServerUtils.getUrl(activity.getBaseContext(), "venues/explore") + "?location=" + location);
            Log.i(TAG_LOGGING, "Making GET request - recommended venues by location name");
            super.makeRequest(
                    Request.Method.GET,
                    true,
                    this.getClass()
            );
        }
    }

    @Override
    public void handleSuccess(final JSONObject response) {
        super.handleSuccess(response);
        Log.w(TAG_LOGGING, response.toString());
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}
