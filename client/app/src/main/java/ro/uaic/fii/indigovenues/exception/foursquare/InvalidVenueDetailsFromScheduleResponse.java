package ro.uaic.fii.indigovenues.exception.foursquare;

public class InvalidVenueDetailsFromScheduleResponse extends RuntimeException {
    public InvalidVenueDetailsFromScheduleResponse(String message) {
        super(message);
    }

    public InvalidVenueDetailsFromScheduleResponse(String message, Throwable cause) {
        super(message, cause);
    }
}
