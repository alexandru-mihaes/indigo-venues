package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Contact implements Serializable {
    @SerializedName("phone")
    private String phone;

    @SerializedName("formattedPhone")
    private String formattedPhone;

    public Contact() {
    }

    public Contact(String phone, String formattedPhone) {
        this.phone = phone;
        this.formattedPhone = formattedPhone;
    }

    public String getPhone() {
        return phone;
    }

    public String getFormattedPhone() {
        return formattedPhone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setFormattedPhone(String formattedPhone) {
        this.formattedPhone = formattedPhone;
    }
}
