package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Location implements Serializable {
    @SerializedName("venues")
    private String address;

    @SerializedName("lat")
    private Double lat;

    @SerializedName("lng")
    private Double lng;

    @SerializedName("country")
    private String country;

    @SerializedName("state")
    private String state;

    @SerializedName("city")
    private String city;

    public Location() {
    }

    public Location(String address, Double lat, Double lng, String country, String state, String city) {
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.country = country;
        this.state = state;
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
