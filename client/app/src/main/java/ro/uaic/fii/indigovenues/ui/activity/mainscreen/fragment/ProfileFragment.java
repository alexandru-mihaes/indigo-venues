package ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.service.mainscreen.ProfileService;
import ro.uaic.fii.indigovenues.ui.activity.SigninActivity;
import ro.uaic.fii.indigovenues.utils.PreferencesUtils;

public class ProfileFragment extends MainscreenFragment {
    private static final String TAG_LOGGING = ProfileFragment.class.getName();

    private View fragmentLayout;
    private EditText editTextUsername, editTextEmail;
    private EditText editTextOldPassword, editTextNewPassword, editTextNewPasswordConfirm;
    private Button btnEditOrSave, btnCancel;
    private boolean editingState;
    private ProgressBar progressBar;

    private ProfileService profileService;

    private View.OnClickListener editOrSaveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            attemptEditOrSave();
        }
    };

    private View.OnClickListener cancelClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            attemptCancel();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentLayout = inflater.inflate(R.layout.fragment_profile, container, false);
        setHasOptionsMenu(true);
        progressBar = fragmentLayout.findViewById(R.id.progress_bar);
        Log.i(TAG_LOGGING, "Changing ProgressBar color to primary...");
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.primary), PorterDuff.Mode.MULTIPLY);
        profileService = new ProfileService(getActivity(), progressBar, this);
        initializeBtns();
        initializeEditTexts();
        moveWidgetsToDefaultState();
        return fragmentLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        moveWidgetsToDefaultState();
    }

    private void initializeBtns() {
        Log.i(TAG_LOGGING, "Initializing buttons...");
        btnEditOrSave = fragmentLayout.findViewById(R.id.btn_edit_or_save);
        btnCancel = fragmentLayout.findViewById(R.id.btn_cancel);
        btnEditOrSave.setOnClickListener(editOrSaveClickListener);
        btnCancel.setOnClickListener(cancelClickListener);
        btnCancel.setEnabled(false);
    }

    private void initializeEditTexts() {
        Log.i(TAG_LOGGING, "Initializing edit texts...");
        editTextUsername = fragmentLayout.findViewById(R.id.edit_text_username);
        editTextEmail = fragmentLayout.findViewById(R.id.edit_text_email);
        editTextOldPassword = fragmentLayout.findViewById(R.id.edit_text_old_password);
        editTextNewPassword = fragmentLayout.findViewById(R.id.edit_text_password);
        editTextNewPasswordConfirm = fragmentLayout.findViewById(R.id.edit_text_new_password_confirm);
    }

    private void setEditTextsContent() {
        Log.i(TAG_LOGGING, "Setting edit texts content...");
        User user = PreferencesUtils.getUser(this.getActivity());
        editTextUsername.setText(user.getUsername());
        editTextEmail.setText(user.getEmail());
        editTextOldPassword.setText(R.string.title_password_star);
        editTextNewPassword.setText(R.string.title_password_star);
        editTextNewPasswordConfirm.setText(R.string.title_password_star);
    }

    private void setEditTextStateEnabled(boolean state) {
        Log.i(TAG_LOGGING, "Setting edit texts state as enabled...");
        editTextUsername.setEnabled(state);
        editTextEmail.setEnabled(state);
        editTextOldPassword.setEnabled(state);
        editTextNewPassword.setEnabled(state);
        editTextNewPasswordConfirm.setEnabled(state);
    }

    private void setEditTextsTextColor(int color) {
        Log.i(TAG_LOGGING, "Setting edit texts text color to " + color + "...");
        editTextUsername.setTextColor(color);
        editTextEmail.setTextColor(color);
        editTextOldPassword.setTextColor(color);
        editTextNewPassword.setTextColor(color);
        editTextNewPasswordConfirm.setTextColor(color);
    }

    private void attemptEditOrSave() {
        if (editingState) {
            Log.i(TAG_LOGGING, "Attempting edit...");
            profileService.makeRequest(
                    editTextUsername.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextOldPassword.getText().toString(),
                    editTextNewPassword.getText().toString(),
                    editTextNewPasswordConfirm.getText().toString()
            );
        } else {
            Log.i(TAG_LOGGING, "Attempting save...");
            moveWidgetsToSavingState();
        }
    }

    private void attemptCancel() {
        Log.i(TAG_LOGGING, "Attempting cancel...");
        editingState = false;
        moveWidgetsToDefaultState();
    }

    public void moveWidgetsToDefaultState() {
        Log.i(TAG_LOGGING, "Moving widgets to default state...");
        editingState = false;
        // set the states of the edit texts
        setEditTextsContent();
        setEditTextStateEnabled(false);
        setEditTextsTextColor(Color.GRAY);
        // set the states of the buttons
        Log.i(TAG_LOGGING, "Setting btnEditOrSave text to 'Edit'...");
        btnEditOrSave.setText(R.string.action_btn_edit);
        btnCancel.setEnabled(false);
    }

    public void moveWidgetsToSavingState() {
        editingState = true;
        // set the states of the edit texts
        editTextOldPassword.setText("");
        editTextNewPassword.setText("");
        editTextNewPasswordConfirm.setText("");
        setEditTextStateEnabled(true);
        setEditTextsTextColor(Color.BLACK);
        // set the states of the buttons
        Log.i(TAG_LOGGING, "Setting btnEditOrSave text to 'Save'...");
        btnEditOrSave.setText(R.string.action_btn_save);
        btnCancel.setEnabled(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.i(TAG_LOGGING, "Inflating profile_menu (logout)...");
        inflater.inflate(R.menu.profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG_LOGGING, "onOptionsItemSelected...");
        switch (item.getItemId()) {
            case R.id.action_logout:
                Log.i(TAG_LOGGING, "Logout item selected");
                PreferencesUtils.removeUser(getActivity());
                Intent signin = new Intent(getActivity(), SigninActivity.class);
                Log.i(TAG_LOGGING, "Starting SigninActivity...");
                startActivity(signin);
                break;

        }
        return true;
    }
}
