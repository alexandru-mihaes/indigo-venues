package ro.uaic.fii.indigovenues.exception;

public class VolleyRequestException extends RuntimeException {
    public VolleyRequestException(String message) {
        super(message);
    }

    public VolleyRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
