package ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;
import com.mapbox.mapboxsdk.location.OnCameraTrackingChangedListener;
import com.mapbox.mapboxsdk.location.OnLocationClickListener;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

import java.util.ArrayList;
import java.util.List;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.service.foursquare.RecommendedVenuesByLatLngService;
import ro.uaic.fii.indigovenues.ui.widget.CustomToast;
import ro.uaic.fii.indigovenues.ui.widget.VenueMapPoint;
import ro.uaic.fii.indigovenues.utils.LatLngDistanceCalculator;
import ro.uaic.fii.indigovenues.utils.PreferencesUtils;

public class ExploreFragment extends MainscreenFragment
        implements OnMapReadyCallback, OnLocationClickListener, PermissionsListener, OnCameraTrackingChangedListener {
    private static final String TAG_LOGGING = ExploreFragment.class.getName();

    private View fragmentLayout;
    private ProgressBar progressBar;

    /////////
    // Mapbox
    private MapView mapView;
    private MapboxMap mapboxMap;

    ////////////////////////////
    // Mapbox - change map style
    List<MenuItem> menuItemsStyles = new ArrayList<>();
    MenuItem menuItemCurrentStyle;

    ////////////////////////////
    // Mapbox - current location
    private PermissionsManager permissionsManager;
    private LocationComponent locationComponent;
    private FloatingActionButton floatingActionButton;
    private double previousLat, previousLng;

    //////////////////////////
    // Mapbox - venues markers
    private List<VenueMapPoint> venueMapPointList;

    public MapView getMapView() {
        return mapView;
    }

    public MapboxMap getMapboxMap() {
        return mapboxMap;
    }

    public List<VenueMapPoint> getVenueMapPointList() {
        return venueMapPointList;
    }

    public void setVenueMapPointList(List<VenueMapPoint> venueMapPointList) {
        this.venueMapPointList = venueMapPointList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG_LOGGING, "Getting mapbox instance...");
        Mapbox.getInstance(requireContext(), getString(R.string.mapbox_access_token));
        fragmentLayout = inflater.inflate(R.layout.fragment_explore, container, false);
        progressBar = fragmentLayout.findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.primary), PorterDuff.Mode.MULTIPLY);
        Boolean isInTrackingMode = PreferencesUtils.getTrackingMode(requireActivity());
        floatingActionButton = fragmentLayout.findViewById(R.id.back_to_camera_tracking_mode);
        if (isInTrackingMode) {
            changeFloatingActionButtonState(floatingActionButton, true);
        } else {
            changeFloatingActionButtonState(floatingActionButton, false);
        }
        mapView = fragmentLayout.findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        setHasOptionsMenu(true);
        return fragmentLayout;
    }

    /////////
    // Mapbox
    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        Log.i(TAG_LOGGING, "Setting the map style...");
        String mapStyle = PreferencesUtils.getMapStyle(requireActivity());
        mapboxMap.setStyle(mapStyle, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                enableLocationComponent(style);
            }
        });
        final MapboxMap MAPBOX_MAP = mapboxMap;
        LatLng cameraCenter = mapboxMap.getCameraPosition().target;
        previousLat = cameraCenter.getLatitude();
        previousLng = cameraCenter.getLongitude();

        venueMapPointList = new ArrayList<>();
        mapboxMap.addOnCameraMoveListener(new MapboxMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                // the server retrieves venues within 20000 meters
                LatLng cameraCenter = MAPBOX_MAP.getCameraPosition().target;
                double currentLat = cameraCenter.getLatitude();
                double currentLng = cameraCenter.getLongitude();
                double distance = LatLngDistanceCalculator.getDistance(previousLat, previousLng, currentLat, currentLng);
                Log.i(TAG_LOGGING, "Camera moved " + distance + " km");
                if (distance > 3.0) { // if the camera moved more then 3 km
                    Log.i(TAG_LOGGING, "Moved camera more than 3 km");
                    getNearVenues();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }


    ////////////////////////////
    // Mapbox - change map style
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.i(TAG_LOGGING, "Initializing map style group menu");
        inflater.inflate(R.menu.explore_menu, menu);
        menuItemsStyles.add(menu.findItem(R.id.action_light));
        menuItemsStyles.add(menu.findItem(R.id.action_dark));
        menuItemsStyles.add(menu.findItem(R.id.action_streets));
        menuItemsStyles.add(menu.findItem(R.id.action_outdoors));
        menuItemsStyles.add(menu.findItem(R.id.action_satellite));
        menuItemsStyles.add(menu.findItem(R.id.action_satellite_streets));
        String mapStyle = PreferencesUtils.getMapStyle(requireActivity());
        switch (mapStyle) {
            case Style.LIGHT:
                menuItemCurrentStyle = menu.findItem(R.id.action_light).setChecked(true).setEnabled(false);
                break;
            case Style.DARK:
                menuItemCurrentStyle = menu.findItem(R.id.action_dark).setChecked(true).setEnabled(false);
                break;
            case Style.MAPBOX_STREETS:
                menuItemCurrentStyle = menu.findItem(R.id.action_streets).setChecked(true).setEnabled(false);
                break;
            case Style.OUTDOORS:
                menuItemCurrentStyle = menu.findItem(R.id.action_outdoors).setChecked(true).setEnabled(false);
                break;
            case Style.SATELLITE:
                menuItemCurrentStyle = menu.findItem(R.id.action_satellite).setChecked(true).setEnabled(false);
                break;
            case Style.SATELLITE_STREETS:
                menuItemCurrentStyle = menu.findItem(R.id.action_satellite_streets).setChecked(true).setEnabled(false);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_light:
                Log.i(TAG_LOGGING, "Selected light map style action menu item");
                menuItemCurrentStyle = item;
                changeStateBtnsMapStyles();
                mapboxMap.setStyle(Style.LIGHT);
                PreferencesUtils.saveMapStyle(requireActivity(), Style.LIGHT);
                return true;
            case R.id.action_dark:
                Log.i(TAG_LOGGING, "Selected dark map style action menu item");
                menuItemCurrentStyle = item;
                changeStateBtnsMapStyles();
                mapboxMap.setStyle(Style.DARK);
                PreferencesUtils.saveMapStyle(requireActivity(), Style.DARK);
                return true;
            case R.id.action_streets:
                Log.i(TAG_LOGGING, "Selected streets map style action menu item");
                menuItemCurrentStyle = item;
                changeStateBtnsMapStyles();
                mapboxMap.setStyle(Style.MAPBOX_STREETS);
                PreferencesUtils.saveMapStyle(requireActivity(), Style.MAPBOX_STREETS);
                return true;
            case R.id.action_outdoors:
                Log.i(TAG_LOGGING, "Selected outdoors map style action menu item");
                menuItemCurrentStyle = item;
                changeStateBtnsMapStyles();
                mapboxMap.setStyle(Style.OUTDOORS);
                PreferencesUtils.saveMapStyle(requireActivity(), Style.OUTDOORS);
                return true;
            case R.id.action_satellite:
                Log.i(TAG_LOGGING, "Selected satellite map style action menu item");
                menuItemCurrentStyle = item;
                changeStateBtnsMapStyles();
                mapboxMap.setStyle(Style.SATELLITE);
                PreferencesUtils.saveMapStyle(requireActivity(), Style.SATELLITE);
                return true;
            case R.id.action_satellite_streets:
                Log.i(TAG_LOGGING, "Selected satellite streets map style action menu item");
                menuItemCurrentStyle = item;
                changeStateBtnsMapStyles();
                mapboxMap.setStyle(Style.SATELLITE_STREETS);
                PreferencesUtils.saveMapStyle(requireActivity(), Style.SATELLITE_STREETS);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void changeStateBtnsMapStyles() {
        Log.i(TAG_LOGGING, "Changing state buttons map styles");
        for (MenuItem menuItem : menuItemsStyles) {
            menuItem.setChecked(false).setEnabled(true);
        }
        menuItemCurrentStyle.setChecked(true).setEnabled(false);
    }

    ////////////////////////////
    // Mapbox - current location
    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        Log.i(TAG_LOGGING, "Enabling location component");
        if (PermissionsManager.areLocationPermissionsGranted(requireContext())) {
            LocationComponentOptions customLocationComponentOptions = LocationComponentOptions.builder(requireContext())
                    .elevation(5)
                    .accuracyAlpha(.6f)
                    .accuracyColor(R.color.primary)
                    .build();
            final LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(requireContext(), loadedMapStyle)
                            .locationComponentOptions(customLocationComponentOptions)
                            .build();
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            Boolean isInTrackingMode = PreferencesUtils.getTrackingMode(requireActivity());
            setInTrackingMode(locationComponent, isInTrackingMode);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            locationComponent.addOnLocationClickListener(this);
            locationComponent.addOnCameraTrackingChangedListener(this);

            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Boolean isInTrackingMode = PreferencesUtils.getTrackingMode(requireActivity());
                    if (!isInTrackingMode) {
                        setInTrackingMode(locationComponent, true);
                    } else {
                        setInTrackingMode(locationComponent, false);
                    }
                }
            });

        } else {
            Log.i(TAG_LOGGING, "Location component was not enabled because location permissions are not granted");
            permissionsManager = new PermissionsManager(this);
            Log.i(TAG_LOGGING, "Requesting location permissions");
            permissionsManager.requestLocationPermissions(requireActivity());
        }
        getNearVenues();
    }

    //////////////////////////
    // Mapbox - venues markers
    private void getNearVenues() {
        CameraPosition cameraPosition = mapboxMap.getCameraPosition();
        double cameraZoom = cameraPosition.zoom;
        LatLng cameraCenter = cameraPosition.target;
        if (cameraZoom >= 12.5) {
            Log.i(TAG_LOGGING, "Getting near venues. Camera zoom is big enough to make request");
            progressBar.setVisibility(View.VISIBLE);
            RecommendedVenuesByLatLngService recommendedVenuesByLatLngService = new RecommendedVenuesByLatLngService(getActivity(), progressBar, this);
            previousLat = cameraCenter.getLatitude();
            previousLng = cameraCenter.getLongitude();
            recommendedVenuesByLatLngService.makeRequest(cameraCenter.getLatitude(), cameraCenter.getLongitude());
        }
    }


    @SuppressLint("MissingPermission")
    private void setInTrackingMode(final LocationComponent locationComponent, boolean trackingMode) {
        if (trackingMode) {
            Log.i(TAG_LOGGING, "Set location component in tracking mode");
            locationComponent.setLocationComponentEnabled(true);
            PreferencesUtils.saveTrackingMode(requireActivity(), true);
            changeFloatingActionButtonState(floatingActionButton, true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    locationComponent.zoomWhileTracking(13f);
                }
            }, 1000);
        } else {
            Log.i(TAG_LOGGING, "Set location compnent out of tracking mode");
            locationComponent.setLocationComponentEnabled(false);
            PreferencesUtils.saveTrackingMode(requireActivity(), false);
            changeFloatingActionButtonState(floatingActionButton, false);
        }
    }

    private void changeFloatingActionButtonState(FloatingActionButton floatingActionButton, Boolean isInTrackingMode) {
        if (isInTrackingMode) {
            Log.i(TAG_LOGGING, "Floating action button in tracking mode");
            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(requireContext().getResources().getColor(R.color.primary)));
        } else {
            Log.i(TAG_LOGGING, "Flaoting action button out of tracking mode");
            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(requireContext().getResources().getColor(R.color.grey_shadow)));
        }
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onLocationComponentClick() {
        if (locationComponent.getLastKnownLocation() != null) {
            Log.i(TAG_LOGGING, requireContext().getString(R.string.current_location)
                    + ": " + locationComponent.getLastKnownLocation().getLatitude()
                    + " - " + locationComponent.getLastKnownLocation().getLongitude()
            );
        }
    }

    @Override
    public void onCameraTrackingDismissed() {
        Log.i(TAG_LOGGING, "Losing focus on tracking point");
    }

    @Override
    public void onCameraTrackingChanged(int currentMode) {
        // Empty on purpose
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG_LOGGING, "onRequestPermissionsResult");
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            Log.i(TAG_LOGGING, "Permission result is granted");
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        } else {
            Log.i(TAG_LOGGING, "Permission result is not granted");
            CustomToast.showToast(
                    requireContext().getString(R.string.user_location_permission_not_granted),
                    requireActivity(),
                    false
            );
        }
    }
}
