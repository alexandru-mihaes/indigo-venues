package ro.uaic.fii.indigovenues.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.mapbox.mapboxsdk.maps.Style;

import org.json.JSONException;
import org.json.JSONObject;

import ro.uaic.fii.indigovenues.exception.InvalidAuthResponseException;
import ro.uaic.fii.indigovenues.exception.PreferencesException;
import ro.uaic.fii.indigovenues.model.User;

public class PreferencesUtils {
    private static final String TAG_LOGGING = PreferencesUtils.class.getName();

    private static final String PREF_USER = "ro.uaic.fii.indigovenues.PREF_USER";
    private static final String PREF_KEY_USERNAME = "ro.uaic.fii.indigovenues.USERNAME";
    private static final String PREF_KEY_EMAIL = "ro.uaic.fii.indigovenues.EMAIL";
    private static final String PREF_KEY_ACCESS_TOKEN = "ro.uaic.fii.indigovenues.ACCESS_TOKEN";
    private static final String JSON_KEY_USERNAME = "username";
    private static final String JSON_KEY_EMAIL = "email";
    private static final String JSON_ACCESS_TOKEN = "accessToken";
    private static final String PREF_KEY_MAP_STYLE = "ro.uaic.fii.indigovenues.MAP_STYLE";
    private static final String PREF_KEY_TRACKING_MODE = "ro.uaic.fii.indigovenues.TRACKING_MODE";

    public static void saveUser(Activity activity, JSONObject response) {
        try {
            Log.i(TAG_LOGGING, "Saving user data...");
            String username = response.getString(JSON_KEY_USERNAME);
            String email = response.getString(JSON_KEY_EMAIL);
            String accessToken = response.getString(JSON_ACCESS_TOKEN);
            SharedPreferences preferences = activity.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(PREF_KEY_USERNAME, username);
            edit.putString(PREF_KEY_EMAIL, email);
            edit.putString(PREF_KEY_ACCESS_TOKEN, accessToken);
            edit.apply();
        } catch (JSONException e) {
            Log.e(TAG_LOGGING, "Throwing InvalidAuthResponseException...");
            throw new InvalidAuthResponseException("Invalid authentication response - " + e.getMessage());
        }
    }

    public static void saveUser(Activity activity, String username, String email) {
        Log.i(TAG_LOGGING, "Saving user data...");
        SharedPreferences preferences = activity.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(PREF_KEY_USERNAME, username);
        edit.putString(PREF_KEY_EMAIL, email);
        edit.apply();
    }

    public static User getUser(Activity activity) throws PreferencesException {
        Log.i(TAG_LOGGING, "Getting user data...");
        SharedPreferences prefs = activity.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        String username = prefs.getString(PREF_KEY_USERNAME, "");
        String email = prefs.getString(PREF_KEY_EMAIL, "");
        String accessToken = prefs.getString(PREF_KEY_ACCESS_TOKEN, "");
        if (username == null || email == null || accessToken == null || username.isEmpty() || email.isEmpty() || accessToken.isEmpty()) {
            Log.w(TAG_LOGGING, "Empty user data. Throwing PreferencesException...");
            throw new PreferencesException("Empty user data");
        }
        Log.i(TAG_LOGGING, "User data found");
        return new User(username, email, accessToken);
    }

    public static String getToken(Activity activity) throws PreferencesException {
        Log.i(TAG_LOGGING, "Getting token...");
        SharedPreferences prefs = activity.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        String accessToken = prefs.getString(PREF_KEY_ACCESS_TOKEN, "");
        if (accessToken == null || accessToken.isEmpty()) {
            Log.w(TAG_LOGGING, "Empty user data. Throwing PreferencesException...");
            throw new PreferencesException("Empty user data");
        }
        Log.i(TAG_LOGGING, "Token found");
        return accessToken;
    }

    public static void removeUser(Activity activity) throws PreferencesException {
        Log.i(TAG_LOGGING, "Removing user data...");
        SharedPreferences prefs = activity.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(PREF_KEY_USERNAME);
        editor.remove(PREF_KEY_EMAIL);
        editor.remove(PREF_KEY_ACCESS_TOKEN);
        editor.apply();
    }

    public static void saveTrackingMode(Activity activity, Boolean isInTrackingMode) {
        Log.i(TAG_LOGGING, "Saving tracking mode...");
        SharedPreferences preferences = activity.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(PREF_KEY_TRACKING_MODE, isInTrackingMode);
        edit.apply();
    }

    public static Boolean getTrackingMode(Activity activity) throws PreferencesException {
        Log.i(TAG_LOGGING, "Getting tracking mode...");
        SharedPreferences prefs = activity.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        Boolean isInTrackingMode = prefs.getBoolean(PREF_KEY_TRACKING_MODE, false);
        return isInTrackingMode;
    }

    public static void saveMapStyle(Activity activity, String mapStyle) {
        Log.i(TAG_LOGGING, "Saving map style...");
        SharedPreferences preferences = activity.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(PREF_KEY_MAP_STYLE, mapStyle);
        edit.apply();
    }

    public static String getMapStyle(Activity activity) {
        Log.i(TAG_LOGGING, "Getting map style...");
        SharedPreferences prefs = activity.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        return prefs.getString(PREF_KEY_MAP_STYLE, Style.LIGHT);
    }
}
