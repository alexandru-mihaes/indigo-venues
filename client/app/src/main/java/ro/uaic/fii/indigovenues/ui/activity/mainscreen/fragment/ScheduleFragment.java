package ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Location;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.VenueDetails;
import ro.uaic.fii.indigovenues.service.foursquare.VenueDetailsFromScheduleService;
import ro.uaic.fii.indigovenues.simulatedannealing.Itinerary;
import ro.uaic.fii.indigovenues.simulatedannealing.SimulatedAnnealing;
import ro.uaic.fii.indigovenues.simulatedannealing.VenueSA;
import ro.uaic.fii.indigovenues.ui.activity.VenueDetailsActivity;
import ro.uaic.fii.indigovenues.utils.LatLngDistanceCalculator;

public class ScheduleFragment extends MainscreenFragment {
    private static final String TAG_LOGGING = ScheduleFragment.class.getName();

    private View fragmentLayout;
    private LayoutInflater layoutInflater;
    private Activity activity;
    private ProgressBar progressBar;
    private List<VenueDetails> bestVenueDetailsList;
    boolean bestItineraryGenerated;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentLayout = inflater.inflate(R.layout.fragment_schedule, container, false);
        this.layoutInflater = inflater;
        this.activity = getActivity();
        this.progressBar = fragmentLayout.findViewById(R.id.progress_bar);
        this.bestItineraryGenerated = false;
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.primary), PorterDuff.Mode.MULTIPLY);
        updateListViewVenues();
        return fragmentLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        ListView listViewVenues = fragmentLayout.findViewById(R.id.list_view_venues);
        if (this.bestItineraryGenerated) {
            CustomAdapter customAdapter = new CustomAdapter(bestVenueDetailsList, true);
            listViewVenues.setAdapter(customAdapter);
        }
    }

    private void updateListViewVenues() {
        progressBar.setVisibility(View.VISIBLE);
        VenueDetailsFromScheduleService venueDetailsFromScheduleService = new VenueDetailsFromScheduleService(getActivity(), progressBar, this);
        venueDetailsFromScheduleService.makeRequest();
    }

    public void createCustomAdapter(List<VenueDetails> venueDetailsList) {
        this.bestItineraryGenerated = true;
        ListView listViewVenues = fragmentLayout.findViewById(R.id.list_view_venues);
        CustomAdapter customAdapter = new CustomAdapter(venueDetailsList, false);
        listViewVenues.setAdapter(customAdapter);
    }


    class CustomAdapter extends BaseAdapter {
        private List<VenueDetails> venueDetailsList;
        private DecimalFormat decimalFormat;

        public CustomAdapter(List<VenueDetails> venueDetailsList, boolean bestItineraryGenerated) {
            decimalFormat = new DecimalFormat("#.#");
            if (bestItineraryGenerated) {
                this.venueDetailsList = venueDetailsList;
            } else {
                List<VenueDetails> venueDetailsSorted = createBestItinerary(venueDetailsList);
                this.venueDetailsList = venueDetailsSorted;
                bestVenueDetailsList = venueDetailsSorted;
            }
        }

        @Override
        public int getCount() {
            return venueDetailsList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final VenueDetails venueDetails = venueDetailsList.get(position);
            Log.i(TAG_LOGGING, "Creating venue item: " + venueDetails.getName());
            convertView = layoutInflater.inflate(R.layout.venue_item_layout, null);
            TextView textViewVenueName = convertView.findViewById(R.id.text_view_venue_name);
            textViewVenueName.setText(venueDetails.getName());
            TextView textViewDistance = convertView.findViewById(R.id.text_view_distance);
            if (position == venueDetailsList.size() - 1) { // the last item does not have a next destination
                RelativeLayout rowDistance = convertView.findViewById(R.id.row_distance);
                rowDistance.setVisibility(View.GONE);
            } else {
                VenueDetails venueDetailsDestination = venueDetailsList.get(position + 1);
                double startLat = venueDetails.getLocation().getLat();
                double startLng = venueDetails.getLocation().getLng();
                double destinationLat = venueDetailsDestination.getLocation().getLat();
                double destinationLng = venueDetailsDestination.getLocation().getLng();
                Double distance = LatLngDistanceCalculator.getDistance(startLat, startLng, destinationLat, destinationLng);
                textViewDistance.setText(decimalFormat.format(distance) + " KM");
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent venueDetailsIntent = new Intent(fragmentLayout.getContext(), VenueDetailsActivity.class);
                    venueDetailsIntent.putExtra(VenueDetailsActivity.VENUE_ID_KEY, venueDetails.getId());
                    Log.i(TAG_LOGGING, "Starting VenueDetailsActivity");
                    activity.startActivity(venueDetailsIntent);
                }
            });
            return convertView;
        }
    }

    private List<VenueDetails> createBestItinerary(List<VenueDetails> venueDetailsList) {
        Log.i(TAG_LOGGING, "Creating best itinerary");
        List<VenueSA> initialVenuesSAstate = new ArrayList<>();
        for (VenueDetails venueDetails : venueDetailsList) {
            Location venueLocation = venueDetails.getLocation();
            VenueSA venueSA = new VenueSA(venueDetails.getName(), venueLocation.getLat(), venueLocation.getLng());
            initialVenuesSAstate.add(venueSA);
        }
        Itinerary bestItinerary = SimulatedAnnealing.run(initialVenuesSAstate);
        List<VenueSA> bestVenuesSA = bestItinerary.getVenuesSA();
        List<VenueDetails> besteVenueDetailsList = new ArrayList<>();
        for (VenueSA venueSA : bestVenuesSA) {
            String venueName = venueSA.getName();
            int venueDetailsIndexToBeFound = 0;
            for (int venueDetailsIndex = 0; venueDetailsIndex < venueDetailsList.size(); venueDetailsIndex++) {
                VenueDetails venueDetails = venueDetailsList.get(venueDetailsIndex);
                if (venueDetails.getName().equals(venueName)) {
                    venueDetailsIndexToBeFound = venueDetailsIndex;
                    break;
                }
            }
            VenueDetails venueDetailsToBeFound = venueDetailsList.get(venueDetailsIndexToBeFound);
            besteVenueDetailsList.add(venueDetailsToBeFound);
        }
        return besteVenueDetailsList;
    }
}
