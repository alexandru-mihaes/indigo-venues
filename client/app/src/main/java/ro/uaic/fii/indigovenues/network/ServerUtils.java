package ro.uaic.fii.indigovenues.network;

import android.content.Context;

import ro.uaic.fii.indigovenues.R;

public class ServerUtils {
    public static String getUrl(Context context, String path) {
        return context.getResources().getString(R.string.server_domain)
                + ":" + context.getResources().getString(R.string.server_port)
                + "/api/" + path;
    }
}
