package ro.uaic.fii.indigovenues.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import ro.uaic.fii.indigovenues.R;

public class CustomToastUtils {
    private static final String TAG_LOGGING = CustomToastUtils.class.getName();

    public static void pleaseFillTheField(Activity activity) {
        Log.w(TAG_LOGGING, "Showing 'Please fill the field' CustomToast...");
        Context context = activity.getBaseContext();
        CustomToast.showToast(context.getString(R.string.error_fill_the_field), activity, false);
    }

    public static void pleaseFillTheFields(Activity activity) {
        Log.w(TAG_LOGGING, "Showing 'Please fill the fields' CustomToast...");
        Context context = activity.getBaseContext();
        CustomToast.showToast(context.getString(R.string.error_fill_the_fields), activity, false);
    }

    public static void passwordsDontMatch(Activity activity) {
        Log.w(TAG_LOGGING, "Showing 'The entered passwords don't match!' CustomToast...");
        Context context = activity.getBaseContext();
        CustomToast.showToast(context.getString(R.string.error_passwords_dont_match), activity, false);
    }
}
