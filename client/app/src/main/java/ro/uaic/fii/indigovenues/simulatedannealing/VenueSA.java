package ro.uaic.fii.indigovenues.simulatedannealing;

import ro.uaic.fii.indigovenues.utils.LatLngDistanceCalculator;

public class VenueSA {
    String name;
    double lat;
    double lng;

    public VenueSA(String name, double lat, double lng) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double distanceTo(VenueSA venueSA) {
        return LatLngDistanceCalculator.getDistance(this, venueSA);
    }

    protected VenueSA getClone() {
        return new VenueSA(this.getName(), this.getLat(), this.getLng());
    }

    @Override
    public String toString() {
        return getName() + " (" + getLat() + ", " + getLng() + ")";
    }
}