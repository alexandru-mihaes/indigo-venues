package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Category implements Serializable {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("pluralName")
    private String pluralName;

    @SerializedName("shortName")
    private String shortName;

    @SerializedName("icon")
    private Icon icon;

    @SerializedName("primary")
    private Boolean primary;

    public Category() {
    }

    public Category(String id, String name, String pluralName, String shortName, Icon icon, Boolean primary) {
        this.id = id;
        this.name = name;
        this.pluralName = pluralName;
        this.shortName = shortName;
        this.icon = icon;
        this.primary = primary;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPluralName() {
        return pluralName;
    }

    public String getShortName() {
        return shortName;
    }

    public Icon getIcon() {
        return icon;
    }

    public Boolean getPrimary() {
        return primary;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPluralName(String pluralName) {
        this.pluralName = pluralName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }
}