package ro.uaic.fii.indigovenues.simulatedannealing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Itinerary {
    private List<VenueSA> venuesSA = new ArrayList<>();
    private int distance = 0;

    public Itinerary() {
        for (int venueIndex = 0; venueIndex < ItineraryManager.numberOfVenuesSA(); venueIndex++) {
            venuesSA.add(null);
        }
    }

    public Itinerary(List<VenueSA> venuesSA) {
        List<VenueSA> clone = new ArrayList<>();
        for (VenueSA venueSA : venuesSA) {
            clone.add(venueSA.getClone());
        }
        this.venuesSA = clone;
    }

    public List<VenueSA> getVenuesSA() {
        return venuesSA;
    }

    public int itinerarySize() {
        return venuesSA.size();
    }

    public VenueSA getVenueSA(int venuePosition) {
        return venuesSA.get(venuePosition);
    }

    public void setVenueSA(int venueIndex, VenueSA venueSA) {
        venuesSA.set(venueIndex, venueSA);
        // If the venuesSA container has been altered we need to reset the fitness and distance
        distance = 0;
    }

    public void generateIndividual() {
        for (int venueIndex = 0; venueIndex < ItineraryManager.numberOfVenuesSA(); venueIndex++) {
            setVenueSA(venueIndex, ItineraryManager.getVenueSA(venueIndex));
        }
        Collections.shuffle(venuesSA);
    }

    public int getDistance() {
        if (distance == 0) {
            int itineraryDistance = 0;
            for (int venueIndex = 0; venueIndex < itinerarySize() - 1; venueIndex++) {
                VenueSA fromVenueSA = getVenueSA(venueIndex);
                /* Unlike the default approach of the traveling salesman problem,
                we will not take into account the distance from the final venue
                to the start venue */
                VenueSA destinationVenueSA = getVenueSA(venueIndex + 1);
                itineraryDistance += fromVenueSA.distanceTo(destinationVenueSA);
            }
            distance = itineraryDistance;
        }
        return distance;
    }

    @Override
    public String toString() {
        String chromosome = "";
        for (int i = 0; i < itinerarySize(); i++) {
            String gene = getVenueSA(i).toString();
            chromosome += gene + " -> ";
        }
        return chromosome;
    }
}
