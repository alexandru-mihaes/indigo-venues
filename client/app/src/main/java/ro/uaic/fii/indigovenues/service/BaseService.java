package ro.uaic.fii.indigovenues.service;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.exception.JsonKeyException;
import ro.uaic.fii.indigovenues.exception.MethodInvokeException;
import ro.uaic.fii.indigovenues.network.VolleyErrorUtils;
import ro.uaic.fii.indigovenues.network.VolleyRequest;
import ro.uaic.fii.indigovenues.ui.widget.CustomToast;

public abstract class BaseService {
    private static final String TAG_LOGGING = BaseService.class.getName();

    protected final Activity activity;
    protected final ProgressBar progressBar;
    protected String urlString;

    public BaseService(Activity activity, ProgressBar progressBar, String urlString) {
        this.activity = activity;
        this.progressBar = progressBar;
        this.urlString = urlString;
    }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

    public void makeRequest(int requestMethod, boolean sendToken, Class classHandler, Pair<String, String>... jsonPairs) {
        try {
            JSONObject jsonObject = new JSONObject();
            for (Pair<String, String> pair : jsonPairs) {
                jsonObject.put(pair.first, pair.second);
            }

            VolleyRequest volleyRequest = new VolleyRequest(
                    activity,
                    urlString,
                    requestMethod,
                    jsonObject,
                    sendToken
            );

            // setting the success & error methods
            volleyRequest.setObjectToInvokeOn(this);
            Class[] successParameters = new Class[1];
            successParameters[0] = JSONObject.class;
            Method successLoginMethod = classHandler.getMethod("handleSuccess", successParameters);
            Class[] errorParameters = new Class[1];
            errorParameters[0] = VolleyError.class;
            Method errorLoginMethod = classHandler.getMethod("handleError", errorParameters);
            volleyRequest.setOnSuccessMethod(successLoginMethod);
            volleyRequest.setOnErrorMethod(errorLoginMethod);

            Log.i(TAG_LOGGING, "Making ProgressBar visible...");
            progressBar.setVisibility(View.VISIBLE);
            String requestMethodString;
            switch (requestMethod) {
                case 0:
                    requestMethodString = "GET";
                    break;
                case 1:
                    requestMethodString = "POST";
                    break;
                case 2:
                    requestMethodString = "PUT";
                    break;
                case 3:
                    requestMethodString = "DELETE";
                    break;
                default:
                    requestMethodString = "GET";
            }
            Log.i(TAG_LOGGING, "Invoking volley request" +
                    " - " + urlString +
                    " - " + requestMethodString
                    + " - " + jsonObject
                    + " - sendToken " + sendToken);
            volleyRequest.invokeRequest();
        } catch (NoSuchMethodException e) {
            Log.e(TAG_LOGGING, "Throwing MethodInvokeException...");
            throw new MethodInvokeException(e.getMessage());
        } catch (JSONException e) {
            Log.e(TAG_LOGGING, "Throwing JsonKeyException...");
            throw new JsonKeyException(e.getMessage());
        }
    }

    protected void handleSuccess(JSONObject response) {
        Log.i(TAG_LOGGING, "Making ProgressBar invisible...");
        progressBar.setVisibility(View.GONE);
    }

    protected void handleError(VolleyError error) {
        Log.w(TAG_LOGGING, "Default handleError...");
        progressBar.setVisibility(View.GONE);
        Context context = activity.getBaseContext();
        if (error instanceof NoConnectionError) {
            CustomToast.showToast(context.getString(R.string.error_no_internet_connection), activity, false);
        } else if (error instanceof TimeoutError || error instanceof ServerError) {
            String responseMessage = VolleyErrorUtils.getValueFromKeyOfJsonResponse(error, "message");
            if (responseMessage.isEmpty()) {
                CustomToast.showToast(context.getString(R.string.error_server), activity, false);
            } else {
                CustomToast.showToast(responseMessage, activity, false);
            }
        } else if (error instanceof NetworkError) {
            CustomToast.showToast(context.getString(R.string.error_network), activity, false);
        } else if (error instanceof ParseError) {
            CustomToast.showToast(context.getString(R.string.error_internal_server), activity, false);
        } else if (error instanceof AuthFailureError) {
            CustomToast.showToast(context.getString(R.string.error_login_invalid_credentials), activity, false);
        }
    }
}
