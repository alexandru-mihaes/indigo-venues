package ro.uaic.fii.indigovenues.utils;

import ro.uaic.fii.indigovenues.simulatedannealing.VenueSA;

public class LatLngDistanceCalculator {
    public static double getDistance(VenueSA venueSAStart, VenueSA venueSADestination) {
        return getDistance(venueSAStart.getLat(), venueSAStart.getLng(), venueSADestination.getLat(), venueSADestination.getLng());
    }

    public static double getDistance(double lat1, double lng1, double lat2, double lng) {
        if ((lat1 == lat2) && (lng1 == lng)) {
            return 0;
        } else {
            double theta = lng1 - lng;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344; // KM
            return (dist);
        }
    }
}
