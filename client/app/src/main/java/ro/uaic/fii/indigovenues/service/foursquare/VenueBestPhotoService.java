package ro.uaic.fii.indigovenues.service.foursquare;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.ui.activity.VenueDetailsActivity;

public class VenueBestPhotoService {
    private static final String TAG_LOGGING = VenueBestPhotoService.class.getName();

    private final VenueDetailsActivity venueDetailsActivity;

    public VenueBestPhotoService(Activity activity) {
        this.venueDetailsActivity = (VenueDetailsActivity) activity;
    }

    public void makeRequest(final String venueId) {
        final String bestPhotoUrl = ServerUtils.getUrl(venueDetailsActivity.getBaseContext(), "venues/best-photo/") + venueId;
        Log.i(TAG_LOGGING, "Creating a request for the venue's best photo " + bestPhotoUrl);
        RequestQueue requestQueue = Volley.newRequestQueue(venueDetailsActivity.getBaseContext());
        ImageRequest imageRequest = new ImageRequest(
                bestPhotoUrl,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        Log.i(TAG_LOGGING, "Response for the venue's best photo " + bestPhotoUrl);
                        ImageView imageViewBestPhoto = venueDetailsActivity.getImageViewBestPhoto();
                        int corenrRadius = 50;
                        response = getRoundedCornerBitmap(response, corenrRadius);
                        imageViewBestPhoto.setImageBitmap(response);
                    }
                },
                0, // Image width
                0, // Image height
                ImageView.ScaleType.CENTER_CROP,
                Bitmap.Config.RGB_565, // Image decode configuration
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG_LOGGING, "Cannot retrieve the venue's best photo from the following url: " + bestPhotoUrl);
                    }
                }
        );
        requestQueue.add(imageRequest);
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final int color = 0xffffffff;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}