package ro.uaic.fii.indigovenues.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.exception.PreferencesException;
import ro.uaic.fii.indigovenues.service.authentication.LoginService;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.MainscreenActivity;
import ro.uaic.fii.indigovenues.utils.PreferencesUtils;

public class SigninActivity extends AppCompatActivity {
    private static final String TAG_LOGGING = SigninActivity.class.getName();

    private Button btnLogin;
    private ProgressBar progressBar;
    private TextView textViewSignUpHere;

    private View.OnClickListener loginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            attemptLogin();
        }
    };

    private View.OnClickListener signUpHereListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startSignUpActivity();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean alreadyAuthenticated = true;
        try {
            PreferencesUtils.getUser(this);
            Intent mainscreen = new Intent(this, MainscreenActivity.class);
            Log.i(TAG_LOGGING, "Starting MainscreenActivity...");
            startActivity(mainscreen);
            finish();
        } catch (PreferencesException exception) {
            Log.w(TAG_LOGGING, "User is not already authenticated.");
            alreadyAuthenticated = false;
        }
        if (!alreadyAuthenticated) {
            setContentView(R.layout.activity_signin);

            btnLogin = findViewById(R.id.btn_login);
            progressBar = findViewById(R.id.progress_bar);
            progressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
            btnLogin.setOnClickListener(loginClickListener);

            textViewSignUpHere = findViewById(R.id.text_view_sign_up_here);
            textViewSignUpHere.setOnClickListener(signUpHereListener);
        }
    }

    private void attemptLogin() {
        Log.i(TAG_LOGGING, "Attempting login...");
        EditText editTextUsername = findViewById(R.id.edit_text_username);
        EditText editTextPassword = findViewById(R.id.edit_text_password);

        LoginService loginService = new LoginService(this, progressBar);
        loginService.makeRequest(
                editTextUsername.getText().toString(),
                editTextPassword.getText().toString()
        );
    }

    private void startSignUpActivity() {
        Intent signUp = new Intent(this, SignupActivity.class);
        Log.i(TAG_LOGGING, "Starting SignupActivity...");
        startActivity(signUp);
    }
}