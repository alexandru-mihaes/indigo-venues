package ro.uaic.fii.indigovenues.service.foursquare;

import android.app.Activity;
import android.util.Log;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.service.BaseService;
import ro.uaic.fii.indigovenues.ui.activity.VenueDetailsActivity;

public class RemoveFromScheduleService extends BaseService {
    private static final String TAG_LOGGING = RemoveFromScheduleService.class.getName();

    private final VenueDetailsActivity venueDetailsActivity;

    public RemoveFromScheduleService(Activity activity, ProgressBar progressBar) {
        super(activity, progressBar, ServerUtils.getUrl(activity.getBaseContext(), "venues/schedule"));
        this.venueDetailsActivity = (VenueDetailsActivity) activity;
    }

    public void makeRequest(final String venueId) {
        super.setUrlString(ServerUtils.getUrl(activity.getBaseContext(), "venues/schedule") + "/" + venueId);
        Log.i(TAG_LOGGING, "Making PUT request - remove venue from schedule");
        super.makeRequest(
                Request.Method.DELETE,
                true,
                this.getClass()
        );
    }

    @Override
    public void handleSuccess(final JSONObject response) {
        super.handleSuccess(response);
        venueDetailsActivity.changeBtnAddToScheduleStateBasedOnText();
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}
