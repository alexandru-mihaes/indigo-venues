package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Hours implements Serializable {
    @SerializedName("status")
    private String status;

    @SerializedName("isOpen")
    private boolean isOpen;

    @SerializedName("timeframes")
    private List<Timeframe> timeframes;

    public Hours() {
    }

    public Hours(String status, boolean isOpen, List<Timeframe> timeframes) {
        this.status = status;
        this.isOpen = isOpen;
        this.timeframes = timeframes;
    }

    public String getStatus() {
        return status;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public List<Timeframe> getTimeframes() {
        return timeframes;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public void setTimeframes(List<Timeframe> timeframes) {
        this.timeframes = timeframes;
    }
}
