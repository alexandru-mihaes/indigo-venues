package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RecommendedVenuesResponse implements Serializable {
    @SerializedName("venues")
    private List<Venue> venues;

    public RecommendedVenuesResponse() {
    }

    public RecommendedVenuesResponse(List<Venue> venues) {
        this.venues = venues;
    }

    public List<Venue> getVenues() {
        return venues;
    }

    public void setVenues(List<Venue> venues) {
        this.venues = venues;
    }
}
