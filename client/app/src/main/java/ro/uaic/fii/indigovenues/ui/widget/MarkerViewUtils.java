package ro.uaic.fii.indigovenues.ui.widget;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerView;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Category;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Icon;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Location;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Venue;
import ro.uaic.fii.indigovenues.ui.activity.VenueDetailsActivity;

public class MarkerViewUtils {
    private static final String TAG_LOGGING = MarkerViewUtils.class.getName();

    /**
     * Creates a custom animation view
     */
    public static View getViewVenue(final Activity activity, final Context context, final Venue venue) {
        Log.i(TAG_LOGGING, "Creating view venue");
        final View viewVenue = LayoutInflater.from(context).inflate(R.layout.view_venue, null);
        viewVenue.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        final ImageView categoryIcon = viewVenue.findViewById(R.id.image_view_category_icon);
        changeCategoryIcon(context, categoryIcon, venue.getCategories().get(0));
        final FrameLayout animationView = viewVenue.findViewById(R.id.animation_layout);
        final TextView venueTitle = viewVenue.findViewById(R.id.text_view_venue_name);
        venueTitle.setText(venue.getName());
        final TextView venueId = viewVenue.findViewById(R.id.text_view_venue_id);
        venueId.setText(venue.getId());
        createCategoryIconListener(categoryIcon, viewVenue, context, activity, venueTitle, animationView);
        return viewVenue;
    }

    private static void changeCategoryIcon(Context context, final ImageView categoryIcon, Category category) {
        final String iconUrl = category.getIcon().getPrefix() + Icon.getSize32() + category.getIcon().getSuffix();
        Log.i(TAG_LOGGING, "Creating a request for the icon category " + iconUrl);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        ImageRequest imageRequest = new ImageRequest(
                iconUrl,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        Log.i(TAG_LOGGING, "Response for icon category " + iconUrl);
                        categoryIcon.setImageBitmap(response);
                    }
                },
                0, // Image width
                0, // Image height
                ImageView.ScaleType.CENTER_CROP,
                Bitmap.Config.RGB_565, // Image decode configuration
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG_LOGGING, "Cannot retrieve the icon category from the following url: " + iconUrl);
                    }
                }
        );
        requestQueue.add(imageRequest);
    }

    private static void createCategoryIconListener(ImageView categoryIcon, final View viewVenue, final Context context, final Activity activity, final TextView venueTitle, final FrameLayout animationView) {
        categoryIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG_LOGGING, "Icon category clicked");
                final View categoryIcon = viewVenue.findViewById(R.id.image_view_category_icon);
                // getting the venue title width
                int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                int heightSpec = View.MeasureSpec.makeMeasureSpec(venueTitle.getHeight(), View.MeasureSpec.EXACTLY);
                venueTitle.measure(widthSpec, heightSpec);
                int venueTitleWidth = venueTitle.getMeasuredWidth();
                // creating the animation
                ValueAnimator anim = ValueAnimator.ofInt(animationView.getMeasuredWidth(), categoryIcon.getWidth() + venueTitleWidth);
                anim.setInterpolator(new AccelerateDecelerateInterpolator());
                anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        int val = (int) valueAnimator.getAnimatedValue();
                        ViewGroup.LayoutParams layoutParams = animationView.getLayoutParams();
                        layoutParams.width = val;
                        animationView.setLayoutParams(layoutParams);
                    }
                });
                int animamationDuration = 500;
                anim.setDuration(animamationDuration);
                anim.start();
                TextView venueId = viewVenue.findViewById(R.id.text_view_venue_id);
                showTheVenueTitle(context, activity, venueTitle, venueId, animationView, animamationDuration);
            }
        });
    }

    private static void showTheVenueTitle(final Context context, final Activity activity, final TextView venueTitle, final TextView venueId, final FrameLayout animationView, int animamationDuration) {
        // showing the venue title after the animation has ended
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG_LOGGING, "Showing the title of the venue");
                venueTitle.setVisibility(View.VISIBLE);
                startVenueDetailsActivity(context, activity, venueTitle, venueId, animationView);

            }
        }, animamationDuration);
    }

    private static void startVenueDetailsActivity(final Context context, final Activity activity, final TextView venueTitle, final TextView venueId, final FrameLayout animationView) {
        int startingActivityDelay = 500;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent venueDetails = new Intent(context, VenueDetailsActivity.class);
                venueDetails.putExtra(VenueDetailsActivity.VENUE_ID_KEY, venueId.getText());
                Log.i(TAG_LOGGING, "Starting VenueDetailsActivity");
                activity.startActivity(venueDetails);
                hideVenueViewTitle(venueTitle, animationView);
            }
        }, startingActivityDelay);
    }

    private static void hideVenueViewTitle(final TextView venueTitle, final FrameLayout animationView) {
        int hidingVenueViewTitleDelay = 1000;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG_LOGGING, "Hiding the venue view title");
                venueTitle.setVisibility(View.GONE);
                ViewGroup.LayoutParams layoutParams = animationView.getLayoutParams();
                layoutParams.width = 0;
                animationView.setLayoutParams(layoutParams);
            }
        }, hidingVenueViewTitleDelay);
    }

    public static MarkerView getMarkerView(Activity activity, Context context, Venue venue) {
        View customView = MarkerViewUtils.getViewVenue(activity, context, venue);
        Location location = venue.getLocation();
        MarkerView markerView = new MarkerView(new LatLng(location.getLat(), location.getLng()), customView);
        return markerView;
    }
}
