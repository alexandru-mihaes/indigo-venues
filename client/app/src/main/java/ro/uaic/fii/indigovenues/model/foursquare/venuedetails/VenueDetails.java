package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import ro.uaic.fii.indigovenues.model.foursquare.recommended.Category;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Location;

public class VenueDetails implements Serializable {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("location")
    private Location location;

    @SerializedName("categories")
    private List<Category> categories;

    @SerializedName("contact")
    private Contact contact;

    @SerializedName("url")
    private String url;

    @SerializedName("hours")
    private Hours hours;

    @SerializedName("bestPhoto")
    private Photo bestPhoto;

    public VenueDetails() {
    }

    public VenueDetails(String id, String name, Location location, List<Category> categories, Contact contact, String url, Hours hours, Photo bestPhoto) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.categories = categories;
        this.contact = contact;
        this.url = url;
        this.hours = hours;
        this.bestPhoto = bestPhoto;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public Contact getContact() {
        return contact;
    }

    public String getUrl() {
        return url;
    }

    public Hours getHours() {
        return hours;
    }

    public Photo getBestPhoto() {
        return bestPhoto;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setHours(Hours hours) {
        this.hours = hours;
    }

    public void setBestPhoto(Photo bestPhoto) {
        this.bestPhoto = bestPhoto;
    }
}
