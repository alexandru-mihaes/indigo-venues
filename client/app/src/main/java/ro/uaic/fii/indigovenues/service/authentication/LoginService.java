package ro.uaic.fii.indigovenues.service.authentication;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.service.BaseService;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.MainscreenActivity;
import ro.uaic.fii.indigovenues.ui.widget.CustomToastUtils;
import ro.uaic.fii.indigovenues.utils.PreferencesUtils;

public class LoginService extends BaseService {
    private static final String TAG_LOGGING = LoginService.class.getName();

    public LoginService(Activity activity, ProgressBar progressBar) {
        super(activity, progressBar, ServerUtils.getUrl(activity.getBaseContext(), "auth/signin"));
    }

    public void makeRequest(final String usernameOrEmail, final String password) {
        if (usernameOrEmail.isEmpty() || password.isEmpty()) {
            CustomToastUtils.pleaseFillTheFields(activity);
        } else {
            Log.i(TAG_LOGGING, "Making login request...");
            super.makeRequest(
                    Request.Method.POST,
                    false,
                    this.getClass(),
                    new Pair<>("usernameOrEmail", usernameOrEmail),
                    new Pair<>("password", password)
            );
        }
    }

    @Override
    protected void handleSuccess(JSONObject response) {
        super.handleSuccess(response);
        PreferencesUtils.saveUser(activity, response);
        Intent mainscreen = new Intent(activity, MainscreenActivity.class);
        Log.i(TAG_LOGGING, "Starting MainscreenActivity...");
        activity.startActivity(mainscreen);
        activity.finish();
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}