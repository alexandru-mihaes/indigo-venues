package ro.uaic.fii.indigovenues.exception.foursquare;

public class InvalidVenuePresenceResponseException extends RuntimeException {
    public InvalidVenuePresenceResponseException(String message) {
        super(message);
    }

    public InvalidVenuePresenceResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
