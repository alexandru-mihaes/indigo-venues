package ro.uaic.fii.indigovenues.simulatedannealing;

import java.util.ArrayList;
import java.util.List;

public class ItineraryManager {
    private static List<VenueSA> destinationVenuesSA = new ArrayList<>();

    public static void addVenueSA(VenueSA venueSA) {
        destinationVenuesSA.add(venueSA);
    }

    public static VenueSA getVenueSA(int index) {
        return destinationVenuesSA.get(index);
    }

    public static int numberOfVenuesSA() {
        return destinationVenuesSA.size();
    }
}
