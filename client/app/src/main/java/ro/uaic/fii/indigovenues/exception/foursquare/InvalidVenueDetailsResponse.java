package ro.uaic.fii.indigovenues.exception.foursquare;

public class InvalidVenueDetailsResponse extends RuntimeException {
    public InvalidVenueDetailsResponse(String message) {
        super(message);
    }

    public InvalidVenueDetailsResponse(String message, Throwable cause) {
        super(message, cause);
    }
}
