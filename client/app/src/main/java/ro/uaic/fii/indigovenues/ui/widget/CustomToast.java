package ro.uaic.fii.indigovenues.ui.widget;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ro.uaic.fii.indigovenues.R;

public class CustomToast {
    public static void showToast(String text, Activity activity, boolean happyToast) {
        View layout = inflateToast(activity);
        ImageView toastImage = layout.findViewById(R.id.toast_image);
        if (happyToast) {
            toastImage.setImageResource(R.drawable.image_toast_emoji_happy);
        } else {
            toastImage.setImageResource(R.drawable.image_toast_emoji_sad);
        }
        setToastText(text, layout);
        createToast(layout, activity);
    }

    private static View inflateToast(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(
                R.layout.widget_custom_toast,
                (ViewGroup) activity.findViewById(R.id.toast_root)
        );
        return layout;
    }

    private static void setToastText(String text, View layout) {
        TextView toastText = layout.findViewById(R.id.toast_text);
        toastText.setText(text);
    }

    private static void createToast(View layout, Activity activity) {
        Toast toast = new Toast(activity.getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

}
