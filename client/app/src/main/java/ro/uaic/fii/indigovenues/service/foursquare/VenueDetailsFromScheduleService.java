package ro.uaic.fii.indigovenues.service.foursquare;

import android.app.Activity;
import android.util.Log;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import ro.uaic.fii.indigovenues.exception.foursquare.InvalidVenueDetailsFromScheduleResponse;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.VenueDetails;
import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.service.BaseService;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment.ScheduleFragment;

public class VenueDetailsFromScheduleService extends BaseService {
    private static final String TAG_LOGGING = VenueDetailsFromScheduleService.class.getName();

    private final ScheduleFragment scheduleFragment;

    public VenueDetailsFromScheduleService(Activity activity, ProgressBar progressBar, ScheduleFragment scheduleFragment) {
        super(activity, progressBar, ServerUtils.getUrl(activity.getBaseContext(), "venues/schedule"));
        this.scheduleFragment = scheduleFragment;
    }

    public void makeRequest() {
        Log.i(TAG_LOGGING, "Making GET request - venue details from schedule");
        super.makeRequest(
                Request.Method.GET,
                true,
                this.getClass()
        );
    }

    @Override
    public void handleSuccess(final JSONObject response) {
        super.handleSuccess(response);

        try {
            Log.i(TAG_LOGGING, "Converting response into object");
            JSONArray jsonArray = response.getJSONArray("venueDetailsList");
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("M/d/yy hh:mm a");
            Gson gson = gsonBuilder.create();
            if (jsonArray.length() > 0) {
                List<VenueDetails> venueDetailsList = Arrays.asList(gson.fromJson(jsonArray.toString(), VenueDetails[].class));
                placeVenueDetailsListInsideListViewVenues(venueDetailsList);
            }
        } catch (JSONException e) {
            Log.e(TAG_LOGGING, "Throwing InvalidAuthResponseException...");
            throw new InvalidVenueDetailsFromScheduleResponse("Invalid venue details from schedule response - " + e.getMessage());
        }

        Log.w(TAG_LOGGING, response.toString());
    }

    private void placeVenueDetailsListInsideListViewVenues(List<VenueDetails> venueDetailsList) {
        Log.i(TAG_LOGGING, "Placing venues details list inside the list view of venues");
        scheduleFragment.createCustomAdapter(venueDetailsList);
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}
