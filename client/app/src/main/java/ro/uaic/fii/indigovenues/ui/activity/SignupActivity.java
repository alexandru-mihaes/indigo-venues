package ro.uaic.fii.indigovenues.ui.activity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.service.authentication.SignupService;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG_LOGGING = SignupActivity.class.getName();

    private EditText editTextUsername;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextPasswordConfirm;
    private Button btnSignup;
    private ProgressBar progressBar;

    private View.OnClickListener signupClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            attemptSignup();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        btnSignup = findViewById(R.id.btn_signup);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
        btnSignup.setOnClickListener(signupClickListener);
    }

    private void attemptSignup() {
        Log.i(TAG_LOGGING, "Attempting signup...");
        editTextUsername = findViewById(R.id.edit_text_username);
        editTextEmail = findViewById(R.id.edit_text_email);
        editTextPassword = findViewById(R.id.edit_text_password);
        editTextPasswordConfirm = findViewById(R.id.edit_text_new_password_confirm);

        SignupService signupService = new SignupService(this, progressBar);
        signupService.makeRequest(
                editTextUsername.getText().toString(),
                editTextEmail.getText().toString(),
                editTextPassword.getText().toString(),
                editTextPasswordConfirm.getText().toString()
        );
    }
}