package ro.uaic.fii.indigovenues.ui.widget;

import android.app.Activity;
import android.content.Context;

import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerView;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerViewManager;

import ro.uaic.fii.indigovenues.model.foursquare.recommended.Venue;

public class VenueMapPoint {
    private MarkerViewManager markerViewManager;
    private MarkerView markerView;
    private String name;

    public VenueMapPoint(MarkerViewManager markerViewManager, MarkerView markerView, String name) {
        this.markerViewManager = markerViewManager;
        this.markerView = markerView;
        this.name = name;
    }

    public VenueMapPoint(MapView mapView, MapboxMap mapboxMap, Activity activity, Context context, Venue venue, String name) {
        markerViewManager = new MarkerViewManager(mapView, mapboxMap);
        markerView = MarkerViewUtils.getMarkerView(activity, context, venue);
        markerViewManager.addMarker(markerView);
        this.name = name;
    }

    public MarkerViewManager getMarkerViewManager() {
        return markerViewManager;
    }

    public MarkerView getMarkerView() {
        return markerView;
    }

    public String getName() {
        return name;
    }
}
