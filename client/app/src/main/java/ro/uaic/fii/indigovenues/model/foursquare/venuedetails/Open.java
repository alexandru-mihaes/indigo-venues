package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Open implements Serializable {
    @SerializedName("renderedTime")
    private String renderedTime;

    public Open() {
    }

    public Open(String renderedTime) {
        this.renderedTime = renderedTime;
    }

    public String getRenderedTime() {
        return renderedTime;
    }

    public void setRenderedTime(String renderedTime) {
        this.renderedTime = renderedTime;
    }
}
