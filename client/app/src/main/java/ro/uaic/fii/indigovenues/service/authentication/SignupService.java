package ro.uaic.fii.indigovenues.service.authentication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.network.ServerUtils;
import ro.uaic.fii.indigovenues.service.BaseService;
import ro.uaic.fii.indigovenues.ui.activity.SigninActivity;
import ro.uaic.fii.indigovenues.ui.widget.CustomToastUtils;

public class SignupService extends BaseService {
    private static final String TAG_LOGGING = SignupService.class.getName();

    public SignupService(Activity activity, ProgressBar progressBar) {
        super(activity, progressBar, ServerUtils.getUrl(activity.getBaseContext(), "auth/signup"));
    }

    public void makeRequest(final String username,
                            final String email,
                            final String password,
                            final String passwordConfirm) {
        if (username.isEmpty() || email.isEmpty() || password.isEmpty() || passwordConfirm.isEmpty()) {
            CustomToastUtils.pleaseFillTheFields(activity);
        } else if (!password.equals(passwordConfirm)) {
            CustomToastUtils.passwordsDontMatch(activity);
        } else {
            Log.i(TAG_LOGGING, "Making signup request...");
            super.makeRequest(
                    Request.Method.POST,
                    false,
                    this.getClass(),
                    new Pair<>("username", username),
                    new Pair<>("email", email),
                    new Pair<>("password", password)
            );
        }
    }

    @Override
    protected void handleSuccess(JSONObject response) {
        super.handleSuccess(response);
        Log.i(TAG_LOGGING, "Showing 'Signed up successfully' AlertDialog...");
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AlertDialog);
        builder.setMessage(activity.getBaseContext().getString(R.string.msg_signed_up_successfully))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent main = new Intent(activity, SigninActivity.class);
                        activity.startActivity(main);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}