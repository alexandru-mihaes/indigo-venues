package ro.uaic.fii.indigovenues.simulatedannealing;

import android.util.Log;

import java.util.List;

public class SimulatedAnnealing {
    private static final String TAG_LOGGING = SimulatedAnnealing.class.getName();

    public static Itinerary run(List<VenueSA> initialVenuesSAstate) {
        double temperature = 10000;
        double coolingRate = 0.003;
        Itinerary initialSolution = new Itinerary(initialVenuesSAstate);
        initialSolution.generateIndividual();
        Log.i(TAG_LOGGING, "Initial solution distance: " + initialSolution.getDistance());
        Log.i(TAG_LOGGING, "Initial solution: " + initialSolution + "\n");

        Itinerary currentSolution = initialSolution;
        Itinerary bestItinerary = new Itinerary(currentSolution.getVenuesSA());
        while (temperature > 1) {
            Itinerary newNeighbourItinerary = new Itinerary(currentSolution.getVenuesSA());

            // Random venues from the itinerary and swap them
            int itineraryIndex1 = (int) (newNeighbourItinerary.itinerarySize() * Math.random());
            int itineraryIndex2 = (int) (newNeighbourItinerary.itinerarySize() * Math.random());
            VenueSA venueSA1 = newNeighbourItinerary.getVenueSA(itineraryIndex1);
            VenueSA venueSA2 = newNeighbourItinerary.getVenueSA(itineraryIndex2);
            newNeighbourItinerary.setVenueSA(itineraryIndex2, venueSA1);
            newNeighbourItinerary.setVenueSA(itineraryIndex1, venueSA2);

            // Decideing if we should accept the new neighbour
            int currentEnergy = currentSolution.getDistance();
            int newNeighbourEnergy = newNeighbourItinerary.getDistance();
            if (acceptanceProbability(currentEnergy, newNeighbourEnergy, temperature) > Math.random()) {
                currentSolution = new Itinerary(newNeighbourItinerary.getVenuesSA());
            }

            // Update the current best itinerary if possible
            if (currentSolution.getDistance() < bestItinerary.getDistance()) {
                bestItinerary = new Itinerary(currentSolution.getVenuesSA());
                Log.i(TAG_LOGGING, "Current temeprature: " + temperature);
                Log.i(TAG_LOGGING, "Current solution distance: " + bestItinerary.getDistance());
                Log.i(TAG_LOGGING, "Current best itinerary: " + bestItinerary + "\n");
            }

            // cooling the system
            temperature = temperature * (1 - coolingRate);
        }

        Log.i(TAG_LOGGING, "Final solution distance: " + bestItinerary.getDistance());
        Log.i(TAG_LOGGING, "Itinerary: " + bestItinerary);
        return bestItinerary;
    }

    public static double acceptanceProbability(int energy, int newEnergy, double temperature) {
        if (newEnergy < energy) { // if the new neighbour is better, then we accept him
            return 1.0;
        }
        // If the new neighbour is worse, we calculate an acceptance probability
        return Math.exp((energy - newEnergy) / temperature);
    }
}
