package ro.uaic.fii.indigovenues.ui.activity.mainscreen;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import ro.uaic.fii.indigovenues.R;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment.ExploreFragment;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment.MainscreenFragment;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment.ProfileFragment;
import ro.uaic.fii.indigovenues.ui.activity.mainscreen.fragment.ScheduleFragment;

public class MainscreenActivity extends AppCompatActivity implements MainscreenFragment.OnFragmentInteractionListener {
    private static final String TAG_LOGGING = MainscreenActivity.class.getName();

    private BottomNavigationView menuBar;
    private List<MainscreenFragment> fragments = new ArrayList<>(3);
    private static final String TAG_FRAGMENT_SCHEDULE = "ScheduleFragment";
    private static final String TAG_FRAGMENT_EXPLORE = "ExploreFragment";
    private static final String TAG_FRAGMENT_PROFILE = "ProfileFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainscreen);

        menuBar = findViewById(R.id.menu_bar);
        initializeMenuBarItemListener();
        buildFragmentsList();
        switchFragment(0, TAG_FRAGMENT_SCHEDULE);
    }

    private void initializeMenuBarItemListener() {
        Log.i(TAG_LOGGING, "Initializing menu bar item listener...");
        menuBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.item_menu_bar_schedule:
                        switchFragment(0, TAG_FRAGMENT_SCHEDULE);
                        break;
                    case R.id.item_menu_bar_explore:
                        switchFragment(1, TAG_FRAGMENT_EXPLORE);
                        break;
                    case R.id.item_menu_bar_profile:
                        switchFragment(2, TAG_FRAGMENT_PROFILE);
                        break;
                }
                return true;
            }
        });
    }

    private void buildFragmentsList() {
        MainscreenFragment scheduleFragment = buildFragment("schedule");
        MainscreenFragment exploreFragment = buildFragment("explore");
        MainscreenFragment profileFragment = buildFragment("profile");

        fragments.add(scheduleFragment);
        fragments.add(exploreFragment);
        fragments.add(profileFragment);
    }

    private MainscreenFragment buildFragment(String title) {
        Log.i(TAG_LOGGING, "Building fragment " + title + "...");
        switch (title) {
            case "schedule": {
                ScheduleFragment scheduleFragment = new ScheduleFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ProfileFragment.ARG_TITLE, title);
                scheduleFragment.setArguments(bundle);
                return scheduleFragment;
            }
            case "explore": {
                ExploreFragment exploreFragment = new ExploreFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ProfileFragment.ARG_TITLE, title);
                exploreFragment.setArguments(bundle);
                return exploreFragment;
            }
            case "profile": {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ProfileFragment.ARG_TITLE, title);
                profileFragment.setArguments(bundle);
                return profileFragment;
            }
        }
        return null;
    }

    private void switchFragment(int pos, String tag) {
        Log.i(TAG_LOGGING, "Switching to fragment " + tag + "...");
        changeActionBarTitle(tag);
        Fragment fragment = fragments.get(pos);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_fragment_holder, fragment, tag)
                .commit();
    }

    private void changeActionBarTitle(String tag) {
        Log.i(TAG_LOGGING, "Changing action bar title to " + tag + "...");
        switch (tag) {
            case "ScheduleFragment": {
                this.setTitle(R.string.item_menu_bar_schedule);
                break;
            }
            case "ExploreFragment": {
                this.setTitle(R.string.item_menu_bar_explore);
                break;
            }
            case "ProfileFragment": {
                this.setTitle(R.string.item_menu_bar_profile);
                break;
            }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Log.i(TAG_LOGGING, "onFragmentInteraction: " + uri.toString());
    }
}