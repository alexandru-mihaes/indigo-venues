package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Venue implements Serializable {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("location")
    private Location location;

    @SerializedName("categories")
    private List<Category> categories;

    public Venue() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}