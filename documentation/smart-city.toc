\babel@toc {romanian}{}
\contentsline {section}{Introducere}{6}% 
\contentsline {section}{Contribu\IeC {\textcommabelow t}ii}{7}% 
\contentsline {section}{\numberline {\uppercase {i}.}Descrierea aplica\IeC {\textcommabelow t}iei}{8}% 
\contentsline {subsection}{\numberline {1.1}Autentificare}{8}% 
\contentsline {subsection}{\numberline {1.2}Fragmentul pentru editarea profilului}{10}% 
\contentsline {subsection}{\numberline {1.3}Fragmentul pentru explorarea obiectivelor turistice}{11}% 
\contentsline {subsection}{\numberline {1.4}Activitatea care cuprinde detalii cu privire la obiectivul turistic}{14}% 
\contentsline {subsection}{\numberline {1.5}Fragmentul pentru vizualizarea itinerarului}{15}% 
\contentsline {section}{\numberline {\uppercase {ii}.}Tehnologii utilizate}{16}% 
\contentsline {subsection}{\numberline {2.1}Spring Boot}{16}% 
\contentsline {subsection}{\numberline {2.2}Android}{18}% 
\contentsline {section}{\numberline {\uppercase {iii}.}Arhitectura serverului}{19}% 
\contentsline {subsection}{\numberline {3.1}Nivelul de persisten\IeC {\textcommabelow t}\IeC {\u a}}{20}% 
\contentsline {subsection}{\numberline {3.2}Nivelul de serviciu}{24}% 
\contentsline {subsection}{\numberline {3.3}Nivelul de securitate}{27}% 
\contentsline {subsection}{\numberline {3.4}Nivelul web}{31}% 
\contentsline {section}{\numberline {\uppercase {iv}.}Arhitectura aplica\IeC {\textcommabelow t}iei Android}{34}% 
\contentsline {subsection}{\numberline {4.1}Relizarea unei cereri HTTP}{36}% 
\contentsline {subsection}{\numberline {4.2}Integrarea cu Mapbox API}{39}% 
\contentsline {subsection}{\numberline {4.3}Algoritmul de sortare a obiectivelor turistice}{40}% 
\contentsline {section}{\numberline {\uppercase {v}.}Manual de utilizare}{44}% 
\contentsline {section}{Concluzii}{46}% 
\contentsline {section}{Bibliografie}{47}% 
