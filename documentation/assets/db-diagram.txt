users
-
id PK
username
email
password
created_at
updated_at

venues
-
id PK
name
location_id FK >- location.venue_id
categories FK >- venues_categories.venue_id

location
-
venue_id PK
address
lat
lng
country
state
city

venues_categories
-
venue_id
category_id

categories
-
id PK FK >- venues_categories.category_id
name
plural_name
short_name
icon_id FK >- icons.prefix
primary_category

icons
-
prefix PK
suffix

venues_details
-
id PK
name
location_id FK >- location.venue_id
categories FK >- venues_categories.venue_id
contact FK >- contacts.venue_id
url
hours_id FK >- hours.venue_id
best_photo_id FK >- photos.id

contacts
-
venue_id
phone
formatted_phone

hours
-
venue_id
status
is_open

hours_timeframes
-
hours_id FK >- hours.venue_id
timeframes_timeframe_id FK >- timeframes.id
timeframes_venue_id FK >- timeframes.venue_id

timeframes
-
id PK
venue_id PK
days
includes_today
opens

timeframes_opens
-
timeframe_id FK >- timeframes.id
opens_venue_id FK >- opens.venue_id
opens_timeframe_id FK >- opens.timeframe_id
opens_open_id FK >- opens.open_id

opens
-
venue_id PK
timeframe_id PK
open_id


photos
-
id FK >- photos_input_stream.id
prefix
suffix
width
height

photos_input_stream
-
id
byte_array

simple_venues
-
id PK FK >- venues_details.id

venue_schedules_simple_venues
-
venue_schedule_id FK >- venue_schedules.id
simple_venue_id FK >- simple_venues.id

venue_schedules
-
id PK FK >- users.id