package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@ApiModel(description = "Details about the category of a venuedetails")
public class Category implements Serializable {
    @Id
    @ApiModelProperty(notes = "A unique identifier for this category")
    private String id;

    @ApiModelProperty(notes = "Name of the category")
    private String name;

    @ApiModelProperty(notes = "Pluralized version of the category name")
    private String pluralName;

    @ApiModelProperty(notes = "Shorter version of the category name")
    private String shortName;

    @ApiModelProperty(notes = "Pieces needed to construct category icons at various sizes")
    @ManyToOne(fetch = FetchType.EAGER)
    private Icon icon;

    @ApiModelProperty(notes = "If this is the primaryCategory category for parent venuedetails object")
    @JsonProperty("primaryCategory")
    private boolean primaryCategory;

    public Category() {
    }

    public Category(String id, String name, String pluralName, String shortName, Icon icon, boolean primaryCategory) {
        this.id = id;
        this.name = name;
        this.pluralName = pluralName;
        this.shortName = shortName;
        this.icon = icon;
        this.primaryCategory = primaryCategory;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPluralName() {
        return pluralName;
    }

    public String getShortName() {
        return shortName;
    }

    public Icon getIcon() {
        return icon;
    }

    public boolean isPrimaryCategory() {
        return primaryCategory;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPluralName(String pluralName) {
        this.pluralName = pluralName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public void setPrimaryCategory(boolean primaryCategory) {
        this.primaryCategory = primaryCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return primaryCategory == category.primaryCategory &&
                Objects.equals(id, category.id) &&
                Objects.equals(name, category.name) &&
                Objects.equals(pluralName, category.pluralName) &&
                Objects.equals(shortName, category.shortName) &&
                Objects.equals(icon, category.icon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, pluralName, shortName, icon, primaryCategory);
    }
}
