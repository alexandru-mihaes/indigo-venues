package ro.uaic.fii.indigovenues.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(Long userId) {
        super("User with id '" + userId + "' does not exist.");
    }

    public UserNotFoundException(String usernameOrEmail) {
        super("User with username/email '" + usernameOrEmail + "' does not exist.");
    }

    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}