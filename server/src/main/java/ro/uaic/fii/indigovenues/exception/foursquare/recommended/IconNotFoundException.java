package ro.uaic.fii.indigovenues.exception.foursquare.recommended;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Icon not found")
public class IconNotFoundException extends RuntimeException {
    public IconNotFoundException(String prefix) {
        super("Icon with prefix " + prefix + " does not exist.");
    }
}