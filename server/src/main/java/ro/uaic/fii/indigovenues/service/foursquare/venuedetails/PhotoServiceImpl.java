package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.BestPhotoNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.ContactNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Contact;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Photo;
import ro.uaic.fii.indigovenues.repository.foursquare.venuedetails.ContactRepository;
import ro.uaic.fii.indigovenues.repository.foursquare.venuedetails.PhotoRepository;

@Service
public class PhotoServiceImpl implements PhotoService {
    private static final Logger logger = LoggerFactory.getLogger(PhotoServiceImpl.class);

    private PhotoRepository photoRepository;

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    public Photo getById(String id) throws BestPhotoNotFoundException {
        logger.info("Getting photo from database: {}", id);
        return photoRepository.findById(id).orElseThrow(() -> new BestPhotoNotFoundException(id));
    }

    @Override
    public Photo save(Photo photo) {
        logger.info("Saving photo to database: {}", photo.getId());
        return photoRepository.save(photo);
    }
}
