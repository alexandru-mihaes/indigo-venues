package ro.uaic.fii.indigovenues.validator;

import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import ro.uaic.fii.indigovenues.exception.InvalidCredentialsException;

import java.util.Optional;

public abstract class UserDataValidator {
    private static final Logger logger = LoggerFactory.getLogger(UserDataValidator.class);

    public static final String USERNAME_VALIDATION_ERROR_CODE = "Username must have between 4 and 20 characters";
    public static final String EMAIL_VALIDATION_ERROR_CODE = "Email is not valid";
    public static final String PASSWORD_VALIDATION_ERROR_CODE = "The password must be have at least:" +
            "\n• 6 characters\n• 1 number\n• 1 uppercase & 1 lowercase character\n• 1 special character" +
            "\n• and must not contain spaces";

    private String madeByUsername;

    public UserDataValidator(String madeByUsername) {
        this.madeByUsername = madeByUsername;
    }

    public static boolean isUsernameValid(String username) {
        return username.length() >= 4 && username.length() <= 20;
    }

    public static boolean isEmailValid(String email) {
        return !email.isEmpty() && email.length() <= 40 && isValidEmailAddress(email);
    }

    private static boolean isValidEmailAddress(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    public static boolean isPasswordValid(String password) {
        return password.length() >= 6 && isValidPassword(password);
    }

    /**
     * (?=.*[0-9]) a digit must occur at least once
     * (?=.*[a-z]) a lower case letter must occur at least once
     * (?=.*[A-Z]) an upper case letter must occur at least once
     * (?=.*[@#$%^&+=]) a special character must occur at least once
     * (?=\\S+$) no whitespace allowed in the entire string
     * .{6,} at least 6 characters
     *
     * @return true if password is valid
     */
    private static boolean isValidPassword(String password) {
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}";
        return password.matches(pattern);
    }

    protected void checkIfHasErrors(Errors bindingResult) {
        if (bindingResult.hasErrors()) {
            Optional<FieldError> fieldError = bindingResult.getFieldErrors().stream().findFirst();
            if (fieldError.isPresent()) {
                logger.error("Invalid credentials made by username: {}", madeByUsername);
                throw new InvalidCredentialsException(fieldError.get().getCode());
            }
        }
    }
}
