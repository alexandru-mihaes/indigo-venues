package ro.uaic.fii.indigovenues.service.foursquare.venueschedule;

import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.VenueSchedule;

import java.util.List;


public interface VenueScheduleService {
    VenueSchedule getById(Long id);

    VenueSchedule save(VenueSchedule userSchedule);

    VenueSchedule update(VenueSchedule userSchedule);

    List<VenueSchedule> getAll();
}
