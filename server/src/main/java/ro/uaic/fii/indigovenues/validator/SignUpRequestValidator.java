package ro.uaic.fii.indigovenues.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ro.uaic.fii.indigovenues.payload.request.SignUpRequest;

public class SignUpRequestValidator extends UserDataValidator implements Validator {
    private static final Logger logger = LoggerFactory.getLogger(SignUpRequestValidator.class);

    public SignUpRequestValidator(String madeByUsername) {
        super(madeByUsername);
    }

    @Override
    public boolean supports(Class clazz) {
        return SignUpRequest.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SignUpRequest signUpRequest = (SignUpRequest) target;
        if (!isUsernameValid(signUpRequest.getUsername())) {
            logger.error("Invalid username: {}", signUpRequest.getUsername());
            errors.rejectValue("username", USERNAME_VALIDATION_ERROR_CODE);
        }
        if (!isEmailValid(signUpRequest.getEmail())) {
            logger.error("Invalid email: {}", signUpRequest.getEmail());
            errors.rejectValue("email", EMAIL_VALIDATION_ERROR_CODE);
        }
        if (!isPasswordValid(signUpRequest.getPassword())) {
            logger.error("Invalid password");
            errors.rejectValue("password", PASSWORD_VALIDATION_ERROR_CODE);
        }
        checkIfHasErrors(errors);
    }
}
