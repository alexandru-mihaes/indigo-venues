package ro.uaic.fii.indigovenues.exception.foursquare.recommended;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FoursquareHttpClientException extends RuntimeException {
    public FoursquareHttpClientException(String message) {
        super(message);
    }
}
