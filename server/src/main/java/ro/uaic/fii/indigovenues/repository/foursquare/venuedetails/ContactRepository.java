package ro.uaic.fii.indigovenues.repository.foursquare.venuedetails;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Location;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Contact;

@Repository
public interface ContactRepository extends CrudRepository<Contact, String> {
}