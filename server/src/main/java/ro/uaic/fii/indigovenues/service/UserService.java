package ro.uaic.fii.indigovenues.service;

import ro.uaic.fii.indigovenues.model.User;

public interface UserService {
    User getUserByUsernameOrEmail(String usernameOrEmail);

    User getUserByUsername(String username);

    User save(User user);

    User update(User user);

    void checkUserDataExists(String username, String email);

    void checkUserDataExists(User currentUser, String username, String email);

    boolean oldPasswordIsValid(User user, String oldPassword);
}
