package ro.uaic.fii.indigovenues.service.foursquare.recommended;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.recommended.IconNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Icon;
import ro.uaic.fii.indigovenues.repository.foursquare.recommended.IconRepository;

@Service
public class IconServiceImpl implements IconService {
    private static final Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);

    private IconRepository iconRepository;

    @Autowired
    public IconServiceImpl(IconRepository iconRepository) {
        this.iconRepository = iconRepository;
    }

    @Override
    public Icon getByPrefix(String prefix) throws IconNotFoundException {
        logger.info("Getting icon from database: {}", prefix);
        return iconRepository.findById(prefix).orElseThrow(()->new IconNotFoundException(prefix));
    }

    @Override
    public Icon save(Icon icon) {
        logger.info("Saving icon to database: {}", icon.getPrefix());
        return iconRepository.save(icon);
    }
}
