package ro.uaic.fii.indigovenues.service.foursquare.recommended;

import ro.uaic.fii.indigovenues.model.foursquare.recommended.Location;

public interface LocationService {
    Location getByVenueId(String address);

    Location save(Location location);
}
