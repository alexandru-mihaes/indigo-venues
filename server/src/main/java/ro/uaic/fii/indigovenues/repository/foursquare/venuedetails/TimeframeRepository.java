package ro.uaic.fii.indigovenues.repository.foursquare.venuedetails;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Timeframe;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.TimeframeKey;

@Repository
public interface TimeframeRepository extends CrudRepository<Timeframe, TimeframeKey> {
}