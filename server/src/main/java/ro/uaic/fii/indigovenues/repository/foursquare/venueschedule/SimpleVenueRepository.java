package ro.uaic.fii.indigovenues.repository.foursquare.venueschedule;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.SimpleVenue;

@Repository
public interface SimpleVenueRepository extends CrudRepository<SimpleVenue, String> {
}
