package ro.uaic.fii.indigovenues.service.foursquare;

import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Venue;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.VenueDetails;
import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.VenueSchedule;

import java.io.InputStream;
import java.util.List;

public interface VenueService {
    List<Venue> getRecommended(String location);

    List<Venue> getRecommendedByLatAndLng(String ll);

    VenueDetails getById(String id);

    InputStream getBestPhoto(String bestPhotoUrl);

    VenueSchedule addVenueToSchedule(User currentUser, String venueId);

    void removeVenueFromSchedule(User currentUser, String venueId);

    List<VenueDetails> getVenueDetailsListFromScheduleOfUser(Long userId);

    Boolean checkVeneuePresenceInSchedule(String venueId, Long userId);
}
