package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.HoursNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Hours;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Timeframe;
import ro.uaic.fii.indigovenues.repository.foursquare.venuedetails.HoursRepository;

import java.util.List;
import java.util.UUID;

@Service
public class HoursServiceImpl implements HoursService {
    private static final Logger logger = LoggerFactory.getLogger(HoursServiceImpl.class);

    private HoursRepository hoursRepository;
    private TimeframeService timeframeService;

    @Autowired
    public HoursServiceImpl(HoursRepository hoursRepository, TimeframeService timeframeService) {
        this.hoursRepository = hoursRepository;
        this.timeframeService = timeframeService;
    }

    @Override
    public Hours getByVenueId(String venueId) throws HoursNotFoundException {
        logger.info("Getting hours from database: {}", venueId);
        return hoursRepository.findById(venueId).orElseThrow(() -> new HoursNotFoundException(venueId));
    }

    @Override
    public Hours save(Hours hours) {
        logger.info("Saving hours to database: {}", hours.getStatus());
        return hoursRepository.save(hours);
    }

    @Override
    public Hours saveOpenTimeframesThenHours(Hours hours) {
        logger.info("Saving open and timeframes then hours to database");
        List<Timeframe> timeframes = hours.getTimeframes();
        for(Integer timeframeIndex = 0; timeframeIndex < timeframes.size(); timeframeIndex++) {
            Timeframe timeframe = timeframes.get(timeframeIndex);
            timeframe.setVenueId(hours.getVenueId());
            timeframe.setTimeframeId(timeframeIndex.toString());
        }
        timeframes = timeframeService.saveOpenThenTimeframes(timeframes);
        hours.setTimeframes(timeframes);
        return this.save(hours);
    }
}
