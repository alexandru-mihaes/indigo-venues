package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Objects;

@ApiModel(description = "Details about the recommended venues response")
public class RecommendedVenuesResponse {
    @ApiModelProperty(notes = "List of recommended venues")
    private List<Venue> venues;

    public RecommendedVenuesResponse() {
    }

    public RecommendedVenuesResponse(List<Venue> venues) {
        this.venues = venues;
    }

    public List<Venue> getVenues() {
        return venues;
    }

    public void setVenues(List<Venue> venues) {
        this.venues = venues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecommendedVenuesResponse that = (RecommendedVenuesResponse) o;
        return Objects.equals(venues, that.venues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(venues);
    }
}
