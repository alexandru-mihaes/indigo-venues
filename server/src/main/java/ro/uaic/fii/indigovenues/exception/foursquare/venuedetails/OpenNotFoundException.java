package ro.uaic.fii.indigovenues.exception.foursquare.venuedetails;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Open not found")
public class OpenNotFoundException extends RuntimeException {
    public OpenNotFoundException(String venueId) {
        super("Open with venueId " + venueId + " does not exist.");
    }
}