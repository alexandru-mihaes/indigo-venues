package ro.uaic.fii.indigovenues.service.foursquare;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ro.uaic.fii.indigovenues.exception.foursquare.recommended.FoursquareHttpClientException;
import ro.uaic.fii.indigovenues.exception.foursquare.recommended.LocationNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.BestPhotoNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.BestPhotoStreamException;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.ContactNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.venueschedule.SimpleVenueNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.venueschedule.VenueScheduleNotFoundException;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.SimpleVenue;
import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.VenueSchedule;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Category;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Location;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.RecommendedVenues;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Venue;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.*;
import ro.uaic.fii.indigovenues.repository.foursquare.recommended.VenueRepository;
import ro.uaic.fii.indigovenues.repository.foursquare.venuedetails.PhotoInputStreamRepository;
import ro.uaic.fii.indigovenues.repository.foursquare.venuedetails.VenueDetailsRepository;
import ro.uaic.fii.indigovenues.service.foursquare.recommended.CategoryService;
import ro.uaic.fii.indigovenues.service.foursquare.recommended.LocationService;
import ro.uaic.fii.indigovenues.service.foursquare.venuedetails.ContactService;
import ro.uaic.fii.indigovenues.service.foursquare.venuedetails.HoursService;
import ro.uaic.fii.indigovenues.service.foursquare.venuedetails.PhotoService;
import ro.uaic.fii.indigovenues.service.foursquare.venueschedule.SimpleVenueService;
import ro.uaic.fii.indigovenues.service.foursquare.venueschedule.VenueScheduleService;
import ro.uaic.fii.indigovenues.utils.FoursquareApiUtils;
import ro.uaic.fii.indigovenues.utils.LatLngDistanceCalculator;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class VenueServiceImpl implements VenueService {
    private static final Logger logger = LoggerFactory.getLogger(VenueServiceImpl.class);

    private RestTemplate restTemplate;
    private FoursquareApiUtils foursquareApiUtils;
    private VenueRepository venueRepository;
    private LocationService locationService;
    private CategoryService categoryService;
    private VenueDetailsRepository venueDetailsRepository;
    private ContactService contactService;
    private HoursService hoursService;
    private PhotoService photoService;
    private PhotoInputStreamRepository photoInputStreamRepository;
    private VenueScheduleService venueScheduleService;
    private SimpleVenueService simpleVenueService;

    @Autowired
    public VenueServiceImpl(RestTemplate restTemplate,
                            FoursquareApiUtils foursquareApiUtils,
                            VenueRepository venueRepository,
                            LocationService locationService,
                            CategoryService categoryService,
                            VenueDetailsRepository venueDetailsRepository,
                            ContactService contactService,
                            HoursService hoursService,
                            PhotoService photoService,
                            PhotoInputStreamRepository photoInputStreamRepository,
                            VenueScheduleService venueScheduleService,
                            SimpleVenueService simpleVenueService) {
        this.restTemplate = restTemplate;
        this.foursquareApiUtils = foursquareApiUtils;
        this.venueRepository = venueRepository;
        this.locationService = locationService;
        this.categoryService = categoryService;
        this.venueDetailsRepository = venueDetailsRepository;
        this.contactService = contactService;
        this.hoursService = hoursService;
        this.photoService = photoService;
        this.photoInputStreamRepository = photoInputStreamRepository;
        this.venueScheduleService = venueScheduleService;
        this.simpleVenueService = simpleVenueService;

    }

    @Override
    public List<Venue> getRecommended(String location) throws HttpClientErrorException {
        String baseUrl = foursquareApiUtils.getUrlVenuesRecommended();
        String fullUrl = foursquareApiUtils.insertDefaultParametersIntoUrl(baseUrl) + "&limit=50&radius=5000&near={location}";
        // the radius to search within is set to 5000 meters
        return getVenues(fullUrl, location, false);
    }

    @Override
    public List<Venue> getRecommendedByLatAndLng(String ll) throws HttpClientErrorException {
        String baseUrl = foursquareApiUtils.getUrlVenuesRecommended();
        // the radius to search within is set to 5000 meters
        String fullUrl = foursquareApiUtils.insertDefaultParametersIntoUrl(baseUrl) + "&limit=50&radius=20000&ll={ll}";
        return getVenues(fullUrl, ll, true);
    }

    private List<Venue> getVenues(String fullUrl, String locationOrLl, boolean coordinates) {
        logger.info("Trying to search for cached recommended venues");
        List<Venue> venues = getVenuesFromDatabase(locationOrLl, coordinates);
        if (venues.size() > 40) {
            logger.info("Found more then 40 venues in the database. Recommended venues are being returned...");
            return venues;
        }
        logger.info("Making GET request to: {}", fullUrl);
        try {
            RecommendedVenues recommendedVenues = restTemplate.getForObject(
                    fullUrl,
                    RecommendedVenues.class,
                    foursquareApiUtils.getClientId(),
                    foursquareApiUtils.getClientSecret(),
                    foursquareApiUtils.getVersion(),
                    locationOrLl
            );
            logger.info("Recommended venues retrieved");
            venues = recommendedVenues.getResponse().getGroups().get(0)
                    .getItems().stream().map(item -> item.getVenue()).collect(Collectors.toList());
            venues = saveLocationIconsCategoriesThenVenues(venues);
            return venues;
        } catch (HttpClientErrorException exception) {
            logger.error("Throwing FoursquareHttpClientException with message: {}", exception.getMessage());
            throw new FoursquareHttpClientException(exception.getMessage());
        }
    }

    private List<Venue> getVenuesFromDatabase(String locationOrLl, boolean coordinates) {
        List<Venue> venues = venueRepository.findAll();
        List<Venue> recommendedVenues = new ArrayList<>();
        Pattern pattern = Pattern.compile(",");
        Matcher matcher = pattern.matcher(locationOrLl);
        String lat, lng;
        if (matcher.find()) {
            lat = locationOrLl.substring(0, matcher.start());
            lng = locationOrLl.substring(matcher.end());
            double latDouble = Double.parseDouble(lat);
            double lngDouble = Double.parseDouble(lng);
            for (Venue venue : venues) {
                Location location = venue.getLocation();
                Double latVenue = location.getLat();
                Double lngVenue = location.getLng();
                double distance = LatLngDistanceCalculator.getDistance(
                        latDouble, lngDouble, latVenue, lngVenue
                );
                if (distance < 20) { // if distance is less than 20 km
                    logger.info("Found venue " + venue.getName() + " is near (" + distance + "). It's being added to recommended venues");
                    recommendedVenues.add(venue);
                }
            }
        }
        return recommendedVenues;
    }

    public List<Venue> saveLocationIconsCategoriesThenVenues(List<Venue> venues) {
        logger.info("Saving location, then icons, then categories, then venues to database");
        for (int indexVenue = 0; indexVenue < venues.size(); indexVenue++) {
            Venue venue = venues.get(indexVenue);
            logger.info("Saving venue to databes: {}", venue.getName());
            Location location = venue.getLocation();
            if (location != null) { // it may be null because the foursquare API may have some blank values on response
                location.setVenueId(venue.getId());
                try {
                    location = locationService.getByVenueId(location.getVenueId());
                } catch (LocationNotFoundException e) {
                    location = locationService.save(location);
                }
                venue.setLocation(location);
            }
            List<Category> categories = venue.getCategories();
            if (categories != null) {
                categories = categoryService.saveIconsThenCategories(categories);
                venue.setCategories(categories);
            }
            Optional<Venue> venueOptional = venueRepository.findById(venue.getId());
            if (!venueOptional.isPresent()) {
                venue = venueRepository.save(venue);
            } else {
                venue = venueOptional.get();
            }
            venues.set(indexVenue, venue);
        }
        return venues;
    }

    @Override
    public VenueDetails getById(String id) {
        logger.info("Trying to search for cached venue details");
        Optional<VenueDetails> venueDetailsOptional = venueDetailsRepository.findById(id);
        if (venueDetailsOptional.isPresent()) {
            logger.info("Found venue details cached in the database...");
            return venueDetailsOptional.get();
        }
        String baseUrl = foursquareApiUtils.getUrlVenues() + "/" + id;
        String fullUrl = foursquareApiUtils.insertDefaultParametersIntoUrl(baseUrl);
        logger.info("Making GET request to: {}", fullUrl);
        try {
            ResponseWrapper responseWrapper = restTemplate.getForObject(
                    fullUrl,
                    ResponseWrapper.class,
                    foursquareApiUtils.getClientId(),
                    foursquareApiUtils.getClientSecret(),
                    foursquareApiUtils.getVersion()
            );
            logger.info("Venue retrieved");
            VenueDetails venueDetails = responseWrapper.getResponse().getVenue();
            venueDetails = saveLocationIconsCategoriesContactHoursTimeframesOpenPhotoThenVenues(venueDetails);
            return venueDetails;
        } catch (HttpClientErrorException exception) {
            logger.error("Throwing FoursquareHttpClientException with message: {}", exception.getMessage());
            throw new FoursquareHttpClientException(exception.getMessage());
        }
    }

    private VenueDetails saveLocationIconsCategoriesContactHoursTimeframesOpenPhotoThenVenues(VenueDetails venueDetails) {
        logger.info("Saving location, then icons, then categories, then contact, " +
                "then open, then timeframes, then hours, then photo, then venues to database");
        Location location = venueDetails.getLocation();
        if (location != null) {
            location.setVenueId(venueDetails.getId());
            try {
                location = locationService.getByVenueId(location.getVenueId());
            } catch (LocationNotFoundException e) {
                location = locationService.save(location);
            }
            venueDetails.setLocation(location);
        }
        List<Category> categories = venueDetails.getCategories();
        if (categories != null) {
            categories = categoryService.saveIconsThenCategories(categories);
            venueDetails.setCategories(categories);
        }
        Contact contact = venueDetails.getContact();
        if (contact != null) {
            contact.setVenueId(venueDetails.getId());
            try {
                contact = contactService.getByVenueId(contact.getVenueId());
            } catch (ContactNotFoundException e) {
                contact = contactService.save(contact);
            }
            venueDetails.setContact(contact);
        }
        Hours hours = venueDetails.getHours();
        if (hours != null) {
            hours.setVenueId(venueDetails.getId());
            hours = hoursService.saveOpenTimeframesThenHours(hours);
            venueDetails.setHours(hours);
        }
        Photo bestPhoto = venueDetails.getBestPhoto();
        if (bestPhoto != null) {
            try {
                bestPhoto = photoService.getById(bestPhoto.getId());
            } catch (BestPhotoNotFoundException e) {
                bestPhoto = photoService.save(bestPhoto);
            }
            venueDetails.setBestPhoto(bestPhoto);
        }
        Optional<VenueDetails> venueDetailsOptional = venueDetailsRepository.findById(venueDetails.getId());
        if (!venueDetailsOptional.isPresent()) {
            venueDetails = venueDetailsRepository.save(venueDetails);
        } else {
            venueDetails = venueDetailsOptional.get();
        }
        return venueDetails;
    }

    @Override
    public InputStream getBestPhoto(String venueId) throws BestPhotoStreamException {
        logger.info("Trying to search for cached best photo: {}", venueId);
        Optional<VenueDetails> venueDetailsOptional = venueDetailsRepository.findById(venueId);
        if (venueDetailsOptional.isPresent()) {
            logger.info("Found venue details cached in the database...");
            VenueDetails venueDetails = venueDetailsOptional.get();
            Photo bestPhoto = venueDetails.getBestPhoto();
            Optional<PhotoInputStream> inputStreamBestPhotoOptional = photoInputStreamRepository.findById(bestPhoto.getId());
            if (inputStreamBestPhotoOptional.isPresent()) {
                logger.info("Found best photo cached in the database...");
                byte[] byteArrayPhoto = inputStreamBestPhotoOptional.get().getByteArray();
                return new ByteArrayInputStream(byteArrayPhoto);
            }
        }
        logger.info("GET Request for a venues with id: {}", venueId);
        VenueDetails venueDetails = this.getById(venueId);
        Photo bestPhoto = venueDetails.getBestPhoto();
        String bestPhotoUrl = bestPhoto.getPrefix() + bestPhoto.getWidth() + "x" + bestPhoto.getHeight() + bestPhoto.getSuffix();
        String fullUrl = foursquareApiUtils.insertDefaultParametersIntoUrl(bestPhotoUrl);
        try {
            logger.info("Creating input stream for venue's best photo: {}", fullUrl);
            URL url = new URL(fullUrl);
            InputStream inputStream = new BufferedInputStream(url.openStream());
            logger.info("Saving best photo to database: {}", bestPhoto.getId());
            byte[] byteArrayPhoto = this.extractByteArrayFromInputStream(inputStream);
            PhotoInputStream photoInputStream = new PhotoInputStream(bestPhoto.getId(), byteArrayPhoto);
            photoInputStreamRepository.save(photoInputStream);
            return new ByteArrayInputStream(byteArrayPhoto);
        } catch (IOException e) {
            logger.info("Cannot retrieve venue's best photo: {} - reason: {}", fullUrl, e.getMessage());
            throw new BestPhotoStreamException("Cannot retrieve best photo: " + e.getMessage());
        }
    }

    private byte[] extractByteArrayFromInputStream(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int read = 0;
        while ((read = inputStream.read(buffer, 0, buffer.length)) != -1) {
            baos.write(buffer, 0, read);
        }
        baos.flush();
        return baos.toByteArray();
    }

    @Override
    public VenueSchedule addVenueToSchedule(User currentUser, String venueId) {
        try {
            VenueSchedule venueSchedule = venueScheduleService.getById(currentUser.getId());
            List<SimpleVenue> simpleVenues = venueSchedule.getSimpleVenues();
            boolean venueAlreadyInSchedule = false;
            for (SimpleVenue simpleVenue : simpleVenues) {
                if (simpleVenue.getId().equals(venueId)) {
                    venueAlreadyInSchedule = true;
                }
            }
            if (!venueAlreadyInSchedule) {
                SimpleVenue simpleVenue = new SimpleVenue(venueId);
                simpleVenueService.save(simpleVenue);
                simpleVenues.add(simpleVenue);
                venueSchedule.setSimpleVenues(simpleVenues);
                venueSchedule = venueScheduleService.update(venueSchedule);
            }
            return venueSchedule;
        } catch (VenueScheduleNotFoundException e) {
            logger.error("Venue schedule not found: {}", currentUser.getId());
            logger.info("Creating a venue schedule for the user: {}", currentUser.getId());
            List<SimpleVenue> simpleVenues = new ArrayList<>();
            SimpleVenue simpleVenue = new SimpleVenue(venueId);
            simpleVenueService.save(simpleVenue);
            simpleVenues.add(simpleVenue);
            VenueSchedule venueSchedule = new VenueSchedule(currentUser.getId(), simpleVenues);
            return venueScheduleService.save(venueSchedule);
        }
    }

    @Override
    public void removeVenueFromSchedule(User currentUser, String venueId) {
        try {
            VenueSchedule venueSchedule = venueScheduleService.getById(currentUser.getId());
            List<SimpleVenue> simpleVenues = venueSchedule.getSimpleVenues();
            boolean venueNotPresent = true;
            SimpleVenue simeplVenueToDelete = null;
            for (SimpleVenue simpleVenue : simpleVenues) {
                if (simpleVenue.getId().equals(venueId)) {
                    venueNotPresent = false;
                    simeplVenueToDelete = simpleVenue;
                }
            }
            if (venueNotPresent) {
                throw new SimpleVenueNotFoundException(venueId);
            } else {
                simpleVenues.remove(simeplVenueToDelete);
                venueScheduleService.save(venueSchedule);
            }
        } catch (VenueScheduleNotFoundException e) {
            logger.error("Venue schedule not found: {}", currentUser.getId());
            logger.info("No venue from schedule to delete for the user: {}", currentUser.getId());
            throw new SimpleVenueNotFoundException(venueId);
        }
    }

    @Override
    public List<VenueDetails> getVenueDetailsListFromScheduleOfUser(Long userId) {
        logger.info("Retreiving venue details list from schedule of user: {}", userId);
        VenueSchedule venueScheduleOfUser = venueScheduleService.getById(userId);
        List<VenueDetails> venueDetailsList = new ArrayList<>();
        List<SimpleVenue> simpleVenueList = venueScheduleOfUser.getSimpleVenues();
        for(SimpleVenue simpleVenue : simpleVenueList) {
            VenueDetails venueDetails = this.getById(simpleVenue.getId());
            logger.info("Adding venue {} to schedue list of user", venueDetails.getName());
            venueDetailsList.add(venueDetails);
        }
        return venueDetailsList;
    }

    @Override
    public Boolean checkVeneuePresenceInSchedule(String venueId, Long userId) {
        logger.info("Checking if venue ({}) is present in schedule of user: {}", venueId, userId);
        VenueSchedule venueSchedule = venueScheduleService.getById(userId);
        List<SimpleVenue> simpleVenues = venueSchedule.getSimpleVenues();
        for(SimpleVenue simpleVenue : simpleVenues) {
            if(simpleVenue.getId().equals(venueId)) {
                return true;
            }
        }
        return false;
    }
}
