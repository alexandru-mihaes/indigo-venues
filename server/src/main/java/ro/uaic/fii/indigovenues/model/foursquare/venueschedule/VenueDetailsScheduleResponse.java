package ro.uaic.fii.indigovenues.model.foursquare.venueschedule;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.VenueDetails;

import java.util.List;
import java.util.Objects;

@ApiModel(description = "Details about the list of venue details from the schedule of a user")
public class VenueDetailsScheduleResponse {
    @ApiModelProperty(notes = "List of venue details from the schedule of a user")
    List<VenueDetails> venueDetailsList;

    public VenueDetailsScheduleResponse() {
    }

    public VenueDetailsScheduleResponse(List<VenueDetails> venueDetailsList) {
        this.venueDetailsList = venueDetailsList;
    }

    public List<VenueDetails> getVenueDetailsList() {
        return venueDetailsList;
    }

    public void setVenueDetailsList(List<VenueDetails> venueDetailsList) {
        this.venueDetailsList = venueDetailsList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VenueDetailsScheduleResponse that = (VenueDetailsScheduleResponse) o;
        return Objects.equals(venueDetailsList, that.venueDetailsList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(venueDetailsList);
    }
}
