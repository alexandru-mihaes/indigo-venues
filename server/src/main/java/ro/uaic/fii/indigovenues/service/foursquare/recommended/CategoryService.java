package ro.uaic.fii.indigovenues.service.foursquare.recommended;

import ro.uaic.fii.indigovenues.model.foursquare.recommended.Category;

import java.util.List;

public interface CategoryService {
    Category getById(String id);

    Category save(Category category);

    List<Category> saveIconsThenCategories(List<Category> categories);
}
