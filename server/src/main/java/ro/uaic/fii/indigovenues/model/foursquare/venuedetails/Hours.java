package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@ApiModel(description = "Contains the hours during the week that the venuedetails is open along with any named hours segments in a human-readable format.")
public class Hours implements Serializable {
    @JsonIgnore
    @Id
    private String venueId;

    @ApiModelProperty(notes = "Human-readable format for the current status regarding the hours of a venuedetails")
    private String status;

    @ApiModelProperty(notes = "Boolean that indicates if the venuedetails is open right now")
    private boolean isOpen;

    @OneToMany(fetch = FetchType.EAGER)
    @ApiModelProperty(notes = "An array of timeframes of open hours.")
    private List<Timeframe> timeframes;

    public Hours() {
    }

    public Hours(String status, boolean isOpen, List<Timeframe> timeframes) {
        this.status = status;
        this.isOpen = isOpen;
        this.timeframes = timeframes;
    }

    public Hours(String venueId, String status, boolean isOpen, List<Timeframe> timeframes) {
        this.venueId = venueId;
        this.status = status;
        this.isOpen = isOpen;
        this.timeframes = timeframes;
    }

    public String getVenueId() {
        return venueId;
    }

    public String getStatus() {
        return status;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public List<Timeframe> getTimeframes() {
        return timeframes;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public void setTimeframes(List<Timeframe> timeframes) {
        this.timeframes = timeframes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hours hours = (Hours) o;
        return isOpen == hours.isOpen &&
                Objects.equals(venueId, hours.venueId) &&
                Objects.equals(status, hours.status) &&
                Objects.equals(timeframes, hours.timeframes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(venueId, status, isOpen, timeframes);
    }
}
