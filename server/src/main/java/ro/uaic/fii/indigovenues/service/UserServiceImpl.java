package ro.uaic.fii.indigovenues.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.EmailAlreadyInUseException;
import ro.uaic.fii.indigovenues.exception.UserNotFoundException;
import ro.uaic.fii.indigovenues.exception.UsernameAlreadyTakenException;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.repository.UserRepository;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User getUserByUsernameOrEmail(String usernameOrEmail) {
        Optional<User> user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
        if (user.isPresent()) {
            logger.info("Found user by username/email: {}", usernameOrEmail);
            return user.get();
        } else {
            logger.error("User not found by username/email: {}", usernameOrEmail);
            throw new UserNotFoundException(usernameOrEmail);
        }
    }

    @Override
    public User getUserByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            logger.info("Found user by username: {}", username);
            return user.get();
        } else {
            logger.error("User not found by username: {}", username);
            throw new UserNotFoundException(username);
        }
    }

    @Override
    public User save(User user) {
        logger.info("Saving user: {}", user.getUsername());
        User userResult = userRepository.save(user);
        return userResult;
    }

    @Override
    public User update(User user) {
        Optional<User> existingUserOptional = userRepository.findById(user.getId());
        if (existingUserOptional.isPresent()) {
            User existingUser = existingUserOptional.get();
            existingUser.update(user);
            existingUser.setPassword(passwordEncoder.encode(existingUser.getPassword()));
            logger.info("Updating user: {}", user.getUsername());
            User userResult = userRepository.save(existingUser);
            return userResult;
        } else {
            logger.error("User not found when trying to update: {} - {}", user.getUsername(), user.getEmail());
            throw new UserNotFoundException(user.getId());
        }
    }

    /**
     * checking if a user with the given credentials already exists
     */
    @Override
    public void checkUserDataExists(String username, String email) {
        logger.info("Checking if user data exists: {} - {}", username, email);
        checkExistenceByUsername(username);
        checkExistenceByEmail(email);
    }

    @Override
    public void checkUserDataExists(User currentUser, String username, String email) {
        logger.info("Checking if user data exists (except current user): {} - {}", username, email);
        if (!(currentUser.getUsername()).equals(username)) {
            checkExistenceByUsername(username);
        }
        if (!(currentUser.getEmail()).equals(email)) {
            checkExistenceByEmail(email);
        }
    }

    private void checkExistenceByUsername(String username) {
        if (userRepository.existsByUsername(username)) {
            logger.error("Conflict with username: {}", username);
            throw new UsernameAlreadyTakenException();
        }
    }

    private void checkExistenceByEmail(String email) {
        if (userRepository.existsByEmail(email)) {
            logger.error("Conflict with email: {}", email);
            throw new EmailAlreadyInUseException();
        }
    }

    @Override
    public boolean oldPasswordIsValid(User user, String oldPassword) {
        return passwordEncoder.matches(oldPassword, user.getPassword());
    }
}
