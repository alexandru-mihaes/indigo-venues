package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Entity
@ApiModel(description = "An object containing phone and formattedPhone of a venuedetails")
public class Contact implements Serializable {
    @JsonIgnore
    @Id
    private String venueId;

    @ApiModelProperty(notes = "Phone number")
    private String phone;

    @ApiModelProperty(notes = "Formatted phone number")
    private String formattedPhone;

    public Contact() {
    }

    public Contact(String phone, String formattedPhone) {
        this.phone = phone;
        this.formattedPhone = formattedPhone;
    }

    public Contact(String venueId, String phone, String formattedPhone) {
        this.venueId = venueId;
        this.phone = phone;
        this.formattedPhone = formattedPhone;
    }

    public String getVenueId() {
        return venueId;
    }

    public String getPhone() {
        return phone;
    }

    public String getFormattedPhone() {
        return formattedPhone;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setFormattedPhone(String formattedPhone) {
        this.formattedPhone = formattedPhone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return Objects.equals(venueId, contact.venueId) &&
                Objects.equals(phone, contact.phone) &&
                Objects.equals(formattedPhone, contact.formattedPhone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(venueId, phone, formattedPhone);
    }
}
