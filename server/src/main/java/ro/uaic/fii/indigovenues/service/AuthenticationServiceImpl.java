package ro.uaic.fii.indigovenues.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.InvalidTokenException;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.LoginRequest;
import ro.uaic.fii.indigovenues.payload.request.SignUpRequest;
import ro.uaic.fii.indigovenues.security.model.JwtUserDetails;
import ro.uaic.fii.indigovenues.security.service.JwtTokenProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    private UserService userService;
    private AuthenticationManager authenticationManager;
    private PasswordEncoder passwordEncoder;
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    public AuthenticationServiceImpl(UserService userService,
                                     AuthenticationManager authenticationManager,
                                     PasswordEncoder passwordEncoder,
                                     JwtTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public String authenticateUser(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);
        logger.info("Token generated for user with username/email: {}", loginRequest.getUsernameOrEmail());
        return jwt;
    }

    @Override
    public User createUser(SignUpRequest signUpRequest) {
        logger.info("Creating new user: {} - {}", signUpRequest.getUsername(), signUpRequest.getEmail());
        User user = new User(
                signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                signUpRequest.getPassword()
        );
        logger.info("Encrypting password...");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User userResult = userService.save(user);
        return userResult;
    }

    @Override
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.isAuthenticated() && !isAnonymous(authentication)) {
            JwtUserDetails jwtUserDetails = (JwtUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            User user = userService.getUserByUsername(jwtUserDetails.getUsername());
            logger.info("Returning current user: {}", user.getUsername());
            return user;
        } else {
            logger.error("Invalid token. Cannot get current user");
            throw new InvalidTokenException();
        }
    }

    private boolean isAnonymous(final Authentication authentication) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        List<String> roles = new ArrayList<String>();
        for (GrantedAuthority authority : authorities) {
            roles.add(authority.getAuthority());
        }
        return roles.contains("ROLE_ANONYMOUS");
    }
}
