package ro.uaic.fii.indigovenues.service.foursquare.venueschedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.venueschedule.SimpleVenueNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.SimpleVenue;
import ro.uaic.fii.indigovenues.repository.foursquare.venueschedule.SimpleVenueRepository;

@Service
public class SimpleVenueServiceImpl implements SimpleVenueService {
    private static final Logger logger = LoggerFactory.getLogger(SimpleVenueServiceImpl.class);

    private SimpleVenueRepository simpleVenueRepository;

    @Autowired
    public SimpleVenueServiceImpl(SimpleVenueRepository simpleVenueRepository) {
        this.simpleVenueRepository = simpleVenueRepository;
    }

    @Override
    public SimpleVenue getById(String id) throws SimpleVenueNotFoundException {
        return simpleVenueRepository.findById(id).orElseThrow(() -> new SimpleVenueNotFoundException(id));
    }

    @Override
    public SimpleVenue save(SimpleVenue simpleVenue) {
        logger.info("Saving simple venue to database: {}", simpleVenue.getId());
        return simpleVenueRepository.save(simpleVenue);
    }
}
