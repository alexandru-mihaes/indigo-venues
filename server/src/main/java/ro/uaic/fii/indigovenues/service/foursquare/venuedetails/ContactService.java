package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Contact;

public interface ContactService {
    Contact getByVenueId(String venueId);

    Contact save(Contact contact);
}
