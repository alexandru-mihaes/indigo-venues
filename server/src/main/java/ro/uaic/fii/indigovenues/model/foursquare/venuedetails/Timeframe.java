package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@ApiModel(description = "Details about a timeframe")
public class Timeframe implements Serializable {
    @JsonIgnore
    @EmbeddedId
    private TimeframeKey timeframeKey;

    @ApiModelProperty(notes = "Days on which these hours apply")
    private String days;

    @ApiModelProperty(notes = "Boolean that indicates if this timeframe includes today")
    private boolean includesToday;

    @OneToMany(fetch = FetchType.EAGER)
    @ApiModelProperty(notes = "An array describing the segments of the days in this timeframe in which the venuedetails is open. Each time segment has:" +
            "start - The time as HHMM (24hr) at which the segment begins; " +
            "end - The time as HHMM (24hr) at which the segment ends")
    private List<Open> open;

    public Timeframe() {
    }

    public Timeframe(String days, boolean includesToday, List<Open> open) {
        this.days = days;
        this.includesToday = includesToday;
        this.open = open;
    }

    public Timeframe(String venueId, String timeframeId, String days, boolean includesToday, List<Open> open) {
        this.timeframeKey = new TimeframeKey(venueId, timeframeId);
        this.days = days;
        this.includesToday = includesToday;
        this.open = open;
    }

    public String getVenueId() {
        return timeframeKey.getVeneuId();
    }

    public String getTimeframeId() {
        return timeframeKey.getTimeframeId();
    }

    public String getDays() {
        return days;
    }

    public boolean isIncludesToday() {
        return includesToday;
    }

    public List<Open> getOpen() {
        return open;
    }

    public void setVenueId(String venueId) {
        if(timeframeKey == null) {
            timeframeKey = new TimeframeKey();
        }
        this.timeframeKey.setVeneuId(venueId);
    }

    public void setTimeframeId(String timeframeId) {
        if(timeframeKey == null) {
            timeframeKey = new TimeframeKey();
        }
        this.timeframeKey.setTimeframeId(timeframeId);
    }

    public void setDays(String days) {
        this.days = days;
    }

    public void setIncludesToday(boolean includesToday) {
        this.includesToday = includesToday;
    }

    public void setOpen(List<Open> open) {
        this.open = open;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Timeframe timeframe = (Timeframe) o;
        return includesToday == timeframe.includesToday &&
                Objects.equals(timeframeKey, timeframe.timeframeKey) &&
                Objects.equals(days, timeframe.days) &&
                Objects.equals(open, timeframe.open);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeframeKey, days, includesToday, open);
    }
}
