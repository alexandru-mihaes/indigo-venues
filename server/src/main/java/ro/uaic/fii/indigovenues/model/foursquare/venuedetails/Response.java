package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import java.io.Serializable;
import java.util.Objects;

public class Response implements Serializable {
    VenueDetails venue;

    public Response() {
    }

    public Response(VenueDetails venue) {
        this.venue = venue;
    }

    public VenueDetails getVenue() {
        return venue;
    }

    public void setVenue(VenueDetails venue) {
        this.venue = venue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Response response = (Response) o;
        return Objects.equals(venue, response.venue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(venue);
    }
}
