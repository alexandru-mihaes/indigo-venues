package ro.uaic.fii.indigovenues.service.foursquare.recommended;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.recommended.IconNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.recommended.LocationNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Location;
import ro.uaic.fii.indigovenues.repository.foursquare.recommended.LocationRepository;

@Service
public class LocationServiceImpl implements LocationService {
    private static final Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);

    private LocationRepository locationRepository;

    @Autowired
    public LocationServiceImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @Override
    public Location getByVenueId(String venueId) throws LocationNotFoundException {
        logger.info("Getting location from database: {}", venueId);
        return locationRepository.findById(venueId).orElseThrow(() -> new LocationNotFoundException(venueId));
    }

    @Override
    public Location save(Location location) {
        logger.info("Saving location to database: {}", location.getAddress());
        return locationRepository.save(location);
    }
}
