package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class OpenKey implements Serializable {
    @Column(name = "Id", nullable = false)
    private String veneuId;

    @Column(name = "Timeframe", nullable = false)
    private String timeframeId;;

    @Column(name = "Open", nullable = false)
    private String openId;

    public OpenKey() {
    }

    public OpenKey(String veneuId, String timeframeId, String openId) {
        this.veneuId = veneuId;
        this.timeframeId = timeframeId;
        this.openId = openId;
    }

    public String getVeneuId() {
        return veneuId;
    }

    public String getTimeframeId() {
        return timeframeId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setVeneuId(String veneuId) {
        this.veneuId = veneuId;
    }

    public void setTimeframeId(String timeframeId) {
        this.timeframeId = timeframeId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpenKey openKey = (OpenKey) o;
        return Objects.equals(veneuId, openKey.veneuId) &&
                Objects.equals(timeframeId, openKey.timeframeId) &&
                Objects.equals(openId, openKey.openId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(veneuId, timeframeId, openId);
    }
}
