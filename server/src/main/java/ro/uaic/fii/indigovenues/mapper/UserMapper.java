package ro.uaic.fii.indigovenues.mapper;

import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.UserUpdateRequest;

@Service
public class UserMapper {
    public User map(UserUpdateRequest userUpdateRequest, Long id) {
        User user = new User();
        user.setId(id);
        user.setUsername(userUpdateRequest.getUsername());
        user.setEmail(userUpdateRequest.getEmail());
        user.setPassword(userUpdateRequest.getNewPassword());
        return user;
    }
}
