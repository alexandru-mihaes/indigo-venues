package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

@Entity
public class PhotoInputStream implements Serializable {
    @Id
    private String id;

    @Lob
    private byte[] byteArray;

    public PhotoInputStream() {
    }

    public PhotoInputStream(String id, byte[] byteArray) {
        this.id = id;
        this.byteArray = byteArray;
    }

    public String getId() {
        return id;
    }

    public byte[] getByteArray() {
        return byteArray;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhotoInputStream that = (PhotoInputStream) o;
        return Objects.equals(id, that.id) &&
                Arrays.equals(byteArray, that.byteArray);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id);
        result = 31 * result + Arrays.hashCode(byteArray);
        return result;
    }
}
