package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Category;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Location;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@ApiModel(description = "Details about a venuedetails")
public class VenueDetails implements Serializable {
    @Id
    @ApiModelProperty(notes = "A unique string identifier for this venuedetails")
    private String id;

    @ApiModelProperty(notes = "The best known name for this venuedetails")
    private String name;

    @ApiModelProperty(notes = "Location of the venuedetails")
    private Location location;

    @ManyToMany(fetch = FetchType.EAGER)
    @ApiModelProperty(notes = "An array of categories that have been applied to this venuedetails")
    private List<Category> categories;

    @ApiModelProperty(notes = "An object containing phone and formattedPhone of a venuedetails")
    private Contact contact;

    @ApiModelProperty(notes = "URL of the venuedetails’s website, typically provided by the venuedetails manager")
    private String url;

    @ApiModelProperty(notes = "Contains the hours during the week that the venuedetails is open along with any named hours segments in a human-readable format")
    private Hours hours;

    @ApiModelProperty(notes = "Photo we have determined to be the best photo for the venuedetails based on user upvotes and our internal algorithm")
    private Photo bestPhoto;

    public VenueDetails() {
    }

    public VenueDetails(String id, String name, Location location, List<Category> categories, Contact contact, String url, Hours hours, Photo bestPhoto) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.categories = categories;
        this.contact = contact;
        this.url = url;
        this.hours = hours;
        this.bestPhoto = bestPhoto;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public Contact getContact() {
        return contact;
    }

    public String getUrl() {
        return url;
    }

    public Hours getHours() {
        return hours;
    }

    public Photo getBestPhoto() {
        return bestPhoto;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setHours(Hours hours) {
        this.hours = hours;
    }

    public void setBestPhoto(Photo bestPhoto) {
        this.bestPhoto = bestPhoto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VenueDetails that = (VenueDetails) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(location, that.location) &&
                Objects.equals(categories, that.categories) &&
                Objects.equals(contact, that.contact) &&
                Objects.equals(url, that.url) &&
                Objects.equals(hours, that.hours) &&
                Objects.equals(bestPhoto, that.bestPhoto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, location, categories, contact, url, hours, bestPhoto);
    }
}
