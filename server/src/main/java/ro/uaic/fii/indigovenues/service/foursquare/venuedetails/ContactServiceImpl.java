package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.recommended.IconNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.recommended.LocationNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.ContactNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Location;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Contact;
import ro.uaic.fii.indigovenues.repository.foursquare.recommended.LocationRepository;
import ro.uaic.fii.indigovenues.repository.foursquare.venuedetails.ContactRepository;
import ro.uaic.fii.indigovenues.service.foursquare.recommended.LocationService;

@Service
public class ContactServiceImpl implements ContactService {
    private static final Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);

    private ContactRepository contactRepository;

    @Autowired
    public ContactServiceImpl(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public Contact getByVenueId(String venueId) throws ContactNotFoundException {
        logger.info("Getting contact from database: {}", venueId);
        return contactRepository.findById(venueId).orElseThrow(() -> new ContactNotFoundException(venueId));
    }

    @Override
    public Contact save(Contact contact) {
        logger.info("Saving contact to database: {}", contact.getPhone());
        return contactRepository.save(contact);
    }
}
