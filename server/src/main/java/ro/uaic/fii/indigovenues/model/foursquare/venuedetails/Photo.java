package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Photo implements Serializable {
    @Id
    @ApiModelProperty(notes = "A unique string identifier for this photo")
    private String id;

    @ApiModelProperty(notes = "Prefix of the photo")
    private String prefix;

    @ApiModelProperty(notes = "Suffix of the photo")
    private String suffix;

    @ApiModelProperty(notes = "Width of the photo")
    private String width;

    @ApiModelProperty(notes = "Height of the photo")
    private String height;

    public Photo() {
    }

    public Photo(String id, String prefix, String suffix, String width, String height) {
        this.id = id;
        this.prefix = prefix;
        this.suffix = suffix;
        this.width = width;
        this.height = height;
    }

    public String getId() {
        return id;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return Objects.equals(id, photo.id) &&
                Objects.equals(prefix, photo.prefix) &&
                Objects.equals(suffix, photo.suffix) &&
                Objects.equals(width, photo.width) &&
                Objects.equals(height, photo.height);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, prefix, suffix, width, height);
    }
}
