package ro.uaic.fii.indigovenues.exception.foursquare.venueschedule;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Venue schedule not found")
public class VenueScheduleNotFoundException extends RuntimeException {
    public VenueScheduleNotFoundException(Long id) {
        super("Venue schedule with id " + id + " does not exist.");
    }
}