package ro.uaic.fii.indigovenues.repository.foursquare.recommended;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Venue;

import java.util.List;

@Repository
public interface VenueRepository extends CrudRepository<Venue, String> {
    @Override
    List<Venue> findAll();
}
