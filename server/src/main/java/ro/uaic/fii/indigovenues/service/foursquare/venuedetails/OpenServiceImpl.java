package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.OpenNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Open;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.OpenKey;
import ro.uaic.fii.indigovenues.repository.foursquare.venuedetails.OpenRepository;

@Service
public class OpenServiceImpl implements OpenService {
    private static final Logger logger = LoggerFactory.getLogger(OpenServiceImpl.class);

    private OpenRepository openRepository;

    @Autowired
    public OpenServiceImpl(OpenRepository openRepository) {
        this.openRepository = openRepository;
    }

    @Override
    public Open getByVenueIdTimeframeIdAndOpenId(String venueId, String timeframeId, String openId) throws OpenNotFoundException {
        logger.info("Getting open from database: {}", venueId);
        return openRepository.findById(new OpenKey(venueId, timeframeId, openId)).orElseThrow(() -> new OpenNotFoundException(venueId));
    }

    @Override
    public Open save(Open open) {
        logger.info("Saving open to database: {}", open.getRenderedTime());
        return openRepository.save(open);
    }
}
