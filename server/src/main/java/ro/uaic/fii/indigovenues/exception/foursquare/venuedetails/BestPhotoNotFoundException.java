package ro.uaic.fii.indigovenues.exception.foursquare.venuedetails;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Best photo not found")
public class BestPhotoNotFoundException extends RuntimeException {
    public BestPhotoNotFoundException(String id) {
        super("Best photo with id " + id + " does not exist.");
    }
}