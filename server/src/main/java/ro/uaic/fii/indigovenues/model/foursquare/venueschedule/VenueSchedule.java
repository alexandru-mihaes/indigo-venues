package ro.uaic.fii.indigovenues.model.foursquare.venueschedule;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;
import java.util.Objects;

@Entity
public class VenueSchedule {
    @Id
    private Long id;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<SimpleVenue> simpleVenues;

    public VenueSchedule() {
    }

    public VenueSchedule(Long id, List<SimpleVenue> simpleVenues) {
        this.id = id;
        this.simpleVenues = simpleVenues;
    }

    public Long getId() {
        return id;
    }

    public List<SimpleVenue> getSimpleVenues() {
        return simpleVenues;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSimpleVenues(List<SimpleVenue> simpleVenues) {
        this.simpleVenues = simpleVenues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VenueSchedule that = (VenueSchedule) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(simpleVenues, that.simpleVenues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, simpleVenues);
    }

    public void update(VenueSchedule userSchedule) {
        id = userSchedule.getId();
        simpleVenues = userSchedule.getSimpleVenues();
    }
}
