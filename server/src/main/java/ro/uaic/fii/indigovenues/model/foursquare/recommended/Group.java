package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Group implements Serializable {
    List<Item> items;

    public Group() {
    }

    public Group(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(items, group.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(items);
    }
}