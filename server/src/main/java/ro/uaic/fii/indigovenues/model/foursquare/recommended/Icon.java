package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@ApiModel(description = "Pieces needed to construct category icons at various sizes. " +
        "Combine prefix with a size (32, 44, 64, and 88 are available) and suffix, " +
        "e.g. https://foursquare.com/img/categories/food/default_64.png. " +
        "To get an image with a gray background, use bg_ before the size, " +
        "e.g. https://foursquare.com/img/categories_v2/food/icecream_bg_32.png")
public class Icon implements Serializable {
    @Id
    @ApiModelProperty(notes = "Prefix of the icon")
    private String prefix;

    private static final String SIZE_32 = "bg_32";
    private static final String SIZE_44 = "bg_44";
    private static final String SIZE_64 = "bg_64";
    private static final String SIZE_88 = "bg_88";

    @ApiModelProperty(notes = "Suffix of the icon")
    private String suffix;

    public Icon() {
    }

    public Icon(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}