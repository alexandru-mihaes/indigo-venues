package ro.uaic.fii.indigovenues.repository.foursquare.recommended;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Icon;

@Repository
public interface IconRepository extends CrudRepository<Icon, String> {
}