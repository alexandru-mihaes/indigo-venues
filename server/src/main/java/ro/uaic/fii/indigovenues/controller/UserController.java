package ro.uaic.fii.indigovenues.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.uaic.fii.indigovenues.exception.InvalidOldPasswordException;
import ro.uaic.fii.indigovenues.mapper.UserMapper;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.UserUpdateRequest;
import ro.uaic.fii.indigovenues.payload.response.ServerResponse;
import ro.uaic.fii.indigovenues.service.AuthenticationService;
import ro.uaic.fii.indigovenues.service.UserService;
import ro.uaic.fii.indigovenues.validator.UserUpdateRequestValidator;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
@Api(value = "User")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private AuthenticationService authenticationService;
    private UserService userService;
    private UserMapper userMapper;

    @Autowired
    public UserController(AuthenticationService authenticationService, UserService userService, UserMapper userMapper) {
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @ApiOperation(value = "Update user", response = ServerResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User updated successfully"),
            @ApiResponse(code = 400, message = "Credentials are not valid"),
            @ApiResponse(code = 409, message = "User with those credentials already exists")
    })
    @PutMapping("/update")
    public ResponseEntity<?> update(@Valid @RequestBody UserUpdateRequest userUpdateRequest, BindingResult bindingResult) {
        User currentUser = authenticationService.getCurrentUser();
        logger.info("Update user request made by user: {}", currentUser.getUsername());
        if (!userService.oldPasswordIsValid(currentUser, userUpdateRequest.getOldPassword())) {
            logger.error("Invalid old password sent by username: {}", userUpdateRequest.getUsername());
            throw new InvalidOldPasswordException();
        }

        // validating the user update request
        UserUpdateRequestValidator userUpdateRequestValidator = new UserUpdateRequestValidator(userUpdateRequest.getUsername());
        userUpdateRequestValidator.validate(userUpdateRequest, bindingResult);
        userService.checkUserDataExists(currentUser, userUpdateRequest.getUsername(), userUpdateRequest.getEmail());

        // updating an existing user
        logger.info("Updating user: {} - {} to {} - {}",
                currentUser.getUsername(), currentUser.getEmail(),
                userUpdateRequest.getUsername(), userUpdateRequest.getEmail());
        userService.update(userMapper.map(userUpdateRequest, currentUser.getId()));
        return ResponseEntity.ok(new ServerResponse("User updated successfully"));
    }
}
