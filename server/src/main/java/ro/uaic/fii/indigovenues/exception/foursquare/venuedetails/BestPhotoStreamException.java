package ro.uaic.fii.indigovenues.exception.foursquare.venuedetails;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BestPhotoStreamException extends RuntimeException {
    public BestPhotoStreamException(String message) {
        super(message);
    }
}
