package ro.uaic.fii.indigovenues.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.LoginRequest;
import ro.uaic.fii.indigovenues.payload.request.SignUpRequest;
import ro.uaic.fii.indigovenues.payload.response.JwtAuthenticationResponse;
import ro.uaic.fii.indigovenues.payload.response.ServerResponse;
import ro.uaic.fii.indigovenues.service.AuthenticationService;
import ro.uaic.fii.indigovenues.service.UserService;
import ro.uaic.fii.indigovenues.validator.SignUpRequestValidator;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/auth")
@Api(value = "Authentication")
public class AuthenticationController {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    private UserService userService;
    private AuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(UserService userService, AuthenticationService authenticationService) {
        this.userService = userService;
        this.authenticationService = authenticationService;
    }

    @ApiOperation(value = "Authenticate user", response = JwtAuthenticationResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User authenticated successfully"),
            @ApiResponse(code = 400, message = "Bad credentials")
    })
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        logger.info("Sign in request with username/email: {}", loginRequest.getUsernameOrEmail());
        String jwt = authenticationService.authenticateUser(loginRequest);
        User user = userService.getUserByUsernameOrEmail(loginRequest.getUsernameOrEmail());
        return ResponseEntity.ok(
                new JwtAuthenticationResponse(
                        jwt,
                        user.getUsername(),
                        user.getEmail(),
                        "User authenticated successfully"
                )
        );
    }

    @ApiOperation(value = "Register user", response = ServerResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "User registered successfully"),
            @ApiResponse(code = 400, message = "Credentials are not valid"),
            @ApiResponse(code = 409, message = "User with those credentials already exists")
    })
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest, BindingResult bindingResult) {
        logger.info("Sign up request with username: {}", signUpRequest.getUsername());

        // validating the sign up request
        SignUpRequestValidator signUpRequestValidator = new SignUpRequestValidator(signUpRequest.getUsername());
        signUpRequestValidator.validate(signUpRequest, bindingResult);
        userService.checkUserDataExists(signUpRequest.getUsername(), signUpRequest.getEmail());

        // creating a new user
        User userResult = authenticationService.createUser(signUpRequest);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(userResult.getUsername()).toUri();
        return ResponseEntity.created(location).body(new ServerResponse("User registered successfully"));
    }
}
