package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Entity
@ApiModel(description = "Details about the location")
public class Location implements Serializable {
    @JsonIgnore
    @Id
    private String venueId;

    @ApiModelProperty(notes = "Location's address")
    private String address;

    @ApiModelProperty(notes = "Location's latitude")
    private Double lat;

    @ApiModelProperty(notes = "Location's longitude")
    private Double lng;

    @ApiModelProperty(notes = "Location's country")
    private String country;

    @ApiModelProperty(notes = "Location's state")
    private String state;

    @ApiModelProperty(notes = "Location's city")
    private String city;

    public Location() {
    }

    public Location(String address, Double lat, Double lng, String country, String state, String city) {
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.country = country;
        this.state = state;
        this.city = city;
    }

    public Location(String venueId, String address, Double lat, Double lng, String country, String state, String city) {
        this.venueId = venueId;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.country = country;
        this.state = state;
        this.city = city;
    }

    public String getVenueId() {
        return venueId;
    }

    public String getAddress() {
        return address;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(venueId, location.venueId) &&
                Objects.equals(address, location.address) &&
                Objects.equals(lat, location.lat) &&
                Objects.equals(lng, location.lng) &&
                Objects.equals(country, location.country) &&
                Objects.equals(state, location.state) &&
                Objects.equals(city, location.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(venueId, address, lat, lng, country, state, city);
    }
}