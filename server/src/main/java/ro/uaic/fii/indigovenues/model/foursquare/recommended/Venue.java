package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@ApiModel(description = "Details about the recommended venuedetails")
public class Venue implements Serializable {
    @Id
    @ApiModelProperty(notes = "A unique string identifier for this venuedetails")
    private String id;

    @ApiModelProperty(notes = "The best known name for this venuedetails")
    private String name;

    @ApiModelProperty(notes = "Location of the venuedetails")
    private Location location;

    @ManyToMany(fetch = FetchType.EAGER)
    @ApiModelProperty(notes = "An array of categories that have been applied to this venuedetails")
    private List<Category> categories;

    public Venue() {
    }

    public Venue(String id, String name, Location location, List<Category> categories) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.categories = categories;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venue venue = (Venue) o;
        return Objects.equals(id, venue.id) &&
                Objects.equals(name, venue.name) &&
                Objects.equals(location, venue.location) &&
                Objects.equals(categories, venue.categories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, location, categories);
    }
}
