package ro.uaic.fii.indigovenues.payload.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

@ApiModel(description = "All details about the user update request")
public class UserUpdateRequest {
    @ApiModelProperty(notes = "User's username")
    @NotBlank
    private String username;

    @ApiModelProperty(notes = "User's email")
    @NotBlank
    private String email;

    @ApiModelProperty(notes = "User's old password")
    @NotBlank
    private String oldPassword;

    @ApiModelProperty(notes = "User's new password")
    @NotBlank
    private String newPassword;

    public UserUpdateRequest() {
    }

    public UserUpdateRequest(@NotBlank String username, @NotBlank String email, @NotBlank String oldPassword, @NotBlank String newPassword) {
        this.username = username;
        this.email = email;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserUpdateRequest that = (UserUpdateRequest) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(email, that.email) &&
                Objects.equals(oldPassword, that.oldPassword) &&
                Objects.equals(newPassword, that.newPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, email, oldPassword, newPassword);
    }
}
