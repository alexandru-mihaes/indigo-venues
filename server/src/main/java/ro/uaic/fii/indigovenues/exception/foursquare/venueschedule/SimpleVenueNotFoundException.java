package ro.uaic.fii.indigovenues.exception.foursquare.venueschedule;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Simple venue not found")
public class SimpleVenueNotFoundException extends RuntimeException {
    public SimpleVenueNotFoundException(String id) {
        super("Simple venue with id " + id + " does not exist.");
    }
}