package ro.uaic.fii.indigovenues.service.foursquare.recommended;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.recommended.CategoryNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.recommended.IconNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Category;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Icon;
import ro.uaic.fii.indigovenues.repository.foursquare.recommended.CategoryRepository;
import ro.uaic.fii.indigovenues.service.foursquare.VenueServiceImpl;

import java.util.List;

@Service
public class CategotyServiceImpl implements CategoryService {
    private static final Logger logger = LoggerFactory.getLogger(VenueServiceImpl.class);

    private CategoryRepository categoryRepository;
    private IconService iconService;

    @Autowired
    public CategotyServiceImpl(CategoryRepository categoryRepository, IconService iconService) {
        this.categoryRepository = categoryRepository;
        this.iconService = iconService;
    }

    @Override
    public Category getById(String id) throws CategoryNotFoundException {
        logger.info("Getting category from database: {}", id);
        return categoryRepository.findById(id).orElseThrow(() -> new CategoryNotFoundException(id));
    }

    @Override
    public Category save(Category category) {
        logger.info("Saving category to database: {}", category.getId());
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> saveIconsThenCategories(List<Category> categories) {
        logger.info("Saving icons then categories to database");
        for (int indexCategory = 0; indexCategory < categories.size(); indexCategory++) {
            Category category = categories.get(indexCategory);
            Icon icon = category.getIcon();
            if (icon != null) {
                try {
                    icon = iconService.getByPrefix(icon.getPrefix());
                } catch (IconNotFoundException e) {
                    icon = iconService.save(icon);
                }
                category.setIcon(icon);
            }
            try {
                category = this.getById(category.getId());
            } catch (CategoryNotFoundException e) {
                category = this.save(category);
            }
            categories.set(indexCategory, category);
        }
        return categories;
    }
}
