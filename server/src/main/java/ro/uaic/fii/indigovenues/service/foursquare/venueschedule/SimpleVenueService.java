package ro.uaic.fii.indigovenues.service.foursquare.venueschedule;

import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.SimpleVenue;


public interface SimpleVenueService {
    SimpleVenue getById(String id);

    SimpleVenue save(SimpleVenue simpleVenue);
}
