package ro.uaic.fii.indigovenues.exception.foursquare.recommended;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Location not found")
public class LocationNotFoundException extends RuntimeException {
    public LocationNotFoundException(String venueId) {
        super("Location with venueId " + venueId + " does not exist.");
    }
}