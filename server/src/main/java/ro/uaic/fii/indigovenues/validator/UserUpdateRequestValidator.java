package ro.uaic.fii.indigovenues.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ro.uaic.fii.indigovenues.payload.request.SignUpRequest;
import ro.uaic.fii.indigovenues.payload.request.UserUpdateRequest;

public class UserUpdateRequestValidator extends UserDataValidator implements Validator {
    private static final Logger logger = LoggerFactory.getLogger(UserUpdateRequestValidator.class);

    public UserUpdateRequestValidator(String madeByUsername) {
        super(madeByUsername);
    }

    @Override
    public boolean supports(Class clazz) {
        return SignUpRequest.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserUpdateRequest userUpdateRequest = (UserUpdateRequest) target;
        if (!isUsernameValid(userUpdateRequest.getUsername())) {
            logger.error("Invalid username: {}", userUpdateRequest.getUsername());
            errors.rejectValue("username", USERNAME_VALIDATION_ERROR_CODE);
        }
        if (!isEmailValid(userUpdateRequest.getEmail())) {
            logger.error("Invalid email: {}", userUpdateRequest.getEmail());
            errors.rejectValue("email", EMAIL_VALIDATION_ERROR_CODE);
        }
        if (!isPasswordValid(userUpdateRequest.getNewPassword())) {
            logger.error("Invalid password");
            errors.rejectValue("newPassword", PASSWORD_VALIDATION_ERROR_CODE);
        }
        checkIfHasErrors(errors);
    }
}
