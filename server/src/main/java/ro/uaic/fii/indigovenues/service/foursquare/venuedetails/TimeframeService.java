package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Timeframe;

import java.util.List;

public interface TimeframeService {
    Timeframe getByVenueIdAndTimeframeId(String venueId, String timeframeId);

    Timeframe save(Timeframe timeframe);

    List<Timeframe> saveOpenThenTimeframes(List<Timeframe> timeframes);
}
