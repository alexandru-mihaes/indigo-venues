package ro.uaic.fii.indigovenues.exception.foursquare.venuedetails;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Hours not found")
public class HoursNotFoundException extends RuntimeException {
    public HoursNotFoundException(String venueId) {
        super("Hours with venueId " + venueId + " does not exist.");
    }
}