package ro.uaic.fii.indigovenues.repository.foursquare.venuedetails;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.PhotoInputStream;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.VenueDetails;

@Repository
public interface PhotoInputStreamRepository extends CrudRepository<PhotoInputStream, String> {
}
