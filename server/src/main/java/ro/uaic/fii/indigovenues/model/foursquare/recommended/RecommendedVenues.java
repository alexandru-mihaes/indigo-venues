package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import java.io.Serializable;
import java.util.Objects;

public class RecommendedVenues implements Serializable {
    Response response;

    public RecommendedVenues() {
    }

    public RecommendedVenues(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecommendedVenues that = (RecommendedVenues) o;
        return Objects.equals(response, that.response);
    }

    @Override
    public int hashCode() {
        return Objects.hash(response);
    }
}