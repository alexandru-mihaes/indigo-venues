package ro.uaic.fii.indigovenues.controller.foursquare;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.uaic.fii.indigovenues.exception.foursquare.BadRequestException;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.RecommendedVenuesResponse;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Venue;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.VenueDetails;
import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.VenueDetailsScheduleResponse;
import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.VenueSchedulePresent;
import ro.uaic.fii.indigovenues.payload.response.ServerResponse;
import ro.uaic.fii.indigovenues.service.AuthenticationService;
import ro.uaic.fii.indigovenues.service.foursquare.VenueService;

import java.io.InputStream;
import java.util.List;


@RestController
@RequestMapping("/api/venues")
@Api(value = "Venue")
public class VenueController {
    private static final Logger logger = LoggerFactory.getLogger(VenueController.class);

    private VenueService venueService;
    private AuthenticationService authenticationService;

    @Autowired
    public VenueController(VenueService venueService, AuthenticationService authenticationService) {
        this.venueService = venueService;
        this.authenticationService = authenticationService;
    }

    @ApiOperation(value = "Recommended venues for a specific location or found by latitude and longitude", response = RecommendedVenuesResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The list of recommended venues for a specific location or found by latitude and longitude"),
            @ApiResponse(code = 400, message = "Bad request")
    })
    @GetMapping("/explore")
    public ResponseEntity<?> getRecommended(@Param("location") String location, @Param("ll") String ll) {
        if (location != null && !location.isEmpty()) {
            logger.info("GET Request for recommended venues at location: {}", location);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            List<Venue> recommendedVenueList = venueService.getRecommended(location);
            return new ResponseEntity<>(new RecommendedVenuesResponse(recommendedVenueList), responseHeaders, HttpStatus.OK);
        } else if (ll != null && !ll.isEmpty()) {
            logger.info("GET Request for recommended found by latitude and longitude: {}", ll);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            List<Venue> recommendedVenueList = venueService.getRecommendedByLatAndLng(ll);
            return new ResponseEntity<>(new RecommendedVenuesResponse(recommendedVenueList), responseHeaders, HttpStatus.OK);
        } else {
            String message = "location or ll parameters should not be empty";
            logger.error("Throwing BadRequestException with message: {}", message);
            throw new BadRequestException(message);
        }
    }

    @ApiOperation(value = "Venue details by id", response = VenueDetails.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Venue details by id"),
            @ApiResponse(code = 400, message = "Bad request")
    })
    @GetMapping("/{venueId}")
    public ResponseEntity<?> getById(@PathVariable(value = "venueId") String venueId) {
        logger.info("GET Request for a venues with id: {}", venueId);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        VenueDetails venueDetails = venueService.getById(venueId);
        return new ResponseEntity<>(venueDetails, responseHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "Venue's best photo by venue's id", response = InputStreamResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Venue's best photo by venue's id"),
            @ApiResponse(code = 400, message = "Bad request")
    })
    @GetMapping("/best-photo/{venueId}")
    public ResponseEntity<InputStreamResource> getBestPhoto(@PathVariable(value = "venueId") String venueId) {
        logger.info("GET Request for the venue's best photo: {}", venueId);
        InputStream inputStream = venueService.getBestPhoto(venueId);
        logger.info("Venue's best photo retrieved: {}", venueId);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(String.valueOf(MediaType.IMAGE_JPEG)))
                .body(new InputStreamResource(inputStream));
    }

    @ApiOperation(value = "Update venue schedule list", response = ServerResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Schedule updated successfully"),
            @ApiResponse(code = 404, message = "Venue schedule not found")
    })
    @PutMapping("/schedule/{venueId}")
    public ResponseEntity<?> updateVenueSchedule(@PathVariable(value = "venueId") String venueId) {
        User currentUser = authenticationService.getCurrentUser();
        logger.info("PUT request - updating venue schedule - user {}  ", currentUser.getUsername());
        venueService.addVenueToSchedule(currentUser, venueId);
        return ResponseEntity.ok(new ServerResponse("Schedule updated successfully"));
    }

    @ApiOperation(value = "Remove venue from schedule list", response = ServerResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Schedule updated successfully"),
            @ApiResponse(code = 404, message = "Venue schedule not found")
    })
    @DeleteMapping("/schedule/{venueId}")
    public ResponseEntity<?> removeVenueFromSchedule(@PathVariable(value = "venueId") String venueId) {
        User currentUser = authenticationService.getCurrentUser();
        logger.info("DELETE request - updating venue schedule - user {}  ", currentUser.getUsername());
        venueService.removeVenueFromSchedule(currentUser, venueId);
        return ResponseEntity.ok(new ServerResponse("Schedule updated successfully"));
    }

    @ApiOperation(value = "List of venue details from schedule", response = VenueDetailsScheduleResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of venue details from schedule"),
            @ApiResponse(code = 400, message = "Bad request")
    })
    @GetMapping("/schedule")
    public ResponseEntity<?> getVenueDetailsFromSchedule() {
        User currentUser = authenticationService.getCurrentUser();
        logger.info("GET Request for the list of venue details from the schedule of the user: {}", currentUser.getId());
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        List<VenueDetails> venueDetailsListOfUser = venueService.getVenueDetailsListFromScheduleOfUser(currentUser.getId());
        VenueDetailsScheduleResponse venueDetailsScheduleResponse = new VenueDetailsScheduleResponse(venueDetailsListOfUser);
        return new ResponseEntity<>(venueDetailsScheduleResponse, responseHeaders, HttpStatus.OK);
    }

        @ApiOperation(value = "Check if venue is present in schedule", response = VenueSchedulePresent.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Venue retrieved from data base with the accrding presence result"),
            @ApiResponse(code = 400, message = "Bad request")
    })
    @GetMapping("/schedule/is-present/{venueId}")
    public ResponseEntity<?> checkVeneuePresenceInSchedule(@PathVariable(value = "venueId") String venueId) {
        User currentUser = authenticationService.getCurrentUser();
        logger.info("GET Request - checking if venue ({}) is present in schedule of user: {}", venueId, currentUser.getId());
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        Boolean isPresent = venueService.checkVeneuePresenceInSchedule(venueId, currentUser.getId());
        VenueSchedulePresent venueSchedulePresent = new VenueSchedulePresent(isPresent);
        return new ResponseEntity<>(venueSchedulePresent, responseHeaders, HttpStatus.OK);
    }
}
