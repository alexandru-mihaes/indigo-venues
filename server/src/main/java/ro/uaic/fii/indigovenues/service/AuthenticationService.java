package ro.uaic.fii.indigovenues.service;

import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.LoginRequest;
import ro.uaic.fii.indigovenues.payload.request.SignUpRequest;

public interface AuthenticationService {
    String authenticateUser(LoginRequest loginRequest);

    User createUser(SignUpRequest signUpRequest);

    User getCurrentUser();
}
