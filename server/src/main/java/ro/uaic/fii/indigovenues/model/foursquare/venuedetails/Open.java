package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Entity
@ApiModel(description = "Details about the start and end hour of a timeframe")
public class Open implements Serializable {
    @JsonIgnore
    @EmbeddedId
    private OpenKey openKey;

    @ApiModelProperty(notes = "Human-readable format for start and end hour")
    private String renderedTime;

    public Open() {
    }

    public Open(String renderedTime) {
        this.renderedTime = renderedTime;
    }

    public Open(String venueId, String timeframeId, String openId, String renderedTime) {
        this.openKey = new OpenKey(venueId, timeframeId, openId);
        this.openKey.setOpenId(openId);
        this.renderedTime = renderedTime;
    }

    public String getVenueId() {
        return openKey.getVeneuId();
    }

    public String getTimeframeId() {
        return openKey.getTimeframeId();
    }

    public String getOpenId() {
        return openKey.getOpenId();
    }

    public String getRenderedTime() {
        return renderedTime;
    }

    public void setVenueId(String venueId) {
        if(openKey == null) {
            openKey = new OpenKey();
        }
        this.openKey.setVeneuId(venueId);
    }

    public void setTimeframeId(String timeframeId) {
        if(openKey == null) {
            openKey = new OpenKey();
        }
        this.openKey.setTimeframeId(timeframeId);
    }

    public void setOpenId(String openId) {
        if(openKey == null) {
            openKey = new OpenKey();
        }
        this.openKey.setOpenId(openId);
    }

    public void setRenderedTime(String renderedTime) {
        this.renderedTime = renderedTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Open open = (Open) o;
        return Objects.equals(openKey, open.openKey) &&
                Objects.equals(renderedTime, open.renderedTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(openKey, renderedTime);
    }
}
