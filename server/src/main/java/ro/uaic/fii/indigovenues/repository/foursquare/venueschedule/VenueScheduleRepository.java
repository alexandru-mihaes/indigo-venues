package ro.uaic.fii.indigovenues.repository.foursquare.venueschedule;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.VenueSchedule;

import java.util.List;

@Repository
public interface VenueScheduleRepository extends CrudRepository<VenueSchedule, Long> {
    @Override
    List<VenueSchedule> findAll();
}
