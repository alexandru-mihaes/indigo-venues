package ro.uaic.fii.indigovenues.service.foursquare.recommended;

import ro.uaic.fii.indigovenues.model.foursquare.recommended.Icon;

public interface IconService {
    Icon getByPrefix(String prefix);

    Icon save(Icon icon);
}
