package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Response implements Serializable {
    Integer totalResults;
    List<Group> groups;

    public Response() {
    }

    public Response(Integer totalResults, List<Group> groups) {
        this.totalResults = totalResults;
        this.groups = groups;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Response response = (Response) o;
        return Objects.equals(totalResults, response.totalResults) &&
                Objects.equals(groups, response.groups);
    }

    @Override
    public int hashCode() {
        return Objects.hash(totalResults, groups);
    }
}