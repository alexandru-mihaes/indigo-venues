package ro.uaic.fii.indigovenues.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FoursquareApiUtils {
    @Value("${app.foursquare.clientId}")
    private String clientId;

    @Value("${app.foursquare.clientSecret}")
    private String clientSecret;

    @Value("${app.foursquare.version}")
    private String version;

    @Value("${app.foursquare.url.venues.recommended}")
    private String urlVenuesRecommended;

    @Value("${app.foursquare.url.venues}")
    private String urlVenues;

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getVersion() {
        return version;
    }

    public String getUrlVenuesRecommended() {
        return urlVenuesRecommended;
    }

    public String getUrlVenues() {
        return urlVenues;
    }

    public String insertDefaultParametersIntoUrl(String baseURL) {
        return baseURL + "?client_id={clientId}&client_secret={clientSecret}&v={version}";
    }
}
