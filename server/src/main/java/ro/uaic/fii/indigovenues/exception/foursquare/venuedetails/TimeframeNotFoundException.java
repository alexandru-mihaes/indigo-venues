package ro.uaic.fii.indigovenues.exception.foursquare.venuedetails;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Timeframe not found")
public class TimeframeNotFoundException extends RuntimeException {
    public TimeframeNotFoundException(String venueId) {
        super("Timeframe with venueId " + venueId + " does not exist.");
    }
}