package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Open;

public interface OpenService {
    Open getByVenueIdTimeframeIdAndOpenId(String venueId, String timeframeId, String openId);

    Open save(Open open);
}
