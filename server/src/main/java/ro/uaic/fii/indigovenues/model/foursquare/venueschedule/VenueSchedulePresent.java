package ro.uaic.fii.indigovenues.model.foursquare.venueschedule;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(description = "Details about the fact if venue is present in the schedule of a user")
public class VenueSchedulePresent {
    @ApiModelProperty(notes = "Boolean that indicates if the venue is present in the schedule of a user")
    private Boolean present;

    public VenueSchedulePresent() {
    }

    public VenueSchedulePresent(Boolean present) {
        this.present = present;
    }

    public Boolean getPresent() {
        return present;
    }

    public void setPresent(Boolean present) {
        this.present = present;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VenueSchedulePresent that = (VenueSchedulePresent) o;
        return Objects.equals(present, that.present);
    }

    @Override
    public int hashCode() {
        return Objects.hash(present);
    }
}
