package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Photo;

public interface PhotoService {
    Photo getById(String id);

    Photo save(Photo photo);
}
