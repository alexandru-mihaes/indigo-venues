package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.OpenNotFoundException;
import ro.uaic.fii.indigovenues.exception.foursquare.venuedetails.TimeframeNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Open;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Timeframe;
import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.TimeframeKey;
import ro.uaic.fii.indigovenues.repository.foursquare.venuedetails.TimeframeRepository;

import java.util.List;

@Service
public class TimeframeServiceImpl implements TimeframeService {
    private static final Logger logger = LoggerFactory.getLogger(TimeframeServiceImpl.class);

    private TimeframeRepository timeframeRepository;
    private OpenService openService;

    @Autowired
    public TimeframeServiceImpl(TimeframeRepository timeframeRepository, OpenService openService) {
        this.timeframeRepository = timeframeRepository;
        this.openService = openService;
    }

    @Override
    public Timeframe getByVenueIdAndTimeframeId(String venueId, String timeframeId) throws TimeframeNotFoundException {
        logger.info("Getting timeframe from database: {}", venueId);
        return timeframeRepository.findById(new TimeframeKey(venueId, timeframeId)).orElseThrow(() -> new TimeframeNotFoundException(venueId));
    }

    @Override
    public Timeframe save(Timeframe timeframe) {
        logger.info("Saving timeframe to database: {}", timeframe.getVenueId());
        return timeframeRepository.save(timeframe);
    }

    @Override
    public List<Timeframe> saveOpenThenTimeframes(List<Timeframe> timeframes) {
        for (int indexTimeframe = 0; indexTimeframe < timeframes.size(); indexTimeframe++) {
            Timeframe timeframe = timeframes.get(indexTimeframe);
            List<Open> openList = timeframe.getOpen();
            for (Integer indexOpen = 0; indexOpen < openList.size(); indexOpen++) {
                Open open = openList.get(indexOpen);
                open.setVenueId(timeframe.getVenueId());
                open.setTimeframeId(timeframe.getTimeframeId());
                open.setOpenId(indexOpen.toString());
                try {
                    open = openService.getByVenueIdTimeframeIdAndOpenId(open.getVenueId(), open.getTimeframeId(), open.getOpenId());
                } catch (OpenNotFoundException e) {
                    open = openService.save(open);
                }
                openList.set(indexOpen, open);
            }
            timeframe.setOpen(openList);
            try {
                timeframe = this.getByVenueIdAndTimeframeId(timeframe.getVenueId(), timeframe.getTimeframeId());
            } catch (TimeframeNotFoundException e) {
                timeframe = this.save(timeframe);
            }
            timeframes.set(indexTimeframe, timeframe);
        }
        return timeframes;
    }
}
