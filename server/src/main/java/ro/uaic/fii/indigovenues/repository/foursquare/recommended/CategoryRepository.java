package ro.uaic.fii.indigovenues.repository.foursquare.recommended;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.uaic.fii.indigovenues.model.foursquare.recommended.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category, String> {
}