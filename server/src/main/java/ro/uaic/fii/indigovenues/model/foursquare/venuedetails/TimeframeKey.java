package ro.uaic.fii.indigovenues.model.foursquare.venuedetails;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TimeframeKey implements Serializable {
    @Column(name = "Id", nullable = false)
    private String veneuId;

    @Column(name = "Timeframe", nullable = false)
    private String timeframeId;

    public TimeframeKey() {
    }

    public TimeframeKey(String veneuId, String timeframeId) {
        this.veneuId = veneuId;
        this.timeframeId = timeframeId;
    }

    public String getVeneuId() {
        return veneuId;
    }

    public String getTimeframeId() {
        return timeframeId;
    }

    public void setVeneuId(String veneuId) {
        this.veneuId = veneuId;
    }

    public void setTimeframeId(String timeframeId) {
        this.timeframeId = timeframeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeframeKey that = (TimeframeKey) o;
        return Objects.equals(veneuId, that.veneuId) &&
                Objects.equals(timeframeId, that.timeframeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(veneuId, timeframeId);
    }
}
