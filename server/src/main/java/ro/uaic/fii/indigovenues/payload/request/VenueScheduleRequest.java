package ro.uaic.fii.indigovenues.payload.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

@ApiModel(description = "All details about the add venue to schedule request")
public class VenueScheduleRequest {
    @ApiModelProperty(notes = "Venue's id")
    @NotBlank
    private String venueId;

    public VenueScheduleRequest() {
    }

    public VenueScheduleRequest(@NotBlank String venueId) {
        this.venueId = venueId;
    }

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VenueScheduleRequest that = (VenueScheduleRequest) o;
        return Objects.equals(venueId, that.venueId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(venueId);
    }
}
