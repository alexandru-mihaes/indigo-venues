package ro.uaic.fii.indigovenues.service.foursquare.venueschedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uaic.fii.indigovenues.exception.foursquare.venueschedule.VenueScheduleNotFoundException;
import ro.uaic.fii.indigovenues.model.foursquare.venueschedule.VenueSchedule;
import ro.uaic.fii.indigovenues.repository.foursquare.venueschedule.VenueScheduleRepository;
import ro.uaic.fii.indigovenues.service.foursquare.VenueServiceImpl;

import java.util.List;
import java.util.Optional;

@Service
public class VenueScheduleServiceImpl implements VenueScheduleService {
    private static final Logger logger = LoggerFactory.getLogger(VenueServiceImpl.class);

    private VenueScheduleRepository venueScheduleRepository;

    @Autowired
    public VenueScheduleServiceImpl(VenueScheduleRepository venueScheduleRepository) {
        this.venueScheduleRepository = venueScheduleRepository;
    }

    @Override
    public VenueSchedule getById(Long id) throws VenueScheduleNotFoundException {
        return venueScheduleRepository.findById(id).orElseThrow(() -> new VenueScheduleNotFoundException(id));
    }

    @Override
    public VenueSchedule save(VenueSchedule venueSchedule) {
        logger.info("Saving venue schedule to database: {}", venueSchedule.getId());
        return venueScheduleRepository.save(venueSchedule);
    }

    @Override
    public VenueSchedule update(VenueSchedule userSchedule) {
        Optional<VenueSchedule> existingUserScheduleOptional = venueScheduleRepository.findById(userSchedule.getId());
        if(existingUserScheduleOptional.isPresent()) {
            VenueSchedule existingUserSchedule = existingUserScheduleOptional.get();
            existingUserSchedule.update(userSchedule);
            logger.info("Updating venue schedule: {}", userSchedule.getId());
            VenueSchedule userScheduleResult = venueScheduleRepository.save(userSchedule);
            return userScheduleResult;
        } else {
            logger.error("Venue schedule not found when trying to update: {}", userSchedule.getId());
            throw new VenueScheduleNotFoundException(userSchedule.getId());
        }
    }

    @Override
    public List<VenueSchedule> getAll() {
        return venueScheduleRepository.findAll();
    }
}
