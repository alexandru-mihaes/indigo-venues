package ro.uaic.fii.indigovenues.model.foursquare.recommended;

import java.io.Serializable;
import java.util.Objects;

public class Item implements Serializable {
    Venue venue;

    public Item() {
    }

    public Item(Venue venue) {
        this.venue = venue;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(venue, item.venue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(venue);
    }
}