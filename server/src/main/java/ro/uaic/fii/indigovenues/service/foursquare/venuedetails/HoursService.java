package ro.uaic.fii.indigovenues.service.foursquare.venuedetails;

import ro.uaic.fii.indigovenues.model.foursquare.venuedetails.Hours;

public interface HoursService {
    Hours getByVenueId(String venueId);

    Hours save(Hours hours);

    Hours saveOpenTimeframesThenHours(Hours hours);
}
