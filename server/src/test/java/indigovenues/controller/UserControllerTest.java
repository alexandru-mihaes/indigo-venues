package ro.uaic.fii.indigovenues.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ro.uaic.fii.indigovenues.exception.EmailAlreadyInUseException;
import ro.uaic.fii.indigovenues.exception.UsernameAlreadyTakenException;
import ro.uaic.fii.indigovenues.mapper.UserMapper;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.UserUpdateRequest;
import ro.uaic.fii.indigovenues.service.AuthenticationService;
import ro.uaic.fii.indigovenues.service.UserService;
import ro.uaic.fii.indigovenues.utils.AuthenticationTestUtils;
import ro.uaic.fii.indigovenues.utils.UserTestUtils;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@RunWith(SpringRunner.class)
public class UserControllerTest {
    private MockMvc mockMvc;
    @Mock
    private AuthenticationService authenticationService;
    @Mock
    private UserService userService;
    @Mock
    private UserMapper userMapper;
    @InjectMocks
    private UserController userController;

    private JacksonTester<UserUpdateRequest> jsonUserUpdateRequest;

    @Before
    public void init() {
        JacksonTester.initFields(this, new ObjectMapper());
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void update_shouldHaveOkStatus_whenUserUpdateRequestIsValid() throws Exception {
        // given
        UserUpdateRequest userUpdateRequest = UserTestUtils.getUserUpdateRequest();
        User currentUser = UserTestUtils.getUser();
        User userUpdated = UserTestUtils.getUserUpdated();
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(currentUser);
        Mockito.when(userService.oldPasswordIsValid(currentUser, userUpdateRequest.getOldPassword())).thenReturn(true);
        Mockito.when(userMapper.map(userUpdateRequest, currentUser.getId())).thenReturn(userUpdated);
        Mockito.when(userService.update(userUpdated)).thenReturn(currentUser);
        // when
        MockHttpServletResponse response = mockMvc.perform(
                put("/api/users/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonUserUpdateRequest.write(userUpdateRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void update_shouldHaveBadRequestStatus_whenCredentialsAreInvalid() throws Exception {
        // given
        UserUpdateRequest userUpdateRequest = UserTestUtils.getUserUpdateRequest();
        userUpdateRequest.setUsername(AuthenticationTestUtils.getInvalidUsername());
        userUpdateRequest.setEmail(AuthenticationTestUtils.getInvalidEmail());
        userUpdateRequest.setNewPassword(AuthenticationTestUtils.getInvalidPassword());
        User currentUser = UserTestUtils.getUser();
        User userUpdated = UserTestUtils.getUserUpdated();
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(currentUser);
        Mockito.when(userService.oldPasswordIsValid(currentUser, userUpdateRequest.getOldPassword())).thenReturn(true);
        Mockito.when(userMapper.map(userUpdateRequest, currentUser.getId())).thenReturn(userUpdated);
        Mockito.when(userService.update(userUpdated)).thenReturn(currentUser);
        // when
        MockHttpServletResponse response = mockMvc.perform(
                put("/api/users/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonUserUpdateRequest.write(userUpdateRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    @Test
    public void update_shouldHaveConflictStatus_whenUserExistsByUsername() throws Exception {
        // given
        UserUpdateRequest userUpdateRequest = UserTestUtils.getUserUpdateRequest();
        User currentUser = UserTestUtils.getUser();
        User userUpdated = UserTestUtils.getUserUpdated();
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(currentUser);
        Mockito.when(userService.oldPasswordIsValid(currentUser, userUpdateRequest.getOldPassword())).thenReturn(true);
        Mockito.when(userMapper.map(userUpdateRequest, currentUser.getId())).thenReturn(userUpdated);
        Mockito.when(userService.update(userUpdated)).thenReturn(currentUser);
        Mockito.doThrow(new UsernameAlreadyTakenException()).when(userService).checkUserDataExists(currentUser, userUpdateRequest.getUsername(), userUpdateRequest.getEmail());
        // when
        MockHttpServletResponse response = mockMvc.perform(
                put("/api/users/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonUserUpdateRequest.write(userUpdateRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.CONFLICT.value(), response.getStatus());
    }

    @Test
    public void update_shouldHaveConflictStatus_whenUserExistsByEmail() throws Exception {
        // given
        UserUpdateRequest userUpdateRequest = UserTestUtils.getUserUpdateRequest();
        User currentUser = UserTestUtils.getUser();
        User userUpdated = UserTestUtils.getUserUpdated();
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(currentUser);
        Mockito.when(userService.oldPasswordIsValid(currentUser, userUpdateRequest.getOldPassword())).thenReturn(true);
        Mockito.when(userMapper.map(userUpdateRequest, currentUser.getId())).thenReturn(userUpdated);
        Mockito.when(userService.update(userUpdated)).thenReturn(currentUser);
        Mockito.doThrow(new EmailAlreadyInUseException()).when(userService).checkUserDataExists(currentUser, userUpdateRequest.getUsername(), userUpdateRequest.getEmail());
        // when
        MockHttpServletResponse response = mockMvc.perform(
                put("/api/users/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonUserUpdateRequest.write(userUpdateRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.CONFLICT.value(), response.getStatus());
    }

    @Test
    public void update_shouldHaveBadRequestStatus_whenOldPasswordIsInvalid() throws Exception {
        // given
        UserUpdateRequest userUpdateRequest = UserTestUtils.getUserUpdateRequest();
        User currentUser = UserTestUtils.getUser();
        User userUpdated = UserTestUtils.getUserUpdated();
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(currentUser);
        Mockito.when(userService.oldPasswordIsValid(currentUser, userUpdateRequest.getOldPassword())).thenReturn(false);
        Mockito.when(userMapper.map(userUpdateRequest, currentUser.getId())).thenReturn(userUpdated);
        Mockito.when(userService.update(userUpdated)).thenReturn(currentUser);
        // when
        MockHttpServletResponse response = mockMvc.perform(
                put("/api/users/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonUserUpdateRequest.write(userUpdateRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }
}
