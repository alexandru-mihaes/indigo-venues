package ro.uaic.fii.indigovenues.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ro.uaic.fii.indigovenues.exception.EmailAlreadyInUseException;
import ro.uaic.fii.indigovenues.exception.UsernameAlreadyTakenException;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.LoginRequest;
import ro.uaic.fii.indigovenues.payload.request.SignUpRequest;
import ro.uaic.fii.indigovenues.payload.response.JwtAuthenticationResponse;
import ro.uaic.fii.indigovenues.service.AuthenticationService;
import ro.uaic.fii.indigovenues.service.UserService;
import ro.uaic.fii.indigovenues.utils.AuthenticationTestUtils;
import ro.uaic.fii.indigovenues.utils.UserTestUtils;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
public class AuthenticationControllerTest {
    private MockMvc mockMvc;
    @Mock
    private UserService userService;
    @Mock
    private AuthenticationService authenticationService;
    @InjectMocks
    private AuthenticationController authenticationController;

    private JacksonTester<LoginRequest> jsonLoginRequest;
    private JacksonTester<JwtAuthenticationResponse> jsonJwtAuthenticationResponse;
    private JacksonTester<SignUpRequest> jsonSignUpRequest;

    @Before
    public void init() {
        JacksonTester.initFields(this, new ObjectMapper());
        mockMvc = MockMvcBuilders.standaloneSetup(authenticationController).build();
    }

    @Test
    public void authenticateUser_shouldReturnJwtAuthenticationResponse() throws Exception {
        // given
        LoginRequest loginRequest = AuthenticationTestUtils.getLoginRequest();
        User user = UserTestUtils.getUser();
        String jwt = AuthenticationTestUtils.getJwt();
        Mockito.when(authenticationService.authenticateUser(loginRequest)).thenReturn(jwt);
        Mockito.when(userService.getUserByUsernameOrEmail(loginRequest.getUsernameOrEmail())).thenReturn(user);
        // when
        MockHttpServletResponse response = mockMvc.perform(
                post("/api/auth/signin")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonLoginRequest.write(loginRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.OK.value(), response.getStatus());
        JwtAuthenticationResponse jwtAuthenticationResponseExpected = AuthenticationTestUtils.getJwtAuthenticationResponse();
        Assert.assertEquals(
                jsonJwtAuthenticationResponse.write(jwtAuthenticationResponseExpected).getJson(),
                response.getContentAsString()
        );
    }

    @Test
    public void registerUser_shouldHaveCreatedStatus_whenSignUpRequestIsValid() throws Exception {
        // given
        SignUpRequest signUpRequest = AuthenticationTestUtils.getSignUpRequest();
        User user = UserTestUtils.getUser();
        Mockito.when(authenticationService.createUser(signUpRequest)).thenReturn(user);
        // when
        MockHttpServletResponse response = mockMvc.perform(
                post("/api/auth/signup")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSignUpRequest.write(signUpRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    }

    @Test
    public void registerUser_shouldHaveBadRequestStatus_whenCredentialsAreInvalid() throws Exception {
        // given
        SignUpRequest signUpRequest = AuthenticationTestUtils.getSignUpRequest();
        signUpRequest.setUsername(AuthenticationTestUtils.getInvalidUsername());
        User user = UserTestUtils.getUser();
        Mockito.when(authenticationService.createUser(signUpRequest)).thenReturn(user);
        // when
        MockHttpServletResponse response = mockMvc.perform(
                post("/api/auth/signup")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSignUpRequest.write(signUpRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    @Test
    public void registerUser_shouldHaveConflictStatus_whenUserExistsByUsername() throws Exception {
        // given
        SignUpRequest signUpRequest = AuthenticationTestUtils.getSignUpRequest();
        User user = UserTestUtils.getUser();
        Mockito.when(authenticationService.createUser(signUpRequest)).thenReturn(user);
        Mockito.doThrow(new UsernameAlreadyTakenException()).when(userService).checkUserDataExists(signUpRequest.getUsername(), signUpRequest.getEmail());
        // when
        MockHttpServletResponse response = mockMvc.perform(
                post("/api/auth/signup")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSignUpRequest.write(signUpRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.CONFLICT.value(), response.getStatus());
    }

    @Test
    public void registerUser_shouldHaveConflictStatus_whenUserExistsByEmail() throws Exception {
        // given
        SignUpRequest signUpRequest = AuthenticationTestUtils.getSignUpRequest();
        User user = UserTestUtils.getUser();
        Mockito.when(authenticationService.createUser(signUpRequest)).thenReturn(user);
        Mockito.doThrow(new EmailAlreadyInUseException()).when(userService).checkUserDataExists(user.getUsername(), user.getEmail());
        // when
        MockHttpServletResponse response = mockMvc.perform(
                post("/api/auth/signup")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSignUpRequest.write(signUpRequest).getJson()))
                .andReturn().getResponse();
        // then
        Assert.assertEquals(HttpStatus.CONFLICT.value(), response.getStatus());
    }
}
