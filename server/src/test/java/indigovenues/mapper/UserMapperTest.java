package ro.uaic.fii.indigovenues.mapper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.UserUpdateRequest;
import ro.uaic.fii.indigovenues.utils.UserTestUtils;

public class UserMapperTest {
    private User user, userUpdated;
    private UserUpdateRequest userUpdateRequest;

    @Before
    public void setup() {
        user = UserTestUtils.getUser();
        userUpdated = UserTestUtils.getUserUpdated();
        userUpdateRequest = UserTestUtils.getUserUpdateRequest();
    }

    @Test
    public void map_shouldReturnInstanceOfUser_whenArgumentIsUserUpdateRequest() {
        UserMapper userMapper = new UserMapper();
        User userUpdatedActual = userMapper.map(userUpdateRequest, user.getId());
        Assert.assertEquals(userUpdated, userUpdatedActual);
    }
}
