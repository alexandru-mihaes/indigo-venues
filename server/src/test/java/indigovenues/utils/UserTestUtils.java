package ro.uaic.fii.indigovenues.utils;

import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.UserUpdateRequest;

import java.util.Date;

public class UserTestUtils {
    public static User getUser() {
        User user = new User();
        user.setId(1L);
        user.setUsername("Username");
        user.setEmail("user@user.ro");
        user.setPassword("Password123#");
        user.setCreatedAt(new Date());
        user.setUpdatedAt(new Date());
        return user;
    }

    public static User getUserWithEmptyId() {
        User user = new User();
        user.setUsername("Username");
        user.setEmail("user@user.ro");
        user.setPassword("Password123#");
        return user;
    }

    public static UserUpdateRequest getUserUpdateRequest() {
        UserUpdateRequest userUpdateRequest = new UserUpdateRequest();
        userUpdateRequest.setUsername("Username_new");
        userUpdateRequest.setEmail("user_new@user.ro");
        userUpdateRequest.setOldPassword(getUser().getPassword());
        userUpdateRequest.setNewPassword("Password123#_new");
        return userUpdateRequest;
    }

    public static User getUserUpdated() {
        User user = getUser();
        UserUpdateRequest userUpdateRequest = getUserUpdateRequest();
        user.setUsername(userUpdateRequest.getUsername());
        user.setEmail(userUpdateRequest.getEmail());
        user.setPassword(userUpdateRequest.getNewPassword());
        return user;
    }

    public static String getNewUsername() {
        return "Username_new";
    }

    public static String getNewEmail() {
        return "user_new@user.ro";
    }
}
