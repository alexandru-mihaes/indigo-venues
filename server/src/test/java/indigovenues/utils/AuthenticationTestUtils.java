package ro.uaic.fii.indigovenues.utils;

import ro.uaic.fii.indigovenues.payload.request.LoginRequest;
import ro.uaic.fii.indigovenues.payload.request.SignUpRequest;
import ro.uaic.fii.indigovenues.payload.response.JwtAuthenticationResponse;

public class AuthenticationTestUtils {
    public static String getJwt() {
        return "eyJhbGciOiJIRzUxMiJ2" +
                ".eyJzdWIiOiIxIiRiaWF0IjoxNTU1Njc4NzcxLCJleHAiOjE1NTYyODM1NzF9" +
                ".ofafD-RufIscmRxxax3aAalEqdCY5bMbXjRdmrpOC82updiMI3aCqPaBQRR1SDHhg00bn6AY9lPx-GcFWHyYrw";
    }

    public static LoginRequest getLoginRequest() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail("Username");
        loginRequest.setPassword("Password123#");
        return loginRequest;
    }

    public static SignUpRequest getSignUpRequest() {
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setUsername("Username");
        signUpRequest.setEmail("user@user.ro");
        signUpRequest.setPassword("Password123#");
        return signUpRequest;
    }

    public static String getInvalidUsername() {
        return "u";
    }

    public static String getInvalidEmail() {
        return "email";
    }

    public static String getInvalidPassword() {
        return "password";
    }

    public static JwtAuthenticationResponse getJwtAuthenticationResponse() {
        JwtAuthenticationResponse jwtAuthenticationResponse = new JwtAuthenticationResponse(null);
        jwtAuthenticationResponse.setAccessToken(getJwt());
        jwtAuthenticationResponse.setUsername("Username");
        jwtAuthenticationResponse.setEmail("user@user.ro");
        jwtAuthenticationResponse.setMessage("User authenticated successfully");
        return jwtAuthenticationResponse;
    }
}
