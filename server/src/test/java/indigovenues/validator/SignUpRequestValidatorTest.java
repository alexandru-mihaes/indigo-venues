package ro.uaic.fii.indigovenues.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import ro.uaic.fii.indigovenues.payload.request.SignUpRequest;
import ro.uaic.fii.indigovenues.utils.AuthenticationTestUtils;
import ro.uaic.fii.indigovenues.utils.UserTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class SignUpRequestValidatorTest {
    private SignUpRequestValidator signUpRequestValidator;

    @Before
    public void setup() {
        signUpRequestValidator = new SignUpRequestValidator(UserTestUtils.getUser().getUsername());
    }

    @Test
    public void validate_shouldNotHaveErrors_whenSignUpRequestIsNotValid() {
        // given
        SignUpRequest signUpRequest = AuthenticationTestUtils.getSignUpRequest();
        Errors errors = new BeanPropertyBindingResult(signUpRequest, "signUpRequest");
        // when
        signUpRequestValidator.validate(signUpRequest, errors);
        // then
        Assert.assertFalse(errors.hasErrors());
    }

    @Test
    public void validate_shouldHaveUsernameFieldError_whenUsernameIsNotValid() {
        // given
        SignUpRequest signUpRequest = AuthenticationTestUtils.getSignUpRequest();
        signUpRequest.setUsername(AuthenticationTestUtils.getInvalidUsername());
        Errors errors = new BeanPropertyBindingResult(signUpRequest, "signUpRequest");
        SignUpRequestValidator signUpRequestValidatorSpy = Mockito.spy(signUpRequestValidator);
        Mockito.doNothing().when(signUpRequestValidatorSpy).checkIfHasErrors(Mockito.<Errors>any());
        // when
        signUpRequestValidatorSpy.validate(signUpRequest, errors);
        // then
        Assert.assertTrue(errors.hasErrors());
        Assert.assertNotNull(errors.getFieldError("username"));
    }

    @Test
    public void validate_shouldHaveEmailRejectValue_whenEmailIsNotValid() {
        // given
        SignUpRequest signUpRequest = AuthenticationTestUtils.getSignUpRequest();
        signUpRequest.setEmail(AuthenticationTestUtils.getInvalidEmail());
        Errors errors = new BeanPropertyBindingResult(signUpRequest, "signUpRequest");
        SignUpRequestValidator signUpRequestValidatorSpy = Mockito.spy(signUpRequestValidator);
        Mockito.doNothing().when(signUpRequestValidatorSpy).checkIfHasErrors(Mockito.<Errors>any());
        // when
        signUpRequestValidatorSpy.validate(signUpRequest, errors);
        // then
        Assert.assertTrue(errors.hasErrors());
        Assert.assertNotNull(errors.getFieldError("email"));
    }

    @Test
    public void validate_shouldHavePasswordRejectValue_whenPasswordIsNotValid() {
        // given
        SignUpRequest signUpRequest = AuthenticationTestUtils.getSignUpRequest();
        signUpRequest.setPassword(AuthenticationTestUtils.getInvalidPassword());
        Errors errors = new BeanPropertyBindingResult(signUpRequest, "signUpRequest");
        SignUpRequestValidator signUpRequestValidatorSpy = Mockito.spy(signUpRequestValidator);
        Mockito.doNothing().when(signUpRequestValidatorSpy).checkIfHasErrors(Mockito.<Errors>any());
        // when
        signUpRequestValidatorSpy.validate(signUpRequest, errors);
        // then
        Assert.assertTrue(errors.hasErrors());
        Assert.assertNotNull(errors.getFieldError("password"));
    }
}
