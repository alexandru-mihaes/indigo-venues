package ro.uaic.fii.indigovenues.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import ro.uaic.fii.indigovenues.exception.EmailAlreadyInUseException;
import ro.uaic.fii.indigovenues.exception.UserNotFoundException;
import ro.uaic.fii.indigovenues.exception.UsernameAlreadyTakenException;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.repository.UserRepository;
import ro.uaic.fii.indigovenues.utils.UserTestUtils;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    private User user;

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @InjectMocks
    private UserServiceImpl userService;

    @Before
    public void setup() {
        user = UserTestUtils.getUser();
    }

    @Test
    public void getUserByUsernameOrEmail_shouldReturnInstanceOfUser() {
        // given
        Optional<User> userOptional = Optional.of(user);
        String usernameOrEmail = user.getUsername();
        Mockito.when(userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)).thenReturn(userOptional);
        // when
        User userActual = userService.getUserByUsernameOrEmail(usernameOrEmail);
        // then
        Assert.assertEquals(user, userActual);
    }

    @Test(expected = UserNotFoundException.class)
    public void getUserByUsernameOrEmail_shouldThrowException_whenUserDoesNotExist() {
        // given
        Optional<User> userOptional = Optional.empty();
        String usernameOrEmail = "Invalid";
        Mockito.when(userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)).thenReturn(userOptional);
        // when
        userService.getUserByUsernameOrEmail(usernameOrEmail);
    }

    @Test
    public void getUserByUsername_shouldReturnInstanceOfUser() {
        // given
        Optional<User> userOptional = Optional.of(user);
        String username = user.getUsername();
        Mockito.when(userRepository.findByUsername(username)).thenReturn(userOptional);
        // when
        User userActual = userService.getUserByUsername(username);
        // then
        Assert.assertEquals(user, userActual);
    }

    @Test(expected = UserNotFoundException.class)
    public void getUserByUsername_shouldThrowException_whenUserDoesNotExist() {
        // given
        Optional<User> userOptional = Optional.empty();
        String username = "Invalid";
        Mockito.when(userRepository.findByUsername(username)).thenReturn(userOptional);
        // when
        userService.getUserByUsername(username);
    }

    @Test
    public void save_shouldReturnInstanceOfUser() {
        // given
        Mockito.when(userRepository.save(user)).thenReturn(user);
        // when
        User userActual = userService.save(user);
        // then
        Assert.assertEquals(user, userActual);
    }

    @Test
    public void update_shouldReturnInstanceOfUser_whenUserWithGivenIdExists() {
        // given
        Optional<User> userOptional = Optional.of(user);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(userOptional);
        Mockito.when(passwordEncoder.encode(user.getPassword())).thenReturn(user.getPassword());
        Mockito.when(userRepository.save(user)).thenReturn(user);
        // when
        User userActual = userService.update(user);
        // then
        Assert.assertEquals(user, userActual);
    }

    @Test(expected = UserNotFoundException.class)
    public void update_shouldThrowException_whenUserWithGivenIdDoesNotExist() {
        // given
        Optional<User> userOptional = Optional.empty();
        Mockito.when(userRepository.findById(user.getId())).thenReturn(userOptional);
        // when
        userService.update(user);
    }

    @Test(expected = UsernameAlreadyTakenException.class)
    public void checkUserDataExists_shouldThrowException_whenUsernameIsAlreadyTaken() {
        // given
        String username = user.getUsername();
        String email = user.getEmail();
        Mockito.when(userRepository.existsByUsername(username)).thenReturn(true);
        Mockito.when(userRepository.existsByEmail(email)).thenReturn(false);
        // when
        userService.checkUserDataExists(username, email);
    }

    @Test(expected = EmailAlreadyInUseException.class)
    public void checkUserDataExists_shouldThrowException_whenEmailIsAlreadyInUser() {
        // given
        String username = user.getUsername();
        String email = user.getEmail();
        Mockito.when(userRepository.existsByUsername(username)).thenReturn(false);
        Mockito.when(userRepository.existsByEmail(email)).thenReturn(true);
        // when
        userService.checkUserDataExists(username, email);
    }

    @Test(expected = UsernameAlreadyTakenException.class)
    public void checkUserDataExists_shouldThrowException_whenUsernameIsAlreadyTaken_and_currentUserIsArgument() {
        // given
        String username = UserTestUtils.getNewUsername();
        String email = UserTestUtils.getNewEmail();
        Mockito.when(userRepository.existsByUsername(username)).thenReturn(true);
        Mockito.when(userRepository.existsByEmail(email)).thenReturn(false);
        // when
        userService.checkUserDataExists(user, username, email);
    }

    @Test(expected = EmailAlreadyInUseException.class)
    public void checkUserDataExists_shouldThrowException_whenEmailIsAlreadyInUser_and_currentUserIsArgument() {
        // given
        String username = UserTestUtils.getNewUsername();
        String email = UserTestUtils.getNewEmail();
        Mockito.when(userRepository.existsByUsername(username)).thenReturn(false);
        Mockito.when(userRepository.existsByEmail(email)).thenReturn(true);
        // when
        userService.checkUserDataExists(user, username, email);
    }
}