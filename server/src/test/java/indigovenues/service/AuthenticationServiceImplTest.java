package ro.uaic.fii.indigovenues.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import ro.uaic.fii.indigovenues.model.User;
import ro.uaic.fii.indigovenues.payload.request.LoginRequest;
import ro.uaic.fii.indigovenues.payload.request.SignUpRequest;
import ro.uaic.fii.indigovenues.security.service.JwtTokenProvider;
import ro.uaic.fii.indigovenues.utils.AuthenticationTestUtils;
import ro.uaic.fii.indigovenues.utils.UserTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationServiceImplTest {
    @Mock
    private UserService userService;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtTokenProvider jwtTokenProvider;
    @InjectMocks
    private AuthenticationServiceImpl authenticationService;

    @Test
    public void authenticateUser_shouldReturnJwt() {
        // given
        String jwt = AuthenticationTestUtils.getJwt();
        LoginRequest loginRequest = AuthenticationTestUtils.getLoginRequest();
        Mockito.when(authenticationManager.authenticate(Mockito.<Authentication>any()))
                .thenReturn(
                        new UsernamePasswordAuthenticationToken(
                                loginRequest.getUsernameOrEmail(),
                                loginRequest.getPassword()
                        ));
        Mockito.when(jwtTokenProvider.generateToken(Mockito.<Authentication>any()))
                .thenReturn(jwt);
        // when
        String jwtActual = authenticationService.authenticateUser(loginRequest);
        // then
        Assert.assertEquals(jwt, jwtActual);
    }

    @Test
    public void createUser_shouldReturnUser() {
        // given
        SignUpRequest signUpRequest = AuthenticationTestUtils.getSignUpRequest();
        User user = UserTestUtils.getUserWithEmptyId();
        String password = user.getPassword();
        String encodedPassword = "Encoded password";
        user.setPassword(encodedPassword);
        Mockito.when(passwordEncoder.encode(password)).thenReturn(encodedPassword);
        Mockito.when(userService.save(Mockito.<User>any())).thenReturn(user);
        // when
        User userActual = authenticationService.createUser(signUpRequest);
        // then
        Assert.assertEquals(user, userActual);
    }
}
