@echo off
echo Maven Clean...
call mvn clean
echo Maven Clean Finished
echo Maven Package...
call mvn package
echo Maven Package Finished
timeout /t 3
echo Spring Boot Run...
call mvn spring-boot:run
pause
